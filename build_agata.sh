#!/bin/bash

mkdir -p build_agata
mkdir -p bin
cd build_agata
cmake -DCMAKE_BUILD_TYPE=release -DUSE_MPI=on -DAGATA=on ..
make -j

cp ./tests/sim_1d ../bin
cp ./tests/sim_2d ../bin

