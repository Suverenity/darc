//
// Created by suverenity on 3/29/18.
//

#ifndef PROJECT_PARTICLE_H
#define PROJECT_PARTICLE_H

#include "Vector.h"

namespace particles
{

template < class RealType >
class Particle
{
public:
    Particle( ) = default;

    Particle( RealType x )
        : m_x( x, 0., 0. )
        , m_v( 0.0, 0.0, 0.0 )
    {
    }

    Particle( const VectorClass< RealType > x, const VectorClass< RealType > v )
        : m_x( x )
        , m_v( v )
    {
    }

    Particle( const Particle & other ) = default;
    Particle( Particle && other )      = default;
    ~Particle( )                       = default;

    inline RealType x( ) const
    {
        return m_x[ 0 ];
    }

    inline RealType y( ) const
    {
        return m_x[ 1 ];
    }

    inline RealType z( ) const
    {
        return m_x[ 2 ];
    }

    inline RealType vx( ) const
    {
        return m_v[ 0 ];
    }

    inline RealType vy( ) const
    {
        return m_v[ 1 ];
    }

    inline RealType vz( ) const
    {
        return m_v[ 2 ];
    }

    inline RealType & x( )
    {
        return m_x[ 0 ];
    }

    inline RealType & y( )
    {
        return m_x[ 1 ];
    }

    inline RealType & z( )
    {
        return m_x[ 2 ];
    }

    inline RealType & vx( )
    {
        return m_v[ 0 ];
    }

    inline RealType & vy( )
    {
        return m_v[ 1 ];
    }

    inline RealType & vz( )
    {
        return m_v[ 2 ];
    }

    VectorClass< RealType > m_x;
    VectorClass< RealType > m_v;

private:
    friend std::ostream & operator<<( std::ostream & os, const Particle & p )
    {
        os << "[x: " << p.m_x << ",v: [" << p.m_v[ 0 ] << "," << p.m_v[ 1 ] << ","
           << p.m_v[ 2 ] << "]]";
        return os;
    }
};

}// namespace particles

#endif//PROJECT_PARTICLE_H
