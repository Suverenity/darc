#pragma once

#ifndef FFTW_ALLOCATOR_H
#define FFTW_ALLOCATOR_H

#include <memory>

extern "C"
{
#include <fftw3.h>
}

namespace particles
{

template < typename Tdata >
class fftw_allocator : public std::allocator< Tdata >
{
public:
    template < typename U >
    struct rebind
    {
        typedef fftw_allocator other;
    };

    Tdata * allocate( size_t n );

    void deallocate( Tdata * data, std::size_t size );
};

}// namespace particles

#include "fftw_allocator.hpp"

#endif
