#pragma once

#include "Mpi.h"
#include "ParticleHolder.h"
#include "Types.h"

#include <fstream>
#include <numeric>

namespace particles
{

template < int Dim, typename MeshType, typename RealType >
class Test
{
public:
    using real_arr_t = typename MeshType::real_arr_t;

    Test( MeshType & mesh, RealType alpha, const size_t ppc, bool save_tofile = false )
        : mesh( mesh )
        , m_ppar( mesh.create_arr( ) )
        , m_pper( mesh.create_arr( ) )
        , save_tofile( save_tofile )
        , mu( alpha * alpha )
        , ppc( ppc )
    {
        if( save_tofile )
        {
            file.open( "energy.csv" );
            file << "kinetic energy, electric energy, magnetic energy" << std::endl;
        }
    }
    ~Test( )
    {
        if( save_tofile )
        {
            file.close( );
        }
    }

    RealType computeEnergy(
        real_arr_t & Ex, real_arr_t & Ey, real_arr_t & Ez,
        real_arr_t & Bx, real_arr_t & By, real_arr_t & Bz,
        ParticleHolders< MeshType, RealType > & particles );

    struct presure
    {
        presure( real_arr_t & ppar, real_arr_t & pper )
            : ppar( ppar )
            , pper( pper )
        {
        }
        real_arr_t & ppar;
        real_arr_t & pper;
    };

    template < typename ParticlesToMesh >
    presure computePresure(
        ParticlesToMesh & particles_to_mesh,
        ParticleHolder< MeshType, RealType > & species,
        const VectorClass< RealType > & b0 );

private:
    RealType compute_electric_energy( real_arr_t & Ex, real_arr_t & Ey, real_arr_t & Ez );

    RealType compute_magnetic_energy( real_arr_t & Bx, real_arr_t & By, real_arr_t & Bz );

    RealType computeKineticEnergy( ParticleHolders< MeshType, RealType > & particles,
                                   const size_t number_of_cells );

    MeshType & mesh;

    real_arr_t m_ppar;
    real_arr_t m_pper;

    //csv format where every line is one time stamp
    std::ofstream file;
    bool save_tofile = false;
    RealType mu;
    RealType ppc;
};

#include "Test.hpp"

}// namespace particles
