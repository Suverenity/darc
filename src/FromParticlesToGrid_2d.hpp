#include "FromParticlesToGrid.h"

#include "Mpi.h"

namespace particles
{

template < typename MeshType, typename RealType >
void FromParticlesToGridFirstOrder< 2, MeshType, RealType >::run( )
{
    for( auto & species : m_particles )
    {
        runSpecies( species );
    }
}

template < typename MeshType, typename RealType >
void FromParticlesToGridFirstOrder< 2, MeshType, RealType >::runSpecies( ParticleHolder< MeshType, RealType > & species )
{
    std::fill( species.m_rho.begin( ), species.m_rho.end( ), 0. );

    std::fill( species.m_jx.begin( ), species.m_jx.end( ), 0. );
    std::fill( species.m_jy.begin( ), species.m_jy.end( ), 0. );
    std::fill( species.m_jz.begin( ), species.m_jz.end( ), 0. );

    for( auto i = 0u; i < species.m_particles.size( ); ++i )
    {
        const auto & p = species.m_particles[ i ];
        runOneParticle( p, species.m_qs_qe,
                        species.m_rho, species.m_jx,
                        species.m_jy, species.m_jz );
    }

    mpi::all_reduce_sum( species.m_rho );

    mpi::all_reduce_sum( species.m_jx );
    mpi::all_reduce_sum( species.m_jy );
    mpi::all_reduce_sum( species.m_jz );

    const auto size = species.m_rho.size( );
    for( size_t i = 0; i != size; ++i )
    {
        species.m_rho[ i ] *= inversed_ppc;

        species.m_jx[ i ] *= inversed_ppc;
        species.m_jy[ i ] *= inversed_ppc;
        species.m_jz[ i ] *= inversed_ppc;
    }
}

template < typename MeshType, typename RealType >
typename FromParticlesToGridFirstOrder< 2, MeshType, RealType >::weights_t
FromParticlesToGridFirstOrder< 2, MeshType, RealType >::compute_weights_particle(
    const Particle< RealType > & p )
{
    const auto x_idxs = m_mesh.computeXIndicesFromPosition( p.x( ) );
    const auto y_idxs = m_mesh.computeYIndicesFromPosition( p.y( ) );

    return compute_weights_particle( p, x_idxs, y_idxs );
}

template < typename MeshType, typename RealType >
typename FromParticlesToGridFirstOrder< 2, MeshType, RealType >::weights_t
FromParticlesToGridFirstOrder< 2, MeshType, RealType >::compute_weights_particle(
    const Particle< RealType > & p, const indexPair_t & x_indices, const indexPair_t & y_indices )
{
    const auto dx_inv      = m_mesh.m_dx_inv;
    const auto weightLeft  = ( p.x( ) - m_mesh.x( x_indices.first ) ) * dx_inv;
    const auto weightRight = RealType( 1 ) - weightLeft;

    const auto dy_inv       = m_mesh.m_dy_inv;
    const auto weightBottom = ( p.y( ) - m_mesh.y( y_indices.first ) ) * dy_inv;
    const auto weightTop    = RealType( 1 ) - weightBottom;

    // bilinear interpolation computed via geometric method uses parts that are diagonally on the other side
    return {
        weightRight * weightTop,
        weightLeft * weightTop,
        weightRight * weightBottom,
        weightLeft * weightBottom
    };
}

template < typename MeshType, typename RealType >
void FromParticlesToGridFirstOrder< 2, MeshType, RealType >::map_to_grid(
    const Particle< RealType > & p, const weights_t & weights,
    real_arr_t & grid_quantity, const RealType quantity )
{
    throw std::runtime_error( "not implemented map_to_grid" );
}

template < typename MeshType, typename RealType >
void FromParticlesToGridFirstOrder< 2, MeshType, RealType >::map_to_grid(
    const Particle< RealType > & p, const weights_t & weights,
    const indexPair_t & x_indices, const indexPair_t & y_indices,
    real_arr_t & grid_quantity, const RealType quantity )
{
    grid_quantity.xy_coord( x_indices.first, y_indices.first ) += weights.left_bottom * quantity;
    grid_quantity.xy_coord( x_indices.first, y_indices.second ) += weights.left_top * quantity;
    grid_quantity.xy_coord( x_indices.second, y_indices.first ) += weights.right_bottom * quantity;
    grid_quantity.xy_coord( x_indices.second, y_indices.second ) += weights.right_top * quantity;
}

template < typename MeshType, typename RealType >
void FromParticlesToGridFirstOrder< 2, MeshType, RealType >::runOneParticle(
    const Particle< RealType > & p, RealType qParticle,
    real_arr_t & rho, real_arr_t & jx, real_arr_t & jy, real_arr_t & jz )
{
    const auto x_idxs = m_mesh.computeXIndicesFromPosition( p.x( ) );
    const auto y_idxs = m_mesh.computeYIndicesFromPosition( p.y( ) );

    const auto weights = compute_weights_particle( p, x_idxs, y_idxs );

    map_to_grid( p, weights, x_idxs, y_idxs, rho, qParticle );

    map_to_grid( p, weights, x_idxs, y_idxs, jx, qParticle * p.vx( ) );
    map_to_grid( p, weights, x_idxs, y_idxs, jy, qParticle * p.vy( ) );
    map_to_grid( p, weights, x_idxs, y_idxs, jz, qParticle * p.vz( ) );
}

template < typename MeshType, typename RealType >
void FromParticlesToGridFirstOrder< 2, MeshType, RealType >::computeDensity(
    std::vector< std::vector< RealType > > & n_s )
{
    throw std::runtime_error( "not implemented" );
}

template < typename MeshType, typename RealType >
RealType FromParticlesToGridFirstOrder< 2, MeshType, RealType >::weightQuantityOnParticle(
    Particle< RealType > & particle, const real_arr_t & quantityOnGrid )
{
    const auto x_idxs = m_mesh.computeXIndicesFromPosition( particle.x( ) );
    const auto y_idxs = m_mesh.computeYIndicesFromPosition( particle.y( ) );

    const auto weights = compute_weights_particle( particle, x_idxs, y_idxs );

    return weights.left_bottom * quantityOnGrid.xy_coord( x_idxs.first, y_idxs.first ) +
        weights.left_top * quantityOnGrid.xy_coord( x_idxs.first, y_idxs.second ) +
        weights.right_bottom * quantityOnGrid.xy_coord( x_idxs.second, y_idxs.first ) +
        weights.right_top * quantityOnGrid.xy_coord( x_idxs.second, y_idxs.second );
}

}// namespace particles
