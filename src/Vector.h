
#ifndef PROJECT_VECTOR_H
#define PROJECT_VECTOR_H

#include <array>
#include <cstring>
#include <iostream>

namespace particles
{

template < typename RealType >
using scalar_t = RealType;

template < typename RealType >
class VectorClass
{
public:
    VectorClass( RealType x, RealType y, RealType z )
    {
        m_v[ 0 ] = x;
        m_v[ 1 ] = y;
        m_v[ 2 ] = z;
    }

    VectorClass( ) = default;

    VectorClass( const VectorClass & other ) = default;

    VectorClass( VectorClass && other ) = default;

    VectorClass & operator=( const VectorClass & other ) = default;

    VectorClass & operator=( VectorClass && other ) = default;

    RealType & x( )
    {
        return m_v[ 0 ];
    }

    RealType & y( )
    {
        return m_v[ 1 ];
    }

    RealType & z( )
    {
        return m_v[ 2 ];
    }

    RealType x( ) const
    {
        return m_v[ 0 ];
    }

    RealType y( ) const
    {
        return m_v[ 1 ];
    }

    RealType z( ) const
    {
        return m_v[ 2 ];
    }

    inline RealType L2Squared( ) const
    {
        return m_v[ 0 ] * m_v[ 0 ] + m_v[ 1 ] * m_v[ 1 ] + m_v[ 2 ] * m_v[ 2 ];
    }

    inline RealType L2( ) const
    {
        return sqrt( L2Squared( ) );
    }

    inline VectorClass crossProduct( const VectorClass & other ) const
    {
        return {
            m_v[ 1 ] * other.m_v[ 2 ] - m_v[ 2 ] * other.m_v[ 1 ],
            m_v[ 2 ] * other.m_v[ 0 ] - m_v[ 0 ] * other.m_v[ 2 ],
            m_v[ 0 ] * other.m_v[ 1 ] - m_v[ 1 ] * other.m_v[ 0 ],
        };
    }

    inline RealType dotProduct( const VectorClass & other ) const
    {
        return other.m_v[ 0 ] * this->m_v[ 0 ] +
            other.m_v[ 1 ] * this->m_v[ 1 ] +
            other.m_v[ 2 ] * this->m_v[ 2 ];
    }

    VectorClass operator+( const VectorClass & v ) const
    {
        return { m_v[ 0 ] + v.m_v[ 0 ],
                 m_v[ 1 ] + v.m_v[ 1 ],
                 m_v[ 2 ] + v.m_v[ 2 ] };
    }

    VectorClass operator-( const VectorClass & v ) const
    {
        return { m_v[ 0 ] - v.m_v[ 0 ],
                 m_v[ 1 ] - v.m_v[ 1 ],
                 m_v[ 2 ] - v.m_v[ 2 ] };
    }

    VectorClass operator/( RealType divisor ) const
    {
        return { m_v[ 0 ] / divisor,
                 m_v[ 1 ] / divisor,
                 m_v[ 2 ] / divisor };
    }

    friend std::ostream & operator<<( std::ostream & os, const VectorClass & v )
    {
        os << "[" << v.m_v[ 0 ] << "," << v.m_v[ 1 ] << "," << v.m_v[ 2 ] << "]";
        return os;
    }

    friend VectorClass< RealType > operator*(
        const scalar_t< RealType > scalar,
        const VectorClass< RealType > & v )
    {
        return { v.m_v[ 0 ] * scalar,
                 v.m_v[ 1 ] * scalar,
                 v.m_v[ 2 ] * scalar };
    }

    friend VectorClass< RealType > operator*(
        const VectorClass< RealType > & v,
        const scalar_t< RealType > scalar )
    {
        return { v.m_v[ 0 ] * scalar,
                 v.m_v[ 1 ] * scalar,
                 v.m_v[ 2 ] * scalar };
    }

    inline RealType & operator[]( size_t index )
    {
        return m_v[ index ];
    }

    inline const RealType operator[]( size_t index ) const
    {
        return m_v[ index ];
    }
    //    private:
    std::array< RealType, 3 > m_v;
};

}// namespace particles

#endif//PROJECT_VECTOR_H
