#include "Test.h"
#include <iostream>

template < int Dim, class MeshType, class RealType >
RealType particles::Test< Dim, MeshType, RealType >::computeEnergy(
    real_arr_t & Ex, real_arr_t & Ey, real_arr_t & Ez,
    real_arr_t & Bx, real_arr_t & By, real_arr_t & Bz,
    ParticleHolders< MeshType, RealType > & particles )
{
    //auto k_eg = computeKineticEnergy( particles, Ex.size( ) );
    auto k_eg = 0;
    auto e_eg = compute_electric_energy( Ex, Ey, Ez );
    auto m_eg = compute_magnetic_energy( Bx, By, Bz );

    //if( save_tofile )
    //{
    //    file << k_eg << "," << e_eg << "," << m_eg << std::endl;
    //}
    std::cout << "electric eg: " << e_eg << ", magnetic eg: " << m_eg
              << ", kinetic eg: " << k_eg << std::endl;
    return k_eg + e_eg + m_eg;
}

template < int Dim, class MeshType, class RealType >
template < typename ParticlesToMesh >
typename particles::Test< Dim, MeshType, RealType >::presure
particles::Test< Dim, MeshType, RealType >::computePresure(
    ParticlesToMesh & particles_to_mesh,
    particles::ParticleHolder< MeshType, RealType > & species,
    const particles::VectorClass< RealType > & b0 )
{
    std::fill( m_ppar.begin( ), m_ppar.end( ), 0 );
    std::fill( m_pper.begin( ), m_pper.end( ), 0 );

    auto & particles = species.m_particles;

    for( size_t i = 0; i != particles.size( ); ++i )
    {
        Particle< RealType > & particle = particles[ i ];

        const RealType v_par = particle.m_v.dotProduct( b0 );
        const auto v_per_sq  = ( particle.m_v - v_par * b0 ).L2Squared( );

        if constexpr( Dim == 1 )
        {
            const auto xidxs   = mesh.computeXIndicesFromPosition( particle.x( ) );
            const auto weights = particles_to_mesh.compute_weights_particle( particle, xidxs );
            particles_to_mesh.map_to_grid( particle, weights, xidxs, m_ppar, v_par * v_par );
            particles_to_mesh.map_to_grid( particle, weights, xidxs, m_pper, v_per_sq );
        }
        else if constexpr( Dim == 2 )
        {
            const auto xidxs   = mesh.computeXIndicesFromPosition( particle.x( ) );
            const auto yidxs   = mesh.computeYIndicesFromPosition( particle.y( ) );
            const auto weights = particles_to_mesh.compute_weights_particle( particle, xidxs, yidxs );
            particles_to_mesh.map_to_grid( particle, weights, xidxs, yidxs, m_ppar, v_par * v_par );
            particles_to_mesh.map_to_grid( particle, weights, xidxs, yidxs, m_pper, v_per_sq );
        }
        else
        {
            std::runtime_error( "Dimension not supported" );
        }
    }

    mpi::all_reduce_sum( m_ppar );
    mpi::all_reduce_sum( m_pper );

    for( auto & value : m_ppar )
    {
        value *= species.m_ms_me;
    }
    for( auto & value : m_pper )
    {
        value *= species.m_ms_me * 0.5;
    }

    return { m_ppar, m_pper };
}

template < int Dim, class MeshType, class RealType >
RealType particles::Test< Dim, MeshType, RealType >::compute_electric_energy(
    real_arr_t & Ex, real_arr_t & Ey, real_arr_t & Ez )
{
    RealType res{ };
#pragma omp parallel for reduction( + \
                                    : res )
    for( size_t i = 0; i < Ex.size( ); i++ )
    {
        res += Ex[ i ] * Ex[ i ] + Ey[ i ] * Ey[ i ] + Ez[ i ] * Ez[ i ];
    }
    return res * 0.5 / Ex.size( );
}

template < int Dim, class MeshType, class RealType >
RealType particles::Test< Dim, MeshType, RealType >::compute_magnetic_energy(
    real_arr_t & Bx, real_arr_t & By, real_arr_t & Bz )
{
    RealType res{ };

#pragma omp parallel for reduction( + \
                                    : res )
    for( size_t i = 0; i < Bx.size( ); i++ )
    {
        res += ( Bx[ i ] * Bx[ i ] + By[ i ] * By[ i ] + Bz[ i ] * Bz[ i ] );
    }
    return res * 0.5 / ( mu * Bx.size( ) );
}

template < int Dim, class MeshType, class RealType >
RealType particles::Test< Dim, MeshType, RealType >::computeKineticEnergy(
    ParticleHolders< MeshType, RealType > & particles, const size_t number_of_cells )
{
    RealType res = 0.;
    for( auto & particle_holder : particles )
    {
        auto size = particle_holder.m_particles.size( );
#pragma omp parallel for reduction( + \
                                    : res )
        for( auto i = 0u; i < size; i++ )
        {
            auto & particle = particle_holder.m_particles[ i ];
            res += particle_holder.m_ms_me * particle.m_v.L2Squared( );
        }
    }
    return 0.5 * res / ( particles.size( ) * ppc * number_of_cells );
}
