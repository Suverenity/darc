#include "Parameters.h"

#include <external/rapidjson/include/rapidjson/document.h>

#include <fstream>
#include <sstream>
#include <string>

#include <iostream>

namespace particles
{
struct JsonParser
{
    template < typename RealType >
    Parameters< RealType > parseParameters( std::string filename )
    {
        std::ifstream file( filename );
        if( !file.is_open( ) )
        {
            throw std::runtime_error( std::string( "could not open json file: " ) + filename );
        }

        std::stringstream sstr;
        sstr << file.rdbuf( );

        rapidjson::Document d;

        std::cout << sstr.str( ) << std::endl;
        d.Parse( sstr.str( ).data( ) );
        Parameters< RealType > pars;
        pars.xmin                  = d[ "xmin" ].GetFloat( );
        pars.xmax                  = d[ "xmax" ].GetFloat( );
        pars.ymin                  = d[ "ymin" ].GetFloat( );
        pars.ymax                  = d[ "ymax" ].GetFloat( );
        pars.numberOfXCells        = d[ "numberOfXCells" ].GetInt( );
        pars.numberOfYCells        = d[ "numberOfYCells" ].GetInt( );
        pars.particlesPerCell      = d[ "particlesPerCell" ].GetInt( );
        pars.dt                    = d[ "dt" ].GetFloat( );
        pars.simulationTime        = d[ "simulationTime" ].GetFloat( );
        pars.saveEachShot          = d[ "saveEachShot" ].GetInt( );
        pars.saveParticlesEachShot = d[ "saveParticlesEachShot" ].GetInt( );
        pars.beta_e_parallel       = d[ "beta_e_parallel" ].GetFloat( );
        pars.beta_p_parallel       = d[ "beta_p_parallel" ].GetFloat( );
        pars.omega_pe_omega_ce     = d[ "omega_pe_omega_ce" ].GetFloat( );
        pars.T_e_per_T_e_par       = d[ "T_e_per_T_e_par" ].GetFloat( );
        pars.T_p_per_T_p_par       = d[ "T_p_per_T_p_par" ].GetFloat( );
        pars.mp_me                 = d[ "mp_me" ].GetFloat( );
        pars.polar_angle           = d[ "polar_angle" ].GetFloat( );
        pars.azimuthal_angle       = d[ "azimuthal_angle" ].GetFloat( );
        pars.iterations            = d[ "iterations" ].GetInt( );
        pars.electromagneticEps    = d[ "electromagneticEps" ].GetFloat( );
        pars.electrostatic         = d[ "electrostatic" ].GetBool( );
        pars.hellinger_init        = d[ "hellinger_init" ].GetBool( );
        pars.save_particles        = d[ "save_particles" ].GetBool( );
        if( d.HasMember( "restart" ) )
        {
            pars.restart              = d[ "restart" ].GetBool( );
            pars.iteration_to_restart = d[ "restart_iter" ].GetUint64( );
        }

        pars.recompute_parameters( );
        return pars;
    }
};
}// namespace particles
