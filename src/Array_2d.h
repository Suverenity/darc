#pragma once

#include <iostream>
#include <vector>

namespace particles
{

// Ordering of elements is meant to be according to
// http://www.fftw.org/fftw3_doc/Multi_002dDimensional-DFTs-of-Real-Data.html#Multi_002dDimensional-DFTs-of-Real-Data
template < typename Type, typename Allocator = std::allocator< Type > >
class Array2d
{
public:
    using value_type     = Type;
    using allocator_type = Allocator;

    explicit Array2d( ) = default;
    Array2d( size_t n0, size_t n1, Type initValue = { } );

    Type * data( )
    {
        return m_array.data( );
    }

    auto begin( )
    {
        return m_array.begin( );
    }

    auto end( )
    {
        return m_array.end( );
    }

    auto size( )
    {
        return m_array.size( );
    }

    Type operator[]( const size_t index ) const;
    Type & operator[]( const size_t index );

    Type operator( )( const size_t n0_idx, const size_t n1_idx ) const;
    Type & operator( )( const size_t n0_idx, const size_t n1_idx );

    Type xy_coord( const size_t x, const size_t y ) const;
    Type & xy_coord( const size_t x, const size_t y );

    friend std::ostream & operator<<( std::ostream & stream, const Array2d< Type, Allocator > & a )
    {

        for( auto y = 0u; y < a.m_ysize; ++y )
        {
            for( auto x = 0u; x < a.m_xsize; ++x )
            {
                stream << "[" << x << "," << y << "]: " << a.xy_coord( x, y ) << " ";
            }
            stream << std::endl;
        }

        return stream;
    }

    std::vector< Type, Allocator > m_array{ };

    size_t m_n0{ }, m_n1{ };
    size_t m_xsize{ }, m_ysize{ };
};
}// namespace particles

template < typename RealType, typename Allocator >
particles::Array2d< RealType, Allocator >::Array2d( size_t n0, size_t n1, RealType initValue )
    : m_array( n0 * n1, initValue )
    , m_n0( n0 )
    , m_n1( n1 )
    , m_xsize( n1 )
    , m_ysize( n0 )
{
}

template < typename RealType, typename Allocator >
RealType particles::Array2d< RealType, Allocator >::operator( )( size_t n0_idx, size_t n1_idx ) const
{
    return m_array[ m_n1 * n0_idx + n1_idx ];
}

template < typename RealType, typename Allocator >
RealType & particles::Array2d< RealType, Allocator >::operator( )( const size_t n0_idx, const size_t n1_idx )
{
    return m_array[ m_n1 * n0_idx + n1_idx ];
}

template < typename RealType, typename Allocator >
RealType particles::Array2d< RealType, Allocator >::operator[]( size_t index ) const
{
    return m_array[ index ];
}

template < typename RealType, typename Allocator >
RealType & particles::Array2d< RealType, Allocator >::operator[]( size_t index )
{
    return m_array[ index ];
}

template < typename RealType, typename Allocator >
RealType particles::Array2d< RealType, Allocator >::xy_coord( size_t x, size_t y ) const
{
    return this->operator( )( y, x );
}

template < typename RealType, typename Allocator >
RealType & particles::Array2d< RealType, Allocator >::xy_coord( size_t x, size_t y )
{
    return this->operator( )( y, x );
}
