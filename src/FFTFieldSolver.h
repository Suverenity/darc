#ifndef PROJECT_FFTFIELDSOLVER_H
#define PROJECT_FFTFIELDSOLVER_H

#include "Fourier.h"
#include "HDFSaver.h"
#include "Parameters.h"
#include "ParticleHolder.h"
#include "Types.h"
#include <complex>

namespace particles
{

//#if defined( _OPENMP )

template < class RealType >
struct thread_vars
{
    thread_vars( size_t size )
        : ay( size )
        , az( size )
        , Mxy( size )
        , Mxz( size )
        , jx( size )
        , jy( size )
        , jz( size )
    {
    }

    void fill_zeros( )
    {
        std::fill( ay.begin( ), ay.end( ), 0 );
        std::fill( az.begin( ), az.end( ), 0 );

        std::fill( jx.begin( ), jx.end( ), 0 );
        std::fill( jy.begin( ), jy.end( ), 0 );
        std::fill( jz.begin( ), jz.end( ), 0 );

        std::fill( Mxy.begin( ), Mxy.end( ), 0 );
        std::fill( Mxz.begin( ), Mxz.end( ), 0 );
    }

    using real_arr_t = Array< 1, RealType >;

    real_arr_t ay;
    real_arr_t az;

    real_arr_t Mxy;
    real_arr_t Mxz;

    real_arr_t jx;
    real_arr_t jy;
    real_arr_t jz;
};

//#endif

template < int Dim,
           typename MeshType,
           typename BoundaryConditions,
           typename ParticlesGridWeighting,
           typename ParticleMover,
           typename RealType >
class FFTFieldSolver
{
};

template < typename MeshType,
           typename BoundaryConditions,
           typename ParticlesGridWeighting,
           typename ParticleMover,
           typename RealType >
class FFTFieldSolver< 1, MeshType, BoundaryConditions, ParticlesGridWeighting, ParticleMover, RealType >
{
public:
#if defined( _OPENMP )

    std::vector< thread_vars< RealType > > m_thread_storages;

#endif

    using real_arr_t    = Array< 1, RealType >;
    using complex_arr_t = ArrayComplex< 1, RealType >;

    FFTFieldSolver( MeshType & mesh,
                    BoundaryConditions & boundaryConditions,
                    ParticlesGridWeighting & particlesToGrid,
                    ParticleMover & mover,
                    real_arr_t & Ex,
                    real_arr_t & Ey,
                    real_arr_t & Ez,
                    real_arr_t & Bx,
                    real_arr_t & By,
                    real_arr_t & Bz,
                    ParticleHolders< MeshType, RealType > & particlesArray,
                    RealType alpha,
                    VectorClass< RealType > B0,
                    RealType dt,
                    size_t iterations,
                    RealType ppc,
                    bool electrostatic,
                    bool hellinger_init,
                    Parameters< RealType > & pars );

    void run( );

    void toFourier_rho( );
    void toFourier_j_a_M( );

    void toReal_El( );
    void toReal_Et_B( );

    bool compareEt( );
    void copyEtToPrev( );

    // solve linear equations in fourier space
    void computeEl( );

    // solve linear equations in fourier space
    void computeEt( );

    // solve linear equations in fourier space
    void computeB( );

    // starting in fourier space but multiple recomputations between real and fourier space
    void iterateEtB( );

    // has to be in real space
    void computeEFromElEt( );

    // adds B0 constant to the m_B arrays real space
    void add_B0( );

    void initialise_a_M( );

    void map_v_dvdt( );

    void map_v_dvdt_openmp( );

    void initialise_a_M_openmp( );

    // private:
public:
    void compute_ks( const MeshType & mesh );

    MeshType & m_mesh;
    BoundaryConditions & m_bc;
    ParticlesGridWeighting & m_particlesToGrid;
    ParticleMover & m_mover;
    ParticleHolders< MeshType, RealType > & m_particles;
    //HDFSaver< 1, MeshType, RealType > m_saver;

    real_arr_t m_rho;

    real_arr_t m_jy;
    real_arr_t m_jz;

    real_arr_t m_ay;
    real_arr_t m_az;

    real_arr_t m_Mxy;
    real_arr_t m_Mxz;

    real_arr_t & m_Ex;
    real_arr_t & m_Ey;
    real_arr_t & m_Ez;

    real_arr_t m_Elx;

    real_arr_t m_Ety;
    real_arr_t m_Etz;

    complex_arr_t m_Ety_prev_complex;
    complex_arr_t m_Etz_prev_complex;

    real_arr_t m_Ety_prev;
    real_arr_t m_Etz_prev;

    real_arr_t & m_Bx;
    real_arr_t & m_By;
    real_arr_t & m_Bz;

    // m_k are FFT wave numbers normalised on given problem
    real_arr_t m_kk;

    Fourier< 1, RealType > m_ftRho;

    Fourier< 1, RealType > m_ftJy;
    Fourier< 1, RealType > m_ftJz;

    Fourier< 1, RealType > m_ftay;
    Fourier< 1, RealType > m_ftaz;

    Fourier< 1, RealType > m_ftMxy;
    Fourier< 1, RealType > m_ftMxz;

    Fourier< 1, RealType > m_ftElx;

    Fourier< 1, RealType > m_ftEty;
    Fourier< 1, RealType > m_ftEtz;

    Fourier< 1, RealType > m_ftBy;
    Fourier< 1, RealType > m_ftBz;

    RealType alpha;
    RealType alpha_2;
    RealType m_dt_inv;
    VectorClass< RealType > B0;
    size_t m_iterations;
    RealType ppc_inv;
    bool electrostatic;
    bool hellinger_init;

    complex_t< RealType > ii{ 0, 1 };

    Parameters< RealType > & pars;
    size_t half_cells;

    size_t iter = 0;
};

template < typename MeshType,
           typename BoundaryConditions,
           typename ParticlesGridWeighting,
           typename ParticleMover,
           typename RealType >
class FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting, ParticleMover, RealType >
{
public:
    using real_arr_t    = typename MeshType::real_arr_t;
    using complex_arr_t = typename Fourier< 2, RealType >::complex_arr_t;

    FFTFieldSolver( MeshType & mesh,
                    BoundaryConditions & boundaryConditions,
                    ParticlesGridWeighting & particlesToGrid,
                    ParticleMover & mover,
                    real_arr_t & Ex,
                    real_arr_t & Ey,
                    real_arr_t & Ez,
                    real_arr_t & Bx,
                    real_arr_t & By,
                    real_arr_t & Bz,
                    ParticleHolders< MeshType, RealType > & particlesArray,
                    RealType alpha,
                    VectorClass< RealType > B0,
                    RealType dt,
                    size_t iterations,
                    RealType ppc,
                    bool electrostatic,
                    bool hellinger_init,
                    Parameters< RealType > & pars );

    void run( );

    void computeEl( );
    void computeB( );
    void computeEt( );

    void init_a_M( );
    void init_et_prev( );

    void rho_to_fourier( );
    void j_to_fourier( );
    void a_M_to_fourier( );

    void a_j_M_from_E_B( );

    void Et_B_to_real( );

    void copyEtToPrev( );
    bool compareEt( );

    void merge_species( );

    void el_to_real( );
    void b_to_real( );
    void et_to_real( );

    void et_el_to_e( );
    void b0_to_b( );

    void compute_wave_numbers( MeshType & mesh );
    void fill_zeroes( complex_arr_t & arr );
    void time_ppc( real_arr_t & arr );

    Parameters< RealType > & pars;

    real_arr_t & Ex;
    real_arr_t & Ey;
    real_arr_t & Ez;

    real_arr_t & Bx;
    real_arr_t & By;
    real_arr_t & Bz;

    real_arr_t rho;

    real_arr_t jx;
    real_arr_t jy;
    real_arr_t jz;

    real_arr_t Elx;
    real_arr_t Ely;

    real_arr_t Etx;
    real_arr_t Ety;
    real_arr_t Etz;

    real_arr_t Etx_prev;
    real_arr_t Ety_prev;
    real_arr_t Etz_prev;

    real_arr_t ax;
    real_arr_t ay;
    real_arr_t az;

    real_arr_t Mxx;
    real_arr_t Mxy;
    real_arr_t Myy;
    real_arr_t Mxz;
    real_arr_t Myz;

    Fourier< 2, RealType > ftRho;

    Fourier< 2, RealType > ftElx;
    Fourier< 2, RealType > ftEly;

    Fourier< 2, RealType > ftEtx;
    Fourier< 2, RealType > ftEty;
    Fourier< 2, RealType > ftEtz;

    Fourier< 2, RealType > ftJx;
    Fourier< 2, RealType > ftJy;
    Fourier< 2, RealType > ftJz;

    Fourier< 2, RealType > ftBx;
    Fourier< 2, RealType > ftBy;
    Fourier< 2, RealType > ftBz;

    Fourier< 2, RealType > ftax;
    Fourier< 2, RealType > ftay;
    Fourier< 2, RealType > ftaz;

    Fourier< 2, RealType > ftMxx;
    Fourier< 2, RealType > ftMxy;
    Fourier< 2, RealType > ftMyy;
    Fourier< 2, RealType > ftMxz;
    Fourier< 2, RealType > ftMyz;

    complex_arr_t Etx_prev_complex;
    complex_arr_t Ety_prev_complex;
    complex_arr_t Etz_prev_complex;

    ParticleHolders< MeshType, RealType > & particles;

    MeshType & mesh;
    ParticlesGridWeighting & particles_grid_weighting;
    ParticleMover & mover;

    // m_k are FFT wave numbers normalised on given problem
    Array< 1, RealType > kk_x;
    Array< 1, RealType > kk_y;
    size_t half_cells_x;
    size_t half_cells_y;
    complex_t< RealType > ii{ 0, 1 };

    size_t index{ };
    HDFSaver< 2, MeshType, RealType > saver;

    RealType alpha;
    RealType alpha_sq;
    RealType m_dt_inv;
    RealType ppc_inv;
    VectorClass< RealType > B0;
};

}// namespace particles

#include "FFTFieldSolver.hpp"
#include "FFTFieldSolver_2d.hpp"

#endif//PROJECT_FFTFIELDSOLVER_H
