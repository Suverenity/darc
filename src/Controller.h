#ifndef PROJECT_CONTROLLER_H
#define PROJECT_CONTROLLER_H

#include <math.h>

#include <algorithm>
#include <iostream>

#include "HDFSaver.h"
#include "Parameters.h"
#include "ParticleGenerator.h"
#include "Test.h"
#include "Types.h"

#include "Mpi.h"

namespace particles
{

template < int Dim, typename MeshType, typename Mover, typename FieldSolver,
           typename BoundaryConditions, typename ParticleToGridRecomputer,
           typename RealType >
class Controller
{
public:
    using real_arr_t = Array< Dim, RealType >;

    Controller( Parameters< RealType > & pars )
        : m_pars( pars )
        , m_mesh( pars )
        , m_Ex( m_mesh.create_arr( ) )
        , m_Ey( m_mesh.create_arr( ) )
        , m_Ez( m_mesh.create_arr( ) )
        , m_Bx( m_mesh.create_arr( ) )
        , m_By( m_mesh.create_arr( ) )
        , m_Bz( m_mesh.create_arr( ) )
        , m_boundary( m_mesh, m_particles )
        , m_particlesToGrid( m_mesh, m_particles, pars.particlesPerCell )
        , m_mover( pars.dt, m_particlesToGrid, m_boundary, m_particles,
                   m_Ex, m_Ey, m_Ez,
                   m_Bx, m_By, m_Bz, pars.particlesPerCell )
        , m_fieldSolver( m_mesh, m_boundary, m_particlesToGrid, m_mover,
                         m_Ex, m_Ey, m_Ez, m_Bx, m_By, m_Bz,
                         m_particles, pars.alpha, pars.B0 * pars.unit_v,
                         pars.dt, pars.iterations, pars.particlesPerCell,
                         pars.electrostatic, pars.hellinger_init, pars )
        , m_test( m_mesh, pars.alpha, pars.particlesPerCell, true )
        , m_dt( pars.dt )
        , timeOfSim( pars.simulationTime )
        , actualTime( 0. )
        , m_saveEachNth( pars.saveEachShot )
        , saveTimestamp( 0 )
    {
        mpi::MPI_handler< real_arr_t >::get_instance( ).create_buffer( m_mesh );

        if( mpi::is_master< real_arr_t >( ) )
        {
            std::cout << "using floating type with size: " << std::to_string( sizeof( RealType ) )
                      << ", probably: " << ( sizeof( RealType ) == 4 ? "float" : "double" ) << std::endl;
        }
        ParticleGenerator< Dim, MeshType, RealType > generator( m_mesh, pars );
        if( !pars.restart )
        {
            m_particles = generator.generate( );
        }
        else
        {
            m_particles = generator.load( );
        }
    }

    Parameters< RealType >
        m_pars;
    MeshType m_mesh;

    real_arr_t m_Ex;
    real_arr_t m_Ey;
    real_arr_t m_Ez;

    real_arr_t m_Bx;
    real_arr_t m_By;
    real_arr_t m_Bz;

    BoundaryConditions m_boundary;
    ParticleToGridRecomputer m_particlesToGrid;
    Mover m_mover;
    FieldSolver m_fieldSolver;
    HDFSaver< Dim, MeshType, RealType > saver;
    Test< Dim, MeshType, RealType > m_test;

    ParticleHolders< MeshType, RealType > m_particles;

    RealType m_dt;
    double timeOfSim{ };
    double actualTime{ };
    size_t m_saveEachNth{ };
    RealType saveTimestamp{ };
    uint64_t iteration{ };
    size_t last_stored_iteration{ };

    // main computation loop, everything should be initialized when calling this
    void run( )
    {
        if( mpi::is_master< real_arr_t >( ) )
        {
            std::cout << "Starting simulation with " << m_particles.size( ) << " species" << std::endl;
        }

        const uint64_t max_iterations = m_pars.simulationTime / m_pars.dt;
        saveTimestamp                 = 0.;

        if( mpi::is_master< real_arr_t >( ) )
        {
            std::cout << "iterations for time:" << m_pars.simulationTime
                      << " with dt: " << m_pars.dt
                      << " to compute:" << max_iterations << std::endl;
        }

        m_boundary.applyParticlesBoundary( );
        //m_particlesToGrid.run( );

        if( m_pars.restart )
        {
            iteration = m_pars.iteration_to_restart + 1;
            if( mpi::is_master< real_arr_t >( ) )
            {
                std::cout << "restarting in iteration:" << m_pars.iteration_to_restart
                          << " with dt: " << m_pars.dt
                          << " to compute:" << max_iterations
                          << " remaining: " << max_iterations - m_pars.iteration_to_restart << std::endl;
            }
        }
        else
        {
            iteration = 0;
        }

        for( ; iteration != max_iterations + 1; ++iteration )
        //for( iteration = 0; iteration != 10; ++iteration )
        {
            actualTime += m_dt;

            m_particlesToGrid.run( );

            if( m_saveEachNth && ( iteration % m_saveEachNth == 0 ) )
            {
                last_stored_iteration = iteration;
                if( mpi::is_master< real_arr_t >( ) )
                {
                    std::cout << "Saving in time: " << actualTime << ", iteration: " << iteration << std::endl;
                    for( auto & holder : m_particles )
                    {
                        saver.saveGridQuantity( holder.m_rho, std::string( "rho_" ) + holder.pname, iteration );
                        saver.saveGridQuantity( holder.m_jx, std::string( "jx_" ) + holder.pname, iteration );
                        saver.saveGridQuantity( holder.m_jy, std::string( "jy_" ) + holder.pname, iteration );
                        saver.saveGridQuantity( holder.m_jz, std::string( "jz_" ) + holder.pname, iteration );
                    }
                }

                for( auto & species : m_particles )
                {
                    auto presure = m_test.computePresure( m_particlesToGrid, species, m_pars.unit_v );
                    if( mpi::is_master< real_arr_t >( ) )
                    {
                        saver.saveGridQuantity( presure.ppar, std::string( "ppar_" ) + species.pname, iteration );
                        saver.saveGridQuantity( presure.pper, std::string( "pper_" ) + species.pname, iteration );
                    }
                }
            }

            m_fieldSolver.run( );

            m_mover.run( );
            m_boundary.applyParticlesBoundary( );

            //m_mover.run_with_map( );
            if( m_pars.save_particles && iteration > 0 && ( iteration % m_pars.saveParticlesEachShot == 0 ) )
            {
                saver.saveParticlesPositions( m_particles, iteration );
                saver.saveParticlesVelocityX( m_particles, iteration );
            }

            if( mpi::is_master< real_arr_t >( ) )
            {
                if( m_saveEachNth && ( iteration % m_saveEachNth == 0 ) )
                {
                    saver.saveGridQuantity( m_Ex, "Ex", iteration );
                    saver.saveGridQuantity( m_Ey, "Ey", iteration );
                    saver.saveGridQuantity( m_Ez, "Ez", iteration );
                    saver.saveGridQuantity( m_Bx, "Bx", iteration );
                    saver.saveGridQuantity( m_By, "By", iteration );
                    saver.saveGridQuantity( m_Bz, "Bz", iteration );

                    saveTimestamp += m_saveEachNth * m_dt;
                    std::cout << "Saved grid values in " << saveTimestamp
                              << std::endl;
                }

                const auto energy = m_test.computeEnergy( m_Ex, m_Ey, m_Ez, m_Bx, m_By, m_Bz, m_particles );
                std::cout << "Energy: " << energy << std::endl;

                if( std::isnan( energy ) || std::isinf( energy ) )
                {
                    throw std::runtime_error( "simulation diverged because of energy nan or inf" );
                }

                std::cout << "Iteration " << iteration << " for time: " << actualTime << " ended"
                          << std::endl
                          << std::endl;
            }
        }
        if( last_stored_iteration != max_iterations )
        {
            if( mpi::is_master< real_arr_t >( ) )
            {
                saver.saveGridQuantity( m_Ex, "Ex", iteration );
                saver.saveGridQuantity( m_Ey, "Ey", iteration );
                saver.saveGridQuantity( m_Ez, "Ez", iteration );
                saver.saveGridQuantity( m_Bx, "Bx", iteration );
                saver.saveGridQuantity( m_By, "By", iteration );
                saver.saveGridQuantity( m_Bz, "Bz", iteration );
                for( auto & holder : m_particles )
                {
                    saver.saveGridQuantity( holder.m_rho, std::string( "rho_" ) + holder.pname, iteration );
                    saver.saveGridQuantity( holder.m_jx, std::string( "jx_" ) + holder.pname, iteration );
                    saver.saveGridQuantity( holder.m_jy, std::string( "jy_" ) + holder.pname, iteration );
                    saver.saveGridQuantity( holder.m_jz, std::string( "jz_" ) + holder.pname, iteration );
                }
            }
            for( auto & species : m_particles )
            {
                auto presure = m_test.computePresure( m_particlesToGrid, species, m_pars.unit_v );
                if( mpi::is_master< real_arr_t >( ) )
                {
                    saver.saveGridQuantity( presure.ppar, std::string( "ppar_" ) + species.pname, iteration );
                    saver.saveGridQuantity( presure.pper, std::string( "pper_" ) + species.pname, iteration );
                }
            }
            if( m_pars.save_particles )
            {
                saver.saveParticlesPositions( m_particles, iteration );
                saver.saveParticlesVelocityX( m_particles, iteration );
            }
        }
    }
};// namespace particles

}// namespace particles

#endif// PROJECT_CONTROLLER_H
