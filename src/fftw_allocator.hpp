#include "fftw_allocator.h"

template <typename Tdata>
Tdata *particles::fftw_allocator<Tdata>::allocate(size_t n) {
#ifdef USE_FLOAT
  return (Tdata *)fftwf_malloc(sizeof(Tdata) * n);
#else
  return (Tdata *)fftw_malloc(sizeof(Tdata) * n);
#endif
}

template <typename Tdata>
void particles::fftw_allocator<Tdata>::deallocate(Tdata *data,
                                                  std::size_t size) {
#ifdef USE_FLOAT
  fftwf_free(data);
#else
  fftw_free(data);
#endif
}
