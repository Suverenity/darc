
#ifndef PROJECT_FROMPARTICLESTOGRID_H
#define PROJECT_FROMPARTICLESTOGRID_H

#include "Mesh.h"
#include "Particle.h"
#include "ParticleHolder.h"
#include "Types.h"

namespace particles
{

template < class RealType >
struct thread_data
{
    thread_data( size_t size )
        : rho( size )
        , jx( size )
        , jy( size )
        , jz( size )
    {
    }

    void reset_zero( )
    {
        std::fill( rho.begin( ), rho.end( ), 0 );
        std::fill( jx.begin( ), jx.end( ), 0 );
        std::fill( jy.begin( ), jy.end( ), 0 );
        std::fill( jz.begin( ), jz.end( ), 0 );
    }

    using real_arr_t = Array< 1, RealType >;

    real_arr_t rho;
    real_arr_t jx;
    real_arr_t jy;
    real_arr_t jz;
};

template < int Dim, typename MeshType, typename RealType >
class FromParticlesToGridFirstOrder;

template < typename MeshType, typename RealType >
class FromParticlesToGridFirstOrder< 1, MeshType, RealType >
{
public:
    using real_arr_t = Array< 1, RealType >;
    FromParticlesToGridFirstOrder(
        MeshType & mesh,
        ParticleHolders< MeshType, RealType > & particles,
        RealType particles_per_cell );

    void run( );

    void runOneParticle( Particle< RealType > & particle, RealType qs_qe );

    void runSpecies( ParticleHolder< MeshType, RealType > & particles );

    void inline runOneParticle( const Particle< RealType > & p,
                                RealType qParticle );

    void inline runOneParticle( const Particle< RealType > & p,
                                RealType qParticle, real_arr_t & rho,
                                real_arr_t & jx, real_arr_t & jy,
                                real_arr_t & jz );

    void inline computeDensity( std::vector< std::vector< RealType > > & n_s );

    using weights_t = std::pair< RealType, RealType >;

    weights_t compute_weights_particle( const Particle< RealType > & p );

    weights_t compute_weights_particle( const Particle< RealType > & p, const indexPair_t & x_idxs );

    void map_to_grid( const Particle< RealType > & p, const weights_t & weights,
                      real_arr_t & grid_quantity, const RealType quantity );

    void map_to_grid( const Particle< RealType > & p, const weights_t & weights, const indexPair_t & indices,
                      real_arr_t & grid_quantity, const RealType quantity );

    RealType weightQuantityOnParticle( Particle< RealType > & particle, real_arr_t & quantityOnGrid );

private:
    MeshType & m_mesh;
    ParticleHolders< MeshType, RealType > & m_particles;
    RealType inversed_ppc;

#if defined( _OPENMP )
    std::vector< thread_data< RealType > > threads_data;
#endif
};

template < typename MeshType, typename RealType >
class FromParticlesToGridFirstOrder< 2, MeshType, RealType >
{
public:
    using real_arr_t = typename MeshType::real_arr_t;

    FromParticlesToGridFirstOrder( MeshType & mesh, ParticleHolders< MeshType, RealType > & particles, RealType ppc )
        : m_mesh( mesh )
        , m_particles( particles )
        , inversed_ppc( 1. / ppc )
    {
    }

    void run( );

    void runSpecies( ParticleHolder< MeshType, RealType > & species );

    void runOneParticle( Particle< RealType > & particle, RealType qs_qe );

    void inline runOneParticle(
        const Particle< RealType > & p,
        RealType qParticle,
        real_arr_t & rho,
        real_arr_t & jx,
        real_arr_t & jy,
        real_arr_t & jz );

    void inline computeDensity( std::vector< std::vector< RealType > > & n_s );

    struct weights_t
    {
        RealType left_bottom;
        RealType right_bottom;
        RealType left_top;
        RealType right_top;
    };

    weights_t compute_weights_particle( const Particle< RealType > & p );

    weights_t compute_weights_particle( const Particle< RealType > & p, const indexPair_t & x_idxs,
                                        const indexPair_t & y_idxs );

    void map_to_grid( const Particle< RealType > & p, const weights_t & weights,
                      real_arr_t & grid_quantity, const RealType quantity );

    void map_to_grid( const Particle< RealType > & p, const weights_t & weights,
                      const indexPair_t & x_indices, const indexPair_t & y_indices,
                      real_arr_t & grid_quantity, const RealType quantity );

    RealType weightQuantityOnParticle( Particle< RealType > & particle, const real_arr_t & quantityOnGrid );

private:
    MeshType & m_mesh;
    ParticleHolders< MeshType, RealType > & m_particles;
    RealType inversed_ppc;
};

}// namespace particles

#include "FromParticlesToGrid_1d.hpp"
#include "FromParticlesToGrid_2d.hpp"

#endif//PROJECT_FROMPARTICLESTOGRID_H
