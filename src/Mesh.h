//
// Created by suverenity on 4/12/18.
//

#ifndef PROJECT_MESH_H
#define PROJECT_MESH_H

#include "Parameters.h"
#include "Types.h"
#include "thread_arrays.h"

namespace particles
{

struct indexPair_t
{
    indexPair_t( size_t _first, size_t _second )
        : first( _first )
        , second( _second )
    {
    }
    size_t first;
    size_t second;
};

template < typename NumberType >
struct interval_t
{
    interval_t( NumberType _min, NumberType _max )
        : min( _min )
        , max( _max )
    {
    }

    NumberType min;
    NumberType max;
};

template < int dim, class RealType >
class Mesh;

template < class RealType >
class Mesh< 1, RealType >
{
public:
    static constexpr int Dim = 1;
    using real_arr_t         = Array< Dim, RealType >;

    Mesh( RealType xMin, RealType xMax, size_t numberOfCells );
    Mesh( Parameters< RealType > & pars );

    indexPair_t computeXIndicesFromPosition( RealType position );
    interval_t< RealType > computeXCellIntervalFromIndex( size_t index );

    real_arr_t create_arr( RealType init_value = { } );
    size_t cells( )
    {
        return m_numberOfCells;
    }

    inline RealType x( const size_t index ) const;

    RealType m_xMin, m_xMax, m_dx, m_dx_inv;
    RealType m_xSize;
    unsigned long m_numberOfCells;
#if defined( _OPENMP )
    thread_arrays_t< RealType > & m_buffers;
#endif
};

template < class RealType >
class Mesh< 2, RealType >
{
public:
    static constexpr int Dim = 2;
    using real_arr_t         = Array< Dim, RealType >;

    Mesh( RealType xMin, RealType xMax, RealType yMin, RealType yMax, size_t numberOfXCells, size_t numberOfYCells );
    Mesh( Parameters< RealType > & pars );

    indexPair_t computeXIndicesFromPosition( RealType position );
    indexPair_t computeYIndicesFromPosition( RealType position );
    interval_t< RealType > computeXCellIntervalFromIndex( size_t index );
    interval_t< RealType > computeYCellIntervalFromIndex( size_t index );

    real_arr_t create_arr( RealType init_value = { } );
    size_t cells( )
    {
        return m_numberOfXCells * m_numberOfYCells;
    }

    inline RealType x( const size_t index ) const;
    inline RealType y( const size_t index ) const;

    RealType m_xMin, m_xMax, m_dx, m_dx_inv;
    RealType m_yMin, m_yMax, m_dy, m_dy_inv;
    RealType m_xSize, m_ySize;

    unsigned long m_numberOfXCells;
    unsigned long m_numberOfYCells;
};

}// namespace particles

#include "Mesh_1d.hpp"
#include "Mesh_2d.hpp"

#endif//PROJECT_MESH_H
