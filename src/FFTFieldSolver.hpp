
#ifndef PROJECT_FFTFIELDSOLVER_HPP
#define PROJECT_FFTFIELDSOLVER_HPP

#include "FFTFieldSolver.h"

#if defined( _OPENMP )
#include <omp.h>
#endif

#include <algorithm>
#include <cmath>
#include <iostream>
#include <math.h>
#include <thread>

#include "FromGridToParticles.h"
#include "FromParticlesToGrid.h"
#include "HDFSaver.h"
#include "Types.h"

namespace particles
{

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
FFTFieldSolver< 1, MeshType, BoundaryConditions, ParticlesGridWeighting, ParticleMover, RealType >::FFTFieldSolver(
    MeshType & mesh,
    BoundaryConditions & boundaryConditions,
    ParticlesGridWeighting & particlesToGrid,
    ParticleMover & mover,
    real_arr_t & Ex,
    real_arr_t & Ey,
    real_arr_t & Ez,
    real_arr_t & Bx,
    real_arr_t & By,
    real_arr_t & Bz,
    ParticleHolders< MeshType, RealType > & particlesArray,
    RealType alpha,
    VectorClass< RealType > B0,
    RealType dt,
    size_t iterations,
    RealType ppc,
    bool electrostatic,
    bool hellinger_init,
    Parameters< RealType > & pars )
    : m_mesh( mesh )
    , m_bc( boundaryConditions )
    , m_particlesToGrid( particlesToGrid )
    , m_mover( mover )
    , m_particles( particlesArray )
    , m_rho( Ex.size( ) )
    , m_jy( Ex.size( ) )
    , m_jz( Ex.size( ) )
    , m_ay( m_mesh.m_numberOfCells )
    , m_az( m_mesh.m_numberOfCells )
    , m_Mxy( m_mesh.m_numberOfCells )
    , m_Mxz( m_mesh.m_numberOfCells )
    , m_Ex( Ex )
    , m_Ey( Ey )
    , m_Ez( Ez )
    , m_Elx( Ex.size( ) )
    , m_Ety( Ey.size( ) )
    , m_Etz( Ez.size( ) )
    , m_Ety_prev_complex( Ey.size( ) )
    , m_Etz_prev_complex( Ez.size( ) )
    , m_Ety_prev( Ey.size( ) )
    , m_Etz_prev( Ez.size( ) )
    , m_Bx( Bx )
    , m_By( By )
    , m_Bz( Bz )
    , m_kk( Ex.size( ) )
    , m_ftRho( m_rho )
    , m_ftJy( m_jy )
    , m_ftJz( m_jz )
    , m_ftay( m_ay )
    , m_ftaz( m_az )
    , m_ftMxy( m_Mxy )
    , m_ftMxz( m_Mxz )
    , m_ftElx( m_Elx )
    , m_ftEty( m_Ety )
    , m_ftEtz( m_Etz )
    , m_ftBy( m_By )
    , m_ftBz( m_Bz )
    , alpha( alpha )
    , alpha_2( alpha * alpha )
    , m_dt_inv( 1. / dt )
    , B0( B0 )
    , m_iterations( iterations )
    , ppc_inv( 1. / ppc )
    , electrostatic( electrostatic )
    , hellinger_init( hellinger_init )
    , pars( pars )
    , half_cells( pars.numberOfXCells / 2 )
{
    if( mpi::is_master< real_arr_t >( ) )
    {
        std::cout << "alpha: " << alpha << ", alpha_sq: " << alpha_2 << std::endl;
    }
    compute_ks( mesh );
    add_B0( );
#if defined( _OPENMP )
    const size_t thread_number = std::thread::hardware_concurrency( );
    m_thread_storages.reserve( thread_number );

    for( size_t i = 0; i != thread_number; ++i )
    {
        m_thread_storages.emplace_back( Ex.size( ) );
    }
#endif
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver< 1, MeshType, BoundaryConditions, ParticlesGridWeighting,
                     ParticleMover, RealType >::toFourier_rho( )
{
    std::fill( m_rho.begin( ), m_rho.end( ), 0 );
    for( auto & holder : m_particles )
    {
#pragma omp parallel for
        for( size_t i = 0; i < holder.m_rho.size( ); ++i )
        {
            m_rho[ i ] += holder.m_rho[ i ];
        }
    }
    //m_bc.apply_for_particle_mesh_value( m_rho );
    m_ftRho.runForward( );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver< 1, MeshType, BoundaryConditions, ParticlesGridWeighting,
                     ParticleMover, RealType >::toFourier_j_a_M( )
{
    m_ftJy.runForward( );
    m_ftJz.runForward( );

    m_ftay.runForward( );
    m_ftaz.runForward( );

    m_ftMxy.runForward( );
    m_ftMxz.runForward( );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver< 1, MeshType, BoundaryConditions, ParticlesGridWeighting,
                     ParticleMover, RealType >::toReal_El( )
{
    m_ftElx.runBackward( );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver< 1, MeshType, BoundaryConditions, ParticlesGridWeighting,
                     ParticleMover, RealType >::toReal_Et_B( )
{
    m_ftEty.runBackward( );
    m_ftEtz.runBackward( );

    m_ftBy.runBackward( );
    m_ftBz.runBackward( );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
bool FFTFieldSolver< 1, MeshType, BoundaryConditions, ParticlesGridWeighting,
                     ParticleMover, RealType >::compareEt( )
{
    double max_diff{ };
    for( size_t i = 0; i != m_Ety.size( ); ++i )
    {
        max_diff = std::max( std::abs( static_cast< double >( m_Ety[ i ] - m_Ety_prev[ i ] ) ), max_diff );
    }

    if( max_diff > pars.electromagneticEps )
    {
        return false;
    }

    for( size_t i = 0; i != m_Ety.size( ); ++i )
    {
        max_diff = std::max( std::abs( static_cast< double >( m_Ety[ i ] - m_Ety_prev[ i ] ) ), max_diff );
    }

    return ( max_diff > pars.electromagneticEps ? false : true );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver< 1, MeshType, BoundaryConditions, ParticlesGridWeighting,
                     ParticleMover, RealType >::copyEtToPrev( )
{
    std::copy( m_Ety.begin( ), m_Ety.end( ), m_Ety_prev.begin( ) );
    std::copy( m_Etz.begin( ), m_Etz.end( ), m_Etz_prev.begin( ) );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver< 1, MeshType, BoundaryConditions, ParticlesGridWeighting,
                     ParticleMover, RealType >::add_B0( )
{
    const auto vec_size = m_Bx.size( );
#pragma omp parallel for
    for( size_t i = 0; i < vec_size; ++i )
    {
        m_Bx[ i ] = B0.x( );// simulation in 1-D cannot compute Bx
        m_By[ i ] += B0.y( );
        m_Bz[ i ] += B0.z( );
    }
}

template < typename mesh_t, typename real_arr_t, typename realtype >
void merge_species_j( real_arr_t & jy,
                      real_arr_t & jz,
                      ParticleHolders< mesh_t, realtype > & holders )
{
    std::fill( jy.begin( ), jy.end( ), 0. );
    std::fill( jz.begin( ), jz.end( ), 0. );
    for( auto & holder : holders )
    {
#pragma omp parallel
        {
#pragma omp for
            for( size_t i = 0; i < jy.size( ); ++i )
            {
                jy[ i ] += holder.m_jy[ i ];
            }
#pragma omp for
            for( size_t i = 0; i < jz.size( ); ++i )
            {
                jz[ i ] += holder.m_jz[ i ];
            }
        }
    }
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver< 1, MeshType, BoundaryConditions, ParticlesGridWeighting,
                     ParticleMover, RealType >::run( )
{
    toFourier_rho( );
    computeEl( );
    toReal_El( );
    if( electrostatic )
    {
        computeEFromElEt( );
        add_B0( );
    }
    else
    {
        // it is not necessary necessary to recompute E and B is it
        // is computed in every iteration
        merge_species_j( m_jy, m_jz, m_particles );
        iterateEtB( );
    }
    iter++;
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver< 1, MeshType, BoundaryConditions, ParticlesGridWeighting,
                     ParticleMover, RealType >::computeEl( )
{
    using namespace std::complex_literals;

    auto & fRho = m_ftRho.m_fourierArr;
    auto & fElx = m_ftElx.m_fourierArr;

#pragma omp parallel for
    for( size_t j = 1; j < fElx.size( ); ++j )
    {
        fElx[ j ] = -ii * fRho[ j ] / m_kk[ j ];
    }

    fElx[ 0 ]          = 0i;
    fElx[ half_cells ] = 0i;
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver<
    1, MeshType, BoundaryConditions, ParticlesGridWeighting,
    ParticleMover, RealType >::map_v_dvdt_openmp( )
{
#if defined( _OPENMP )

    //#pragma omp parallel
    //    {
    for( auto & thread_storage : m_thread_storages )
    {
        thread_storage.fill_zeros( );
        //const auto thread_id = omp_get_thread_num( );
        //m_thread_storages[ thread_id ].fill_zeros( );
    }
    //    }

    for( auto & particle_holder : m_particles )
    {
#pragma omp parallel
        {
            const auto thread_id = omp_get_thread_num( );
            auto & th_stor       = m_thread_storages[ thread_id ];
#pragma omp for
            for( auto partIdx = 0u;
                 partIdx < particle_holder.m_particles.size( );
                 ++partIdx )
            {
                auto & p          = particle_holder.m_particles[ partIdx ];
                const auto v_star = m_mover.computeSpeedForOneParticle(
                    p, particle_holder.m_qs_qe, particle_holder.m_ms_me );
                const auto vNew    = ( 0.5 * ( v_star + p.m_v ) );
                const auto dvdtNew = ( v_star - p.m_v ) * m_dt_inv;

                const auto x = p.m_x.x( );
                const auto weights =
                    m_particlesToGrid.compute_weights_particle( x );

                const auto indices =
                    m_mesh.computeXIndicesFromPosition( x );

                //const auto thread_id = omp_get_thread_num( );
                //auto & th_stor       = m_thread_storages[ thread_id ];
                // TODO optimalizace nasobeni qs_qe pres cela pole -> usetreni
                // poctu operaci vs. opetovny pristup do RAM
                m_particlesToGrid.map_to_grid(
                    x, weights, indices, th_stor.jx, particle_holder.m_qs_qe * vNew.x( ) );
                m_particlesToGrid.map_to_grid(
                    x, weights, indices, th_stor.jy, particle_holder.m_qs_qe * vNew.y( ) );
                m_particlesToGrid.map_to_grid(
                    x, weights, indices, th_stor.jz, particle_holder.m_qs_qe * vNew.z( ) );

                m_particlesToGrid.map_to_grid(
                    x, weights, indices, th_stor.ay, particle_holder.m_qs_qe * dvdtNew.y( ) );
                m_particlesToGrid.map_to_grid(
                    x, weights, indices, th_stor.az, particle_holder.m_qs_qe * dvdtNew.z( ) );

                m_particlesToGrid.map_to_grid(
                    x, weights, indices, th_stor.Mxy, particle_holder.m_qs_qe * vNew.x( ) * vNew.y( ) );
                m_particlesToGrid.map_to_grid(
                    x, weights, indices, th_stor.Mxz, particle_holder.m_qs_qe * vNew.x( ) * vNew.z( ) );
            }
        }
    }

    std::fill( m_jy.begin( ), m_jy.end( ), 0 );
    std::fill( m_jz.begin( ), m_jz.end( ), 0 );

    std::fill( m_ay.begin( ), m_ay.end( ), 0 );
    std::fill( m_az.begin( ), m_az.end( ), 0 );

    std::fill( m_Mxy.begin( ), m_Mxy.end( ), 0 );
    std::fill( m_Mxz.begin( ), m_Mxz.end( ), 0 );

    for( auto & thread_storage : m_thread_storages )
    {
#pragma omp parallel for
        for( size_t i = 0; i < m_Mxy.size( ); ++i )
        {
            m_Mxy[ i ] += thread_storage.Mxy[ i ];
            m_Mxz[ i ] += thread_storage.Mxz[ i ];

            m_ay[ i ] += thread_storage.ay[ i ];
            m_az[ i ] += thread_storage.az[ i ];

            m_jy[ i ] += thread_storage.jy[ i ];
            m_jz[ i ] += thread_storage.jz[ i ];
        }
    }

#pragma omp parallel
    {
#pragma omp for
        for( size_t i = 0; i < m_jy.size( ); ++i )
        {
            m_jy[ i ] *= ppc_inv;
        }
#pragma omp for
        for( size_t i = 0; i < m_jz.size( ); ++i )
        {
            m_jz[ i ] *= ppc_inv;
        }

#pragma omp for
        for( size_t i = 0; i < m_ay.size( ); ++i )
        {
            m_ay[ i ] *= ppc_inv;
        }
#pragma omp for
        for( size_t i = 0; i < m_az.size( ); ++i )
        {
            m_az[ i ] *= ppc_inv;
        }

#pragma omp for
        for( size_t i = 0; i < m_Mxy.size( ); ++i )
        {
            m_Mxy[ i ] *= ppc_inv;
        }
#pragma omp for
        for( size_t i = 0; i < m_Mxz.size( ); ++i )
        {
            m_Mxz[ i ] *= ppc_inv;
        }
    }
    //m_bc.apply_for_particle_mesh_value( m_jy );
    //m_bc.apply_for_particle_mesh_value( m_jz );

    //m_bc.apply_for_particle_mesh_value( m_ay );
    //m_bc.apply_for_particle_mesh_value( m_az );

    //m_bc.apply_for_particle_mesh_value( m_Mxy );
    //m_bc.apply_for_particle_mesh_value( m_Mxz );
#endif
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver<
    1, MeshType, BoundaryConditions, ParticlesGridWeighting,
    ParticleMover, RealType >::initialise_a_M_openmp( )
{
#if defined( _OPENMP )

    // initial values for iteration are: v_init = v of current particle, dvdt = 0
    // j is already precomputed from particles to grid step => a = 0, and compute M

#pragma omp parallel
    {
        const auto thread_idx = ::omp_get_thread_num( );
        m_thread_storages[ thread_idx ].fill_zeros( );
    }

    for( auto & particle_holder : m_particles )
    {
//#pragma omp parallel
#pragma omp parallel for
        for( auto partIdx = 0u;
             partIdx < particle_holder.m_particles.size( );
             ++partIdx )
        {
            const auto & p = particle_holder.m_particles[ partIdx ];
            const auto weights =
                m_particlesToGrid.compute_weights_particle( p.m_x.x( ) );

            const auto indices =
                m_mesh.computeXIndicesFromPosition( p.m_x.x( ) );

            const auto thread_idx = ::omp_get_thread_num( );

            m_particlesToGrid.map_to_grid(
                p.m_x.x( ), weights, indices, m_thread_storages[ thread_idx ].Mxy,
                particle_holder.m_qs_qe * p.vx( ) * p.vy( ) );
            m_particlesToGrid.map_to_grid(
                p.m_x.x( ), weights, indices, m_thread_storages[ thread_idx ].Mxz,
                particle_holder.m_qs_qe * p.vx( ) * p.vz( ) );
        }
    }

    std::fill( m_ay.begin( ), m_ay.end( ), 0 );
    std::fill( m_az.begin( ), m_az.end( ), 0 );
    std::fill( m_Mxy.begin( ), m_Mxy.end( ), 0 );
    std::fill( m_Mxz.begin( ), m_Mxz.end( ), 0 );

    for( auto & thread_storage : m_thread_storages )
    {
#pragma omp parallel for
        for( size_t i = 0; i < m_Mxy.size( ); ++i )
        {
            m_Mxy[ i ] += thread_storage.Mxy[ i ];
            m_Mxz[ i ] += thread_storage.Mxz[ i ];
        }
    }

#pragma omp parallel
    {
#pragma omp for
        for( size_t i = 0; i < m_Mxy.size( ); ++i )
        {
            m_Mxy[ i ] *= ppc_inv;
        }
#pragma omp for
        for( size_t i = 0; i < m_Mxy.size( ); ++i )
        {
            m_Mxy[ i ] *= ppc_inv;
        }
    }

    //m_bc.apply_for_particle_mesh_value( m_Mxy );
    //m_bc.apply_for_particle_mesh_value( m_Mxz );

#endif
}// namespace particles

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver<
    1, MeshType, BoundaryConditions, ParticlesGridWeighting,
    ParticleMover, RealType >::initialise_a_M( )
{
    // initial values for iteration are: v_init = v of current particle, dvdt = 0
    // j is already precomputed from particles to grid step => a = 0, and compute M
    std::fill( m_ay.begin( ), m_ay.end( ), 0 );
    std::fill( m_az.begin( ), m_az.end( ), 0 );
    std::fill( m_Mxy.begin( ), m_Mxy.end( ), 0 );
    std::fill( m_Mxz.begin( ), m_Mxz.end( ), 0 );

    for( auto & particle_holder : m_particles )
    {
        for( auto partIdx = 0u;
             partIdx < particle_holder.m_particles.size( );
             ++partIdx )
        {
            const auto & p = particle_holder.m_particles[ partIdx ];
            const auto weights =
                m_particlesToGrid.compute_weights_particle( p.x( ) );

            const auto indices =
                m_mesh.computeXIndicesFromPosition( p.x( ) );

            m_particlesToGrid.map_to_grid(
                p.m_x.x( ), weights, indices, m_Mxy,
                particle_holder.m_qs_qe * p.vx( ) * p.vy( ) );
            m_particlesToGrid.map_to_grid(
                p.m_x.x( ), weights, indices, m_Mxz,
                particle_holder.m_qs_qe * p.vx( ) * p.vz( ) );
        }
    }

    mpi::all_reduce_sum( m_Mxy );
    mpi::all_reduce_sum( m_Mxz );

    for( size_t i = 0; i < m_Mxy.size( ); ++i )
    {
        m_Mxy[ i ] *= ppc_inv;
    }
    for( size_t i = 0; i < m_Mxz.size( ); ++i )
    {
        m_Mxz[ i ] *= ppc_inv;
    }

    //m_bc.apply_for_particle_mesh_value( m_Mxy );
    //m_bc.apply_for_particle_mesh_value( m_Mxz );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver<
    1, MeshType, BoundaryConditions, ParticlesGridWeighting,
    ParticleMover, RealType >::map_v_dvdt( )
{
    std::fill( m_jy.begin( ), m_jy.end( ), 0 );
    std::fill( m_jz.begin( ), m_jz.end( ), 0 );

    std::fill( m_ay.begin( ), m_ay.end( ), 0 );
    std::fill( m_az.begin( ), m_az.end( ), 0 );

    std::fill( m_Mxy.begin( ), m_Mxy.end( ), 0 );
    std::fill( m_Mxz.begin( ), m_Mxz.end( ), 0 );

    for( auto & particle_holder : m_particles )
    {
        for( auto partIdx = 0u;
             partIdx < particle_holder.m_particles.size( );
             ++partIdx )
        {
            auto & p          = particle_holder.m_particles[ partIdx ];
            const auto v_star = m_mover.computeSpeedForOneParticle(
                p, particle_holder.m_qs_qe, particle_holder.m_ms_me );
            const auto vNew    = ( 0.5 * ( v_star + p.m_v ) );
            const auto dvdtNew = ( v_star - p.m_v ) * m_dt_inv;

            const auto x = p.m_x.x( );
            const auto weights =
                m_particlesToGrid.compute_weights_particle( x );

            const auto indices =
                m_mesh.computeXIndicesFromPosition( x );
            m_particlesToGrid.map_to_grid(
                x, weights, indices, m_jy, particle_holder.m_qs_qe * vNew.y( ) );
            m_particlesToGrid.map_to_grid(
                x, weights, indices, m_jz, particle_holder.m_qs_qe * vNew.z( ) );

            m_particlesToGrid.map_to_grid(
                x, weights, indices, m_ay, particle_holder.m_qs_qe * dvdtNew.y( ) );
            m_particlesToGrid.map_to_grid(
                x, weights, indices, m_az, particle_holder.m_qs_qe * dvdtNew.z( ) );

            m_particlesToGrid.map_to_grid(
                x, weights, indices, m_Mxy, particle_holder.m_qs_qe * vNew.x( ) * vNew.y( ) );
            m_particlesToGrid.map_to_grid(
                x, weights, indices, m_Mxz, particle_holder.m_qs_qe * vNew.x( ) * vNew.z( ) );
        }
    }

    mpi::all_reduce_sum( m_jy );
    mpi::all_reduce_sum( m_jz );

    mpi::all_reduce_sum( m_ay );
    mpi::all_reduce_sum( m_az );

    mpi::all_reduce_sum( m_Mxy );
    mpi::all_reduce_sum( m_Mxz );

    for( size_t i = 0; i < m_jy.size( ); ++i )
    {
        m_jy[ i ] *= ppc_inv;
    }
    for( size_t i = 0; i < m_jz.size( ); ++i )
    {
        m_jz[ i ] *= ppc_inv;
    }

    for( size_t i = 0; i < m_ay.size( ); ++i )
    {
        m_ay[ i ] *= ppc_inv;
    }
    for( size_t i = 0; i < m_az.size( ); ++i )
    {
        m_az[ i ] *= ppc_inv;
    }

    for( size_t i = 0; i < m_Mxy.size( ); ++i )
    {
        m_Mxy[ i ] *= ppc_inv;
    }
    for( size_t i = 0; i < m_Mxz.size( ); ++i )
    {
        m_Mxz[ i ] *= ppc_inv;
    }
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver< 1, MeshType, BoundaryConditions, ParticlesGridWeighting,
                     ParticleMover, RealType >::iterateEtB( )
{
    if( !hellinger_init )
    {
#if defined( _OPENMP )
        initialise_a_M_openmp( );
        //initialise_a_M( );
#else
        initialise_a_M( );
#endif
        toFourier_j_a_M( );

        ////set previous Ety Etz in complex to zero
        constexpr complex_t< RealType > zero_complex{ };
        std::fill( m_Ety_prev_complex.begin( ),
                   m_Ety_prev_complex.end( ), zero_complex );
        std::fill( m_Etz_prev_complex.begin( ),
                   m_Etz_prev_complex.end( ), zero_complex );

        // compute initial values of fields in fourier space
        computeEt( );

        computeB( );

        // convert them to real space
        toReal_Et_B( );
        copyEtToPrev( );
        computeEFromElEt( );
        add_B0( );

        //m_bc.applyFieldBoundaries( );
    }

    for( size_t iteration = 0; iteration != m_iterations; ++iteration )
    {

#if defined( _OPENMP )
        map_v_dvdt_openmp( );
#else
        map_v_dvdt( );
#endif
        toFourier_j_a_M( );
        //copies computed values to previous Et
        computeEt( );
        computeB( );
        toReal_Et_B( );

        computeEFromElEt( );
        add_B0( );

        if( compareEt( ) )
        {
            if( mpi::is_master< real_arr_t >( ) )
            {
                std::cout << "iteration suceeded after: " << iteration << std::endl;
            }
            break;
        }
        copyEtToPrev( );

        if( mpi::is_master< real_arr_t >( ) )
        {
            std::cout << "iteration: " << iteration << std::endl;
        }
    }
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver< 1, MeshType, BoundaryConditions, ParticlesGridWeighting,
                     ParticleMover, RealType >::computeEt( )
{
    const auto & fay = m_ftay.m_fourierArr;
    const auto & faz = m_ftaz.m_fourierArr;

    const auto & fMxy = m_ftMxy.m_fourierArr;
    const auto & fMxz = m_ftMxz.m_fourierArr;

    auto & fEty = m_ftEty.m_fourierArr;
    auto & fEtz = m_ftEtz.m_fourierArr;

    const RealType plasma_frequency_sq = ( 1. + 1. / pars.mp_me );
#pragma omp parallel for
    for( size_t index = 1; index < m_Ety.size( ); ++index )
    {
        using namespace std::complex_literals;
        const auto k_norm_sq = m_kk[ index ] * m_kk[ index ];

        // uu Tensor is symmetric!
        const auto ftdjydt = fay[ index ] - ii * m_kk[ index ] * fMxy[ index ];
        const auto ftdjzdt = faz[ index ] - ii * m_kk[ index ] * fMxz[ index ];

        // // no convergency member
        //fEty[ index ] = -alpha_2 / k_norm_sq * ftdjydt;
        //fEtz[ index ] = -alpha_2 / k_norm_sq * ftdjzdt;

        // no ion_frequency
        //const auto koef = -alpha_2 / ( k_norm_sq + alpha_2 );
        //fEty[ index ]   = koef * ( ftdjydt - m_Ety_prev_complex[ index ] );
        //fEtz[ index ]   = koef * ( ftdjzdt - m_Etz_prev_complex[ index ] );

        const auto koef = -alpha_2 / ( k_norm_sq + plasma_frequency_sq * alpha_2 );
        fEty[ index ]   = koef * ( ftdjydt - plasma_frequency_sq * m_Ety_prev_complex[ index ] );
        fEtz[ index ]   = koef * ( ftdjzdt - plasma_frequency_sq * m_Etz_prev_complex[ index ] );

        m_Ety_prev_complex[ index ] = fEty[ index ];
        m_Etz_prev_complex[ index ] = fEtz[ index ];
    }

    fEty[ 0 ]                        = { };
    fEtz[ 0 ]                        = { };
    m_Ety_prev_complex[ half_cells ] = { };
    m_Etz_prev_complex[ half_cells ] = { };
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver< 1, MeshType, BoundaryConditions, ParticlesGridWeighting,
                     ParticleMover, RealType >::computeB( )
{
    using namespace std::complex_literals;
    auto & fJy = m_ftJy.m_fourierArr;
    auto & fJz = m_ftJz.m_fourierArr;

    auto & fBy = m_ftBy.m_fourierArr;
    auto & fBz = m_ftBz.m_fourierArr;

#pragma omp parallel for
    for( unsigned int index = 1; index < m_Bx.size( ); ++index )
    {
        fBy[ index ] = -alpha_2 * ii * fJz[ index ] / m_kk[ index ];
        fBz[ index ] = alpha_2 * ii * fJy[ index ] / m_kk[ index ];
    }

    fBy[ 0 ]          = 0i;
    fBz[ 0 ]          = 0i;
    fBy[ half_cells ] = 0i;
    fBz[ half_cells ] = 0i;
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver< 1, MeshType, BoundaryConditions, ParticlesGridWeighting,
                     ParticleMover, RealType >::computeEFromElEt( )
{
#pragma omp parallel for
    for( auto i = 0u; i < m_Ex.size( ); ++i )
    {
        m_Ex[ i ] = m_Elx[ i ];
        m_Ey[ i ] = m_Ety[ i ];
        m_Ez[ i ] = m_Etz[ i ];
    }
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void FFTFieldSolver< 1, MeshType, BoundaryConditions, ParticlesGridWeighting,
                     ParticleMover, RealType >::compute_ks( const MeshType & mesh )
{
    m_kk.resize( m_rho.size( ) );
    std::iota( m_kk.begin( ), m_kk.end( ), 0 );

    const auto size = m_kk.size( );
    for( auto & k : m_kk )
    {
        k *= RealType( 2 * M_PI ) / ( mesh.m_xMax - mesh.m_xMin );
    }
    for( size_t i = half_cells + 1; i != m_kk.size( ); ++i )
    {
        m_kk[ i ] = -m_kk[ size - i ];
    }
    m_kk[ 0 ] = 1;
}

}// namespace particles

#endif// PROJECT_FFTFIELDSOLVER_HPP
