#ifndef PROJECT_MESH2D_HPP
#define PROJECT_MESH2D_HPP

#include "Mesh.h"
#include <cmath>

namespace particles
{

template < class RealType >
Mesh< 2, RealType >::Mesh( RealType xMin, RealType xMax, RealType yMin, RealType yMax, size_t numberOfXCells, size_t numberOfYCells )
    : m_xMin( xMin )
    , m_xMax( xMax )
    , m_yMin( yMin )
    , m_yMax( yMax )
    , m_xSize( xMax - xMin )
    , m_ySize( yMax - yMin )
    , m_numberOfXCells( numberOfXCells )
    , m_numberOfYCells( numberOfYCells )
{
    m_dx     = ( m_xMax - m_xMin ) / m_numberOfXCells;
    m_dy     = ( m_yMax - m_yMin ) / m_numberOfYCells;
    m_dx_inv = RealType( 1 ) / m_dx;
    m_dy_inv = RealType( 1 ) / m_dy;
    std::cout << "creating grid x: " << m_xMin << ", " << m_xMax << ", cells: " << m_numberOfXCells << ", dx: " << m_dx << std::endl;
    std::cout << "creating grid y: " << m_yMin << ", " << m_yMax << ", cells: " << m_numberOfYCells << ", dy: " << m_dy << std::endl;
}

template < class RealType >
Mesh< 2, RealType >::Mesh( Parameters< RealType > & pars )
    : Mesh( pars.xmin, pars.xmax, pars.ymin, pars.ymax, pars.numberOfXCells, pars.numberOfYCells )
{
}

template < class RealType >
indexPair_t Mesh< 2, RealType >::computeXIndicesFromPosition( RealType position )
{
    const auto i = size_t( ( position - m_xMin ) * m_dx_inv );

    // when float precision fails it can happen that particle position is == m_numberOfXCells which
    // causes problems so this check is necessary and basically says that the error is so small that
    // we can put the particle to the initial point. It would cause memory leak instead
    // TODO can be optimalised that these ifs are elsewhere?
    if( i >= m_numberOfXCells )
    {
        return { 0, 0 };
    }
    assert( i >= 0 && i < m_numberOfXCells );
    return { i, ( i + 1 ) >= m_numberOfXCells ? 0 : i + 1 };
}

template < class RealType >
indexPair_t Mesh< 2, RealType >::computeYIndicesFromPosition( RealType position )
{
    const auto i = size_t( ( position - m_yMin ) * m_dy_inv );

    // when float precision fails it can happen that particle position is == m_numberOfXCells which
    // causes problems so this check is necessary and basically says that the error is so small that
    // we can put the particle to the initial point. It would cause memory leak instead
    // TODO can be optimalised that these ifs are elsewhere?
    if( i >= m_numberOfYCells )
    {
        return { 0, 0 };
    }
    assert( i >= 0 && i < m_numberOfYCells );
    return { i, ( i + 1 ) >= m_numberOfYCells ? 0 : i + 1 };
}

template < class RealType >
interval_t< RealType > Mesh< 2, RealType >::computeXCellIntervalFromIndex( size_t index )
{
    return { x( index ), x( index + 1 ) };
}

template < class RealType >
interval_t< RealType > Mesh< 2, RealType >::computeYCellIntervalFromIndex( size_t index )
{
    return { y( index ), y( index + 1 ) };
}

template < class RealType >
RealType Mesh< 2, RealType >::x( const size_t index ) const
{
    const RealType res = m_xMin + m_dx * index;
    return res;
}

template < class RealType >
RealType Mesh< 2, RealType >::y( const size_t index ) const
{
    const RealType res = m_yMin + m_dy * index;
    return res;
}

template < class RealType >
typename Mesh< 2, RealType >::real_arr_t Mesh< 2, RealType >::create_arr( RealType init_value )
{
    return real_arr_t( m_numberOfYCells, m_numberOfXCells, init_value );
}

}// namespace particles

#endif//PROJECT_MESH1D_HPP
