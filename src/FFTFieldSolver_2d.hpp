#include "FFTFieldSolver.h"

#include "Mpi.h"

#include <algorithm>
#include <numeric>

template < typename MeshType, typename BoundaryConditions, typename ParticlesGridWeighting, typename ParticleMover, typename RealType >
particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting, ParticleMover, RealType >::FFTFieldSolver(
    MeshType & mesh,
    BoundaryConditions & boundaryConditions,
    ParticlesGridWeighting & particlesToGrid,
    ParticleMover & mover,
    real_arr_t & Ex,
    real_arr_t & Ey,
    real_arr_t & Ez,
    real_arr_t & Bx,
    real_arr_t & By,
    real_arr_t & Bz,
    ParticleHolders< MeshType, RealType > & particlesArray,
    RealType alpha,
    VectorClass< RealType > B0,
    RealType dt,
    size_t iterations,
    RealType ppc,
    bool electrostatic,
    bool hellinger_init,
    Parameters< RealType > & pars )

    : pars( pars )

    , Ex( Ex )
    , Ey( Ey )
    , Ez( Ez )

    , Bx( Bx )
    , By( By )
    , Bz( Bz )

    , rho( mesh.create_arr( ) )

    , jx( mesh.create_arr( ) )
    , jy( mesh.create_arr( ) )
    , jz( mesh.create_arr( ) )

    , Elx( mesh.create_arr( ) )
    , Ely( mesh.create_arr( ) )

    , Etx( mesh.create_arr( ) )
    , Ety( mesh.create_arr( ) )
    , Etz( mesh.create_arr( ) )

    , Etx_prev( mesh.create_arr( ) )
    , Ety_prev( mesh.create_arr( ) )
    , Etz_prev( mesh.create_arr( ) )

    , ax( mesh.create_arr( ) )
    , ay( mesh.create_arr( ) )
    , az( mesh.create_arr( ) )

    , Mxx( mesh.create_arr( ) )
    , Mxy( mesh.create_arr( ) )
    , Myy( mesh.create_arr( ) )
    , Mxz( mesh.create_arr( ) )
    , Myz( mesh.create_arr( ) )

    , ftRho( rho )

    , ftElx( Elx )
    , ftEly( Ely )

    , ftEtx( Etx )
    , ftEty( Ety )
    , ftEtz( Etz )

    , ftJx( jx )
    , ftJy( jy )
    , ftJz( jz )

    , ftBx( Bx )
    , ftBy( By )
    , ftBz( Bz )

    , ftax( ax )
    , ftay( ay )
    , ftaz( az )

    , ftMxx( Mxx )
    , ftMxy( Mxy )
    , ftMyy( Myy )
    , ftMxz( Mxz )
    , ftMyz( Myz )

    , Etx_prev_complex( ftEtx.m_fourierArr.m_n0, ftEtx.m_fourierArr.m_n1 )
    , Ety_prev_complex( ftEty.m_fourierArr.m_n0, ftEty.m_fourierArr.m_n1 )
    , Etz_prev_complex( ftEtz.m_fourierArr.m_n0, ftEtz.m_fourierArr.m_n1 )

    , particles( particlesArray )

    , mesh( mesh )
    , particles_grid_weighting( particlesToGrid )
    , mover( mover )

    , kk_x( pars.numberOfXCells )
    , kk_y( pars.numberOfYCells )

    , half_cells_x( mesh.m_numberOfXCells / 2 )
    , half_cells_y( mesh.m_numberOfYCells / 2 )

    , alpha( alpha )
    , alpha_sq( alpha * alpha )
    , m_dt_inv( RealType( 1. ) / dt )
    , ppc_inv( RealType( 1. ) / pars.particlesPerCell )
    , B0( B0 )

{
    compute_wave_numbers( mesh );
}

template < typename MeshType, typename BoundaryConditions, typename ParticlesGridWeighting, typename ParticleMover, typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting, ParticleMover, RealType >::run( )
{
    merge_species( );
    rho_to_fourier( );
    computeEl( );
    el_to_real( );

    if( !pars.electrostatic )
    {
        //  compute initial Electric and magnetic field
        init_a_M( );
        init_et_prev( );

        a_M_to_fourier( );
        j_to_fourier( );

        computeB( );
        computeEt( );

        et_to_real( );
        copyEtToPrev( );

        b_to_real( );

        et_el_to_e( );
        b0_to_b( );

        for( size_t iteration = 0; iteration != pars.iterations; ++iteration )
        {
            if( mpi::is_master< real_arr_t >( ) )
            {
                std::cout << "iteration: " << iteration << " started" << std::endl;
            }
            a_j_M_from_E_B( );

            a_M_to_fourier( );
            j_to_fourier( );

            computeB( );
            computeEt( );

            b_to_real( );
            et_to_real( );

            et_el_to_e( );
            b0_to_b( );

            if( compareEt( ) )
            {
                if( mpi::is_master< real_arr_t >( ) )
                {
                    std::cout << "iteration suceeded after: " << iteration << " iterations" << std::endl;
                }
                break;
            }

            copyEtToPrev( );

            if( mpi::is_master< real_arr_t >( ) )
            {
                std::cout << "iteration: " << iteration << " done" << std::endl;
            }
        }
    }
    else
    {
        et_el_to_e( );
    }

    index++;
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::compute_wave_numbers( MeshType & mesh )
{
    std::iota( kk_x.begin( ), kk_x.end( ), 0 );
    std::iota( kk_y.begin( ), kk_y.end( ), 0 );

    const auto size_x = kk_x.size( );
    for( auto & k : kk_x )
    {
        k *= RealType( 2 * M_PI ) / ( mesh.m_xMax - mesh.m_xMin );
    }
    for( size_t i = half_cells_x + 1; i != kk_x.size( ); ++i )
    {
        kk_x[ i ] = -kk_x[ size_x - i ];
    }

    const auto size_y = kk_y.size( );
    for( auto & k : kk_y )
    {
        k *= RealType( 2 * M_PI ) / ( mesh.m_yMax - mesh.m_yMin );
    }
    for( size_t i = half_cells_y + 1; i != kk_y.size( ); ++i )
    {
        kk_y[ i ] = -kk_y[ size_y - i ];
    }
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::fill_zeroes( complex_arr_t & arr )
{
    using namespace std::complex_literals;
    arr.xy_coord( 0, 0 ) = 0i;

    for( size_t x = 0; x != arr.m_xsize; ++x )
    {
        arr.xy_coord( x, half_cells_y ) = 0i;
    }

    for( size_t y = 0; y != arr.m_ysize; ++y )
    {
        arr.xy_coord( half_cells_x, y ) = 0i;
    }
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::time_ppc( real_arr_t & arr )
{
    for( auto & v : arr )
    {
        v *= ppc_inv;
    }
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::init_a_M( )
{
    std::fill( ax.begin( ), ax.end( ), 0 );
    std::fill( ay.begin( ), ay.end( ), 0 );
    std::fill( az.begin( ), az.end( ), 0 );

    std::fill( Mxx.begin( ), Mxx.end( ), 0 );
    std::fill( Mxy.begin( ), Mxy.end( ), 0 );
    std::fill( Myy.begin( ), Myy.end( ), 0 );
    std::fill( Mxz.begin( ), Mxz.end( ), 0 );
    std::fill( Myz.begin( ), Myz.end( ), 0 );

    for( auto & species : particles )
    {
        for( auto & p : species.m_particles )
        {
            const auto x_idxs  = mesh.computeXIndicesFromPosition( p.x( ) );
            const auto y_idxs  = mesh.computeYIndicesFromPosition( p.y( ) );
            const auto weights = particles_grid_weighting.compute_weights_particle( p, x_idxs, y_idxs );

            particles_grid_weighting.map_to_grid( p, weights, x_idxs, y_idxs, Mxx, species.m_qs_qe * p.vx( ) * p.vx( ) );
            particles_grid_weighting.map_to_grid( p, weights, x_idxs, y_idxs, Mxy, species.m_qs_qe * p.vx( ) * p.vy( ) );
            particles_grid_weighting.map_to_grid( p, weights, x_idxs, y_idxs, Myy, species.m_qs_qe * p.vy( ) * p.vy( ) );
            particles_grid_weighting.map_to_grid( p, weights, x_idxs, y_idxs, Mxz, species.m_qs_qe * p.vx( ) * p.vz( ) );
            particles_grid_weighting.map_to_grid( p, weights, x_idxs, y_idxs, Myz, species.m_qs_qe * p.vy( ) * p.vz( ) );
        }
    }

    mpi::all_reduce_sum( Mxx );
    mpi::all_reduce_sum( Mxy );
    mpi::all_reduce_sum( Myy );
    mpi::all_reduce_sum( Mxz );
    mpi::all_reduce_sum( Myz );

    time_ppc( Mxx );
    time_ppc( Mxy );
    time_ppc( Myy );
    time_ppc( Mxz );
    time_ppc( Myz );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::init_et_prev( )
{
    using namespace std::complex_literals;
    std::fill( Etx_prev_complex.begin( ), Etx_prev_complex.end( ), 0i );
    std::fill( Ety_prev_complex.begin( ), Ety_prev_complex.end( ), 0i );
    std::fill( Etz_prev_complex.begin( ), Etz_prev_complex.end( ), 0i );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::rho_to_fourier( )
{
    ftRho.runForward( );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::j_to_fourier( )
{
    ftJx.runForward( );
    ftJy.runForward( );
    ftJz.runForward( );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::a_M_to_fourier( )
{
    ftax.runForward( );
    ftay.runForward( );
    ftaz.runForward( );

    ftMxx.runForward( );
    ftMxy.runForward( );
    ftMyy.runForward( );
    ftMxz.runForward( );
    ftMyz.runForward( );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::a_j_M_from_E_B( )
{
    std::fill( ax.begin( ), ax.end( ), 0 );
    std::fill( ay.begin( ), ay.end( ), 0 );
    std::fill( az.begin( ), az.end( ), 0 );

    std::fill( Mxx.begin( ), Mxx.end( ), 0 );
    std::fill( Mxy.begin( ), Mxy.end( ), 0 );
    std::fill( Myy.begin( ), Myy.end( ), 0 );
    std::fill( Mxz.begin( ), Mxz.end( ), 0 );
    std::fill( Myz.begin( ), Myz.end( ), 0 );

    std::fill( jx.begin( ), jx.end( ), 0 );
    std::fill( jy.begin( ), jy.end( ), 0 );
    std::fill( jz.begin( ), jz.end( ), 0 );

    for( auto & species : particles )
    {
        for( auto & p : species.m_particles )
        {
            const auto v_star   = mover.computeSpeedForOneParticle( p, species.m_qs_qe, species.m_ms_me );
            const auto v_new    = ( 0.5 * ( v_star + p.m_v ) );
            const auto dvdt_new = ( v_star - p.m_v ) * m_dt_inv;

            const auto weights = particles_grid_weighting.compute_weights_particle( p );

            const auto x_idxs = mesh.computeXIndicesFromPosition( p.x( ) );
            const auto y_idxs = mesh.computeYIndicesFromPosition( p.y( ) );

            particles_grid_weighting.map_to_grid( p, weights, x_idxs, y_idxs, jx, species.m_qs_qe * v_new.x( ) );
            particles_grid_weighting.map_to_grid( p, weights, x_idxs, y_idxs, jy, species.m_qs_qe * v_new.y( ) );
            particles_grid_weighting.map_to_grid( p, weights, x_idxs, y_idxs, jz, species.m_qs_qe * v_new.z( ) );

            particles_grid_weighting.map_to_grid( p, weights, x_idxs, y_idxs, ax, species.m_qs_qe * dvdt_new.x( ) );
            particles_grid_weighting.map_to_grid( p, weights, x_idxs, y_idxs, ay, species.m_qs_qe * dvdt_new.y( ) );
            particles_grid_weighting.map_to_grid( p, weights, x_idxs, y_idxs, az, species.m_qs_qe * dvdt_new.z( ) );

            particles_grid_weighting.map_to_grid( p, weights, x_idxs, y_idxs, Mxx, species.m_qs_qe * v_new.x( ) * v_new.x( ) );
            particles_grid_weighting.map_to_grid( p, weights, x_idxs, y_idxs, Mxy, species.m_qs_qe * v_new.x( ) * v_new.y( ) );
            particles_grid_weighting.map_to_grid( p, weights, x_idxs, y_idxs, Myy, species.m_qs_qe * v_new.y( ) * v_new.y( ) );
            particles_grid_weighting.map_to_grid( p, weights, x_idxs, y_idxs, Mxz, species.m_qs_qe * v_new.x( ) * v_new.z( ) );
            particles_grid_weighting.map_to_grid( p, weights, x_idxs, y_idxs, Myz, species.m_qs_qe * v_new.y( ) * v_new.z( ) );
        }
    }

    mpi::all_reduce_sum( jx );
    mpi::all_reduce_sum( jy );
    mpi::all_reduce_sum( jz );

    mpi::all_reduce_sum( ax );
    mpi::all_reduce_sum( ay );
    mpi::all_reduce_sum( az );

    mpi::all_reduce_sum( Mxx );
    mpi::all_reduce_sum( Mxy );
    mpi::all_reduce_sum( Myy );
    mpi::all_reduce_sum( Mxz );
    mpi::all_reduce_sum( Myz );

    time_ppc( ax );
    time_ppc( ay );
    time_ppc( az );

    time_ppc( jx );
    time_ppc( jy );
    time_ppc( jz );

    time_ppc( Mxx );
    time_ppc( Mxy );
    time_ppc( Myy );
    time_ppc( Mxz );
    time_ppc( Myz );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::Et_B_to_real( )
{
    ftEtx.runBackward( );
    ftEty.runBackward( );
    ftEtz.runBackward( );

    ftBx.runBackward( );
    ftBy.runBackward( );
    ftBz.runBackward( );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::copyEtToPrev( )
{
    std::copy( Etx.begin( ), Etx.end( ), Etx_prev.begin( ) );
    std::copy( Ety.begin( ), Ety.end( ), Ety_prev.begin( ) );
    std::copy( Etz.begin( ), Etz.end( ), Etz_prev.begin( ) );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
bool particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::compareEt( )
{
    double max_diff{ };
    for( size_t i = 0; i != Etx.size( ); ++i )
    {
        max_diff = std::max( std::abs( static_cast< double >( Etx[ i ] - Etx_prev[ i ] ) ), max_diff );
    }
    if( max_diff > pars.electromagneticEps )
    {
        //std::cout << "failing with diff: " << max_diff << std::endl;
        return false;
    }

    for( size_t i = 0; i != Ety.size( ); ++i )
    {
        max_diff = std::max( std::abs( static_cast< double >( Ety[ i ] - Ety_prev[ i ] ) ), max_diff );
    }
    if( max_diff > pars.electromagneticEps )
    {
        //std::cout << "failing with diff: " << max_diff << std::endl;
        return false;
    }

    for( size_t i = 0; i != Etz.size( ); ++i )
    {
        max_diff = std::max( static_cast< double >( std::abs( Etz[ i ] - Etz_prev[ i ] ) ), max_diff );
    }
    //std::cout << "diff: " << max_diff << std::endl;
    return ( max_diff > pars.electromagneticEps ) ? false : true;
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::merge_species( )
{
    std::fill( rho.begin( ), rho.end( ), 0 );
    std::fill( jx.begin( ), jx.end( ), 0 );
    std::fill( jy.begin( ), jy.end( ), 0 );
    std::fill( jz.begin( ), jz.end( ), 0 );

    for( auto & species : particles )
    {
        for( size_t i = 0; i < species.m_rho.size( ); ++i )
        {
            rho[ i ] += species.m_rho[ i ];
        }
        for( size_t i = 0; i < species.m_jx.size( ); ++i )
        {
            jx[ i ] += species.m_jx[ i ];
        }
        for( size_t i = 0; i < species.m_jx.size( ); ++i )
        {
            jy[ i ] += species.m_jy[ i ];
        }
        for( size_t i = 0; i < species.m_jx.size( ); ++i )
        {
            jz[ i ] += species.m_jz[ i ];
        }
    }
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::computeEl( )
{
    using namespace std::complex_literals;

    const auto & ft_rho = ftRho.m_fourierArr;

    auto & ft_elx = ftElx.m_fourierArr;
    auto & ft_ely = ftEly.m_fourierArr;

    for( size_t y = 0; y != ft_rho.m_ysize; ++y )
    {
        for( size_t x = 0; x != ft_rho.m_xsize; ++x )
        {
            const auto k_sq = RealType( 1 ) / ( kk_x[ x ] * kk_x[ x ] + kk_y[ y ] * kk_y[ y ] );

            ft_elx.xy_coord( x, y ) = -ii * k_sq * kk_x[ x ] * ft_rho.xy_coord( x, y );
            ft_ely.xy_coord( x, y ) = -ii * k_sq * kk_y[ y ] * ft_rho.xy_coord( x, y );
        }
    }

    fill_zeroes( ft_elx );
    fill_zeroes( ft_ely );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::computeB( )
{
    using namespace std::complex_literals;

    const auto & ft_jx = ftJx.m_fourierArr;
    const auto & ft_jy = ftJy.m_fourierArr;
    const auto & ft_jz = ftJz.m_fourierArr;

    auto & ft_bx = ftBx.m_fourierArr;
    auto & ft_by = ftBy.m_fourierArr;
    auto & ft_bz = ftBz.m_fourierArr;

    for( size_t y = 0; y != ft_jx.m_ysize; ++y )
    {
        for( size_t x = 0; x != ft_jx.m_xsize; ++x )
        {
            const auto i_alpha_sq_k_sq = ii * alpha_sq / ( kk_x[ x ] * kk_x[ x ] + kk_y[ y ] * kk_y[ y ] );

            ft_bx.xy_coord( x, y ) = i_alpha_sq_k_sq * kk_y[ y ] * ft_jz.xy_coord( x, y );
            ft_by.xy_coord( x, y ) = -i_alpha_sq_k_sq * kk_x[ x ] * ft_jz.xy_coord( x, y );
            ft_bz.xy_coord( x, y ) = i_alpha_sq_k_sq * ( kk_x[ x ] * ft_jy.xy_coord( x, y ) - kk_y[ y ] * ft_jx.xy_coord( x, y ) );
        }
    }

    fill_zeroes( ft_bx );
    fill_zeroes( ft_by );
    fill_zeroes( ft_bz );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::computeEt( )
{
    using namespace std::complex_literals;

    const auto & fax = ftax.m_fourierArr;
    const auto & fay = ftay.m_fourierArr;
    const auto & faz = ftaz.m_fourierArr;

    const auto & fMxx = ftMxx.m_fourierArr;
    const auto & fMxy = ftMxy.m_fourierArr;
    const auto & fMyy = ftMyy.m_fourierArr;
    const auto & fMxz = ftMxz.m_fourierArr;
    const auto & fMyz = ftMyz.m_fourierArr;

    auto & fEtx = ftEtx.m_fourierArr;
    auto & fEty = ftEty.m_fourierArr;
    auto & fEtz = ftEtz.m_fourierArr;

    const RealType plasma_frequency_sq = 1 + 1. / pars.mp_me;

    for( size_t y = 0; y != fEtx.m_ysize; ++y )
    {
        for( size_t x = 0; x != fEtx.m_xsize; ++x )
        {
            const auto fdjdtx = fax.xy_coord( x, y ) - ii * ( kk_x[ x ] * fMxx.xy_coord( x, y ) + kk_y[ y ] * fMxy.xy_coord( x, y ) );
            const auto fdjdty = fay.xy_coord( x, y ) - ii * ( kk_x[ x ] * fMxy.xy_coord( x, y ) + kk_y[ y ] * fMyy.xy_coord( x, y ) );
            const auto fdjdtz = faz.xy_coord( x, y ) - ii * ( kk_x[ x ] * fMxz.xy_coord( x, y ) + kk_y[ y ] * fMyz.xy_coord( x, y ) );

            const RealType k_sq     = kk_x[ x ] * kk_x[ x ] + kk_y[ y ] * kk_y[ y ];
            const RealType k_sq_inv = RealType( 1. ) / k_sq;

            const auto fdjtdtx = fdjdtx - kk_x[ x ] * k_sq_inv * ( fdjdtx * kk_x[ x ] + fdjdty * kk_y[ y ] );
            const auto fdjtdty = fdjdty - kk_y[ y ] * k_sq_inv * ( fdjdtx * kk_x[ x ] + fdjdty * kk_y[ y ] );
            const auto fdjtdtz = fdjdtz;

            const RealType koef = -alpha_sq / ( plasma_frequency_sq * alpha_sq + k_sq );

            fEtx.xy_coord( x, y ) = koef * ( fdjtdtx - plasma_frequency_sq * Etx_prev_complex.xy_coord( x, y ) );
            fEty.xy_coord( x, y ) = koef * ( fdjtdty - plasma_frequency_sq * Ety_prev_complex.xy_coord( x, y ) );
            fEtz.xy_coord( x, y ) = koef * ( fdjtdtz - plasma_frequency_sq * Etz_prev_complex.xy_coord( x, y ) );

            Etx_prev_complex.xy_coord( x, y ) = fEtx.xy_coord( x, y );
            Ety_prev_complex.xy_coord( x, y ) = fEty.xy_coord( x, y );
            Etz_prev_complex.xy_coord( x, y ) = fEtz.xy_coord( x, y );
        }
    }

    fill_zeroes( fEtx );
    fill_zeroes( fEty );
    fill_zeroes( fEtz );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::el_to_real( )
{
    ftElx.runBackward( );
    ftEly.runBackward( );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::b_to_real( )
{
    ftBx.runBackward( );
    ftBy.runBackward( );
    ftBz.runBackward( );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::et_to_real( )
{
    ftEtx.runBackward( );
    ftEty.runBackward( );
    ftEtz.runBackward( );
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::et_el_to_e( )
{
    std::fill( Ex.begin( ), Ex.end( ), 0 );
    std::fill( Ey.begin( ), Ey.end( ), 0 );

    for( size_t i = 0; i != Ex.size( ); ++i )
    {
        Ex[ i ] = Elx[ i ] + Etx[ i ];
    }
    for( size_t i = 0; i != Ey.size( ); ++i )
    {
        Ey[ i ] = Ely[ i ] + Ety[ i ];
    }
    for( size_t i = 0; i != Ez.size( ); ++i )
    {
        Ez[ i ] = Etz[ i ];
    }
}

template < typename MeshType, typename BoundaryConditions,
           typename ParticlesGridWeighting, typename ParticleMover,
           typename RealType >
void particles::FFTFieldSolver< 2, MeshType, BoundaryConditions, ParticlesGridWeighting,
                                ParticleMover, RealType >::b0_to_b( )
{
    for( size_t i = 0; i != Bx.size( ); ++i )
    {
        Bx[ i ] += B0.x( );
    }
    for( size_t i = 0; i != By.size( ); ++i )
    {
        By[ i ] += B0.y( );
    }
    for( size_t i = 0; i != Bz.size( ); ++i )
    {
        Bz[ i ] += B0.z( );
    }
}
