#include "FromParticlesToGrid.h"

#include "Parameters.h"

#include <algorithm>
#include <thread>

#if defined( _OPENMP )
#include <omp.h>
#endif

namespace particles
{

template < typename MeshType, typename RealType >
FromParticlesToGridFirstOrder< 1, MeshType, RealType >::FromParticlesToGridFirstOrder(
    MeshType & mesh,
    ParticleHolders< MeshType, RealType > & particles,
    RealType particles_per_cell )
    : m_mesh( mesh )
    , m_particles( particles )
    , inversed_ppc( 1. / particles_per_cell )
#if defined( _OPENMP )
    , threads_data( std::thread::hardware_concurrency( ), { mesh.m_numberOfCells } )
#endif
{
}

template < typename MeshType, typename RealType >
void FromParticlesToGridFirstOrder< 1, MeshType, RealType >::run( )
{
    for( auto & particles : m_particles )
    {
        runSpecies( particles );
    }
}

template < typename MeshType, typename RealType >
void FromParticlesToGridFirstOrder< 1, MeshType, RealType >::runSpecies(
    ParticleHolder< MeshType, RealType > & species )
{
    std::fill( species.m_rho.begin( ), species.m_rho.end( ), 0. );

    std::fill( species.m_jx.begin( ), species.m_jx.end( ), 0. );
    std::fill( species.m_jy.begin( ), species.m_jy.end( ), 0. );
    std::fill( species.m_jz.begin( ), species.m_jz.end( ), 0. );

#if defined( _OPENMP )
#pragma omp parallel
    {
        const auto thread_id = omp_get_thread_num( );

        threads_data[ thread_id ].reset_zero( );
        auto & rho = threads_data[ thread_id ].rho;
        auto & jx  = threads_data[ thread_id ].jx;
        auto & jy  = threads_data[ thread_id ].jy;
        auto & jz  = threads_data[ thread_id ].jz;

#pragma omp for
        for( auto i = 0u; i < species.m_particles.size( ); ++i )
        {
            const auto & p = species.m_particles[ i ];
            runOneParticle( p, species.m_qs_qe,
                            rho, jx, jy, jz );
        }
    }

    {
        auto & rho = species.m_rho;
        auto & jx  = species.m_jx;
        auto & jy  = species.m_jy;
        auto & jz  = species.m_jz;
        for( size_t t_id = 0; t_id < threads_data.size( ); ++t_id )
        {
            auto & t_rho = threads_data[ t_id ].rho;
            auto & t_jx  = threads_data[ t_id ].jx;
            auto & t_jy  = threads_data[ t_id ].jy;
            auto & t_jz  = threads_data[ t_id ].jz;

#pragma omp parallel for
            for( size_t i = 0; i < t_rho.size( ); ++i )
            {
                rho[ i ] += t_rho[ i ];
            }
#pragma omp parallel for
            for( size_t i = 0; i < t_jx.size( ); ++i )
            {
                jx[ i ] += t_jx[ i ];
            }
#pragma omp parallel for
            for( size_t i = 0; i < t_jy.size( ); ++i )
            {
                jy[ i ] += t_jy[ i ];
            }
#pragma omp parallel for
            for( size_t i = 0; i < t_jz.size( ); ++i )
            {
                jz[ i ] += t_jz[ i ];
            }
        }
    }

#else

    for( auto i = 0u; i < species.m_particles.size( ); ++i )
    {
        const auto & p = species.m_particles[ i ];
        runOneParticle( p, species.m_qs_qe,
                        species.m_rho, species.m_jx,
                        species.m_jy, species.m_jz );
    }

#endif

    mpi::all_reduce_sum( species.m_rho );

    mpi::all_reduce_sum( species.m_jx );
    mpi::all_reduce_sum( species.m_jy );
    mpi::all_reduce_sum( species.m_jz );

#pragma omp parallel
    {
#pragma omp for
        for( size_t i = 0; i < species.m_rho.size( ); ++i )
        {
            species.m_rho[ i ] *= inversed_ppc;
        }
#pragma omp for
        for( size_t i = 0; i < species.m_jx.size( ); ++i )
        {
            species.m_jx[ i ] *= inversed_ppc;
        }
#pragma omp for
        for( size_t i = 0; i < species.m_jy.size( ); ++i )
        {
            species.m_jy[ i ] *= inversed_ppc;
        }
#pragma omp for
        for( size_t i = 0; i < species.m_jz.size( ); ++i )
        {
            species.m_jz[ i ] *= inversed_ppc;
        }
    }
}

template < typename MeshType, typename RealType >
void FromParticlesToGridFirstOrder< 1, MeshType, RealType >::runOneParticle(
    const Particle< RealType > & p, RealType qParticle,
    real_arr_t & rho, real_arr_t & jx, real_arr_t & jy, real_arr_t & jz )
{
    const indexPair_t idxs = m_mesh.computeXIndicesFromPosition( p.x( ) );

    const auto weights = compute_weights_particle( p.x( ), idxs );

    map_to_grid( p, weights, idxs, rho, qParticle );

    map_to_grid( p, weights, idxs, jx, qParticle * p.vx( ) );
    map_to_grid( p, weights, idxs, jy, qParticle * p.vy( ) );
    map_to_grid( p, weights, idxs, jz, qParticle * p.vz( ) );
}

template < typename MeshType, typename RealType >
std::pair< RealType, RealType >
FromParticlesToGridFirstOrder< 1, MeshType, RealType >::
    compute_weights_particle( const Particle< RealType > & p, const indexPair_t & idxs )
{
    const auto dx_inv      = m_mesh.m_dx_inv;
    const auto weightRight = ( p.x( ) - m_mesh.x( idxs.first ) ) * dx_inv;
    const auto weightLeft  = 1. - weightRight;
    return { weightLeft, weightRight };
}

template < typename MeshType, typename RealType >
std::pair< RealType, RealType >
FromParticlesToGridFirstOrder< 1, MeshType, RealType >::
    compute_weights_particle( const Particle< RealType > & p )
{
    const auto idxs = m_mesh.computeXIndicesFromPosition( p.x( ) );
    return compute_weights_particle( p, idxs );
}

template < typename MeshType, typename RealType >
void FromParticlesToGridFirstOrder< 1, MeshType, RealType >::map_to_grid(
    const Particle< RealType > & p, const weights_t & weights, const indexPair_t & indices,
    real_arr_t & grid_quantity, const RealType quantity )
{
    grid_quantity[ indices.first ] += weights.first * quantity;
    grid_quantity[ indices.second ] += weights.second * quantity;
}

template < typename MeshType, typename RealType >
RealType FromParticlesToGridFirstOrder< 1, MeshType, RealType >::weightQuantityOnParticle(
    Particle< RealType > & particle, Array< 1, RealType > & quantityOnGrid )
{
    const auto x_idxs = m_mesh.computeXIndicesFromPosition( particle.x( ) );

    const auto weights = compute_weights_particle( particle.x( ) );

    return quantityOnGrid[ x_idxs.first ] * weights.first + quantityOnGrid[ x_idxs.second ] * weights.second;
}

template < typename MeshType, typename RealType >
void FromParticlesToGridFirstOrder< 1, MeshType, RealType >::map_to_grid(
    const Particle< RealType > & p, const weights_t & weights,
    real_arr_t & grid_quantity, const RealType quantity )
{
    const auto indices = m_mesh.computeXIndicesFromPosition( p.x( ) );
    map_to_grid( p.x( ), weights, indices, grid_quantity, quantity );
}

template < typename MeshType, typename RealType >
void FromParticlesToGridFirstOrder< 1, MeshType, RealType >::computeDensity(
    std::vector< std::vector< RealType > > & n_s )
{
    for( auto holderIdx = 0u; holderIdx < m_particles.size( ); ++holderIdx )
    {
        auto & holder = m_particles[ holderIdx ];
        auto & n      = n_s[ holderIdx ];
        std::fill( n.begin( ), n.end( ), 0. );
        auto ms_inv = 1. / holder.m_ms_me_nonsuper;
        for( auto & p : holder.m_particles )
        {
            auto idxs        = m_mesh.computeXIndicesFromPosition( p.x( ) );
            auto dx          = m_mesh.m_dx;
            auto weightLeft  = ( m_mesh.x( idxs.second ) - p.x( ) ) / dx;
            auto weightRight = ( p.x( ) - m_mesh.x( idxs.first ) ) / dx;
            n[ idxs.first ] += ms_inv * holder.m_ms_me * weightLeft;
            n[ idxs.second ] += ms_inv * holder.m_ms_me * weightRight;
        }
        auto cellLength = ( m_mesh.m_xMax - m_mesh.m_xMin ) / m_mesh.m_numberOfCells;
        for( auto & val : n )
        {
            val /= ( cellLength );
        }
        auto mean = ( n[ 0 ] + n[ n.size( ) - 1 ] );
        n[ 0 ] = n[ n.size( ) - 1 ] = mean;
    }
}

}// namespace particles
