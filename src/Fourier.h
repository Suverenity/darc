//
// Created by suverenity on 3/1/18.
//

#ifndef PROJECT_FOURIER_H
#define PROJECT_FOURIER_H

#include <algorithm>
#include <memory>
#include <vector>

#include "Types.h"

extern "C"
{
#include <fftw3.h>
}

namespace particles
{

template < typename RealType, typename = void >
struct FFT_plan;

template < typename PreciseCompile >
struct FFT_plan< double, PreciseCompile >
{
    fftw_plan plan;

    ~FFT_plan( )
    {
        fftw_destroy_plan( plan );
    }
};

template < typename PreciseCompile >
struct FFT_plan< float, PreciseCompile >
{
    fftwf_plan plan;

    ~FFT_plan( )
    {
        fftwf_destroy_plan( plan );
    }
};

template < int Dim, typename RealType, typename = void >
class FourierPlanForward;

template < typename PreciseCompile >
class FourierPlanForward< 1, float, PreciseCompile >
{
    using real_t = float;

public:
    FourierPlanForward( Array< 1, real_t > & in, ArrayComplex< 1, real_t > & out )
        : plan{ fftwf_plan_dft_r2c_1d(
              in.size( ), in.data( ), reinterpret_cast< fftwf_complex * >( out.data( ) ),
              FFTW_MEASURE ) }
    {
    }

    FourierPlanForward( FourierPlanForward & ) = delete;

    FFT_plan< real_t > plan;
};

template < typename PreciseCompile >
class FourierPlanForward< 1, double, PreciseCompile >
{
    using real_t = double;

public:
    FourierPlanForward( Array< 1, real_t > & in, ArrayComplex< 1, real_t > & out )
        : plan{ fftw_plan_dft_r2c_1d( in.size( ), in.data( ),
                                      reinterpret_cast< fftw_complex * >( out.data( ) ),
                                      FFTW_MEASURE ) }
    {
    }

    FourierPlanForward( FourierPlanForward & ) = delete;

    FFT_plan< real_t > plan;
};

template < typename PreciseCompile >
class FourierPlanForward< 2, float, PreciseCompile >
{
    using real_t = float;

public:
    FourierPlanForward( Array< 2, real_t > & in, ArrayComplex< 2, real_t > & out )
        : plan{ fftwf_plan_dft_r2c_2d(
              in.m_n0, in.m_n1,
              in.data( ), reinterpret_cast< fftwf_complex * >( out.data( ) ),
              FFTW_MEASURE ) }
    {
    }

    FourierPlanForward( FourierPlanForward & ) = delete;

    FFT_plan< real_t > plan;
};

template < typename PreciseCompile >
class FourierPlanForward< 2, double, PreciseCompile >
{
    using real_t = double;

public:
    FourierPlanForward( Array< 2, real_t > & in, ArrayComplex< 2, real_t > & out )
        : plan{ fftw_plan_dft_r2c_2d(
              in.m_n0, in.m_n1,
              in.data( ), reinterpret_cast< fftw_complex * >( out.data( ) ),
              FFTW_MEASURE ) }
    {
    }

    FourierPlanForward( FourierPlanForward & ) = delete;

    FFT_plan< real_t > plan;
};

template < int Dim, typename RealType, typename = void >
class FourierPlanBackward;

template < typename PreciseCompile >
class FourierPlanBackward< 1, float, PreciseCompile >
{
    using real_t = float;

public:
    FourierPlanBackward( ArrayComplex< 1, real_t > & in, Array< 1, real_t > & out )
        : plan{ fftwf_plan_dft_c2r_1d(
              in.size( ), reinterpret_cast< fftwf_complex * >( in.data( ) ), out.data( ),
              FFTW_MEASURE ) }
    {
    }

    FourierPlanBackward( FourierPlanBackward & ) = delete;

    FFT_plan< real_t > plan;
};

template < typename PreciseCompile >
class FourierPlanBackward< 1, double, PreciseCompile >
{
    using real_t = double;

public:
    FourierPlanBackward( ArrayComplex< 1, real_t > & in, Array< 1, real_t > & out )
        : plan{ fftw_plan_dft_c2r_1d( in.size( ),
                                      reinterpret_cast< fftw_complex * >( in.data( ) ),
                                      out.data( ), FFTW_MEASURE ) }
    {
    }

    FourierPlanBackward( FourierPlanBackward & ) = delete;

    FFT_plan< real_t > plan;
};

template < typename PreciseCompile >
class FourierPlanBackward< 2, float, PreciseCompile >
{
    using real_t = float;

public:
    FourierPlanBackward( ArrayComplex< 2, real_t > & in, Array< 2, real_t > & out )
        : plan{ plan.plan = fftwf_plan_dft_c2r_2d(
                    out.m_n0, out.m_n1,
                    reinterpret_cast< fftwf_complex * >( in.data( ) ), out.data( ),
                    FFTW_MEASURE ) }
    {
    }

    FourierPlanBackward( FourierPlanBackward & ) = delete;

    FFT_plan< real_t > plan;
};

template < typename PreciseCompile >
class FourierPlanBackward< 2, double, PreciseCompile >
{
    using real_t = double;

public:
    FourierPlanBackward( ArrayComplex< 2, real_t > & in, Array< 2, real_t > & out )
        : plan{ plan.plan = fftw_plan_dft_c2r_2d(
                    out.m_n0, out.m_n1,
                    reinterpret_cast< fftw_complex * >( in.data( ) ), out.data( ),
                    FFTW_MEASURE ) }
    {
    }

    FourierPlanBackward( FourierPlanBackward & ) = delete;

    FFT_plan< real_t > plan;
};

/**
 * @brief Fourier is an object used to convert array to Fourier space and back. Fill passed arrays AFTER you assigned them to Fourier obj.
 */

template < int Dim, typename RealType, typename = void >
class Fourier;

template < typename PreciseCompile >
class Fourier< 1, float, PreciseCompile >
{
    using real_t = float;

public:
    using real_arr_t    = Array< 1, real_t >;
    using complex_arr_t = ArrayComplex< 1, real_t >;

    Fourier( real_arr_t & arrayToConvert )
        : m_realArr{ arrayToConvert }
        , m_fourierArr( arrayToConvert.size( ) )
        , m_forwardPlan{ m_realArr, m_fourierArr }
        , m_backwardPlan{ m_fourierArr, m_realArr }
        , m_size_inv{ 1.0f / m_realArr.size( ) }
    {
    }

    void runForward( )
    {
        fftwf_execute( m_forwardPlan.plan.plan );
    }
    void runBackward( )
    {
        fftwf_execute( m_backwardPlan.plan.plan );
        //TODO parallel?
        std::for_each( m_realArr.begin( ), m_realArr.end( ),
                       [ this ]( auto & val ) { val *= m_size_inv; } );
    }

    real_arr_t & m_realArr;
    complex_arr_t m_fourierArr;

private:
    FourierPlanForward< 1, real_t > m_forwardPlan;
    FourierPlanBackward< 1, real_t > m_backwardPlan;
    const real_t m_size_inv;
};

template < typename PreciseCompile >
class Fourier< 1, double, PreciseCompile >
{
    using real_t = double;

public:
    using real_arr_t    = Array< 1, real_t >;
    using complex_arr_t = ArrayComplex< 1, real_t >;

    Fourier( real_arr_t & arrayToConvert )
        : m_realArr{ arrayToConvert }
        , m_fourierArr( arrayToConvert.size( ) )
        , m_forwardPlan{ m_realArr, m_fourierArr }
        , m_backwardPlan{ m_fourierArr, m_realArr }
        , m_size_inv{ 1.0 / m_realArr.size( ) }
    {
    }

    void runForward( )
    {
        fftw_execute( m_forwardPlan.plan.plan );
    }
    void runBackward( )
    {
        fftw_execute( m_backwardPlan.plan.plan );
        //TODO parallel?
        std::for_each( m_realArr.begin( ), m_realArr.end( ),
                       [ this ]( auto & val ) { val *= m_size_inv; } );
    }

    real_arr_t & m_realArr;
    complex_arr_t m_fourierArr;

private:
    FourierPlanForward< 1, real_t > m_forwardPlan;
    FourierPlanBackward< 1, real_t > m_backwardPlan;
    const real_t m_size_inv;
};

template < typename PreciseCompile >
class Fourier< 2, float, PreciseCompile >
{
    using real_t = float;

public:
    using real_arr_t    = Array< 2, real_t >;
    using complex_arr_t = ArrayComplex< 2, real_t >;

    Fourier( real_arr_t & arrayToConvert )
        : m_realArr( arrayToConvert )
        , m_fourierArr( arrayToConvert.m_n0, arrayToConvert.m_n1 / 2 + 1 )
        , m_forwardPlan( m_realArr, m_fourierArr )
        , m_backwardPlan( m_fourierArr, m_realArr )
        , m_size_inv{ 1.0f / ( m_realArr.m_n0 * m_realArr.m_n1 ) }
    {
    }

    void runForward( )
    {
        fftwf_execute( m_forwardPlan.plan.plan );
    }

    void runBackward( )
    {
        fftwf_execute( m_backwardPlan.plan.plan );
        std::for_each(
            m_realArr.begin( ),
            m_realArr.end( ),
            [ this ]( float & val ) { val *= m_size_inv; } );
    }

    real_arr_t & m_realArr;
    complex_arr_t m_fourierArr;

private:
    FourierPlanForward< 2, real_t > m_forwardPlan;
    FourierPlanBackward< 2, real_t > m_backwardPlan;
    const real_t m_size_inv;
};

template < typename PreciseCompile >
class Fourier< 2, double, PreciseCompile >
{
    using real_t = double;

public:
    using real_arr_t    = Array< 2, real_t >;
    using complex_arr_t = ArrayComplex< 2, real_t >;

    Fourier( real_arr_t & arrayToConvert )
        : m_realArr( arrayToConvert )
        , m_fourierArr( arrayToConvert.m_n0, arrayToConvert.m_n1 / 2 + 1 )
        , m_forwardPlan( m_realArr, m_fourierArr )
        , m_backwardPlan( m_fourierArr, m_realArr )
        , m_size_inv{ 1.0f / ( m_realArr.m_n0 * m_realArr.m_n1 ) }
    {
    }

    void runForward( )
    {
        fftw_execute( m_forwardPlan.plan.plan );
    }

    void runBackward( )
    {
        fftw_execute( m_backwardPlan.plan.plan );
        std::for_each(
            m_realArr.begin( ),
            m_realArr.end( ),
            [ this ]( float & val ) { val *= m_size_inv; } );
    }

    real_arr_t & m_realArr;
    complex_arr_t m_fourierArr;

private:
    FourierPlanForward< 2, real_t > m_forwardPlan;
    FourierPlanBackward< 2, real_t > m_backwardPlan;
    const real_t m_size_inv;
};
}// namespace particles

#endif//PROJECT_FOURIER_H
