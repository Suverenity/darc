#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <math.h>

#include "Types.h"
#include "Vector.h"

namespace particles
{

//Debye length, thermal electron speed,
template < class RT >
class Parameters
{
public:
    typedef RT RealType;

    //Parameters( )
    //{
    //    recompute_parameters( );
    //}

    // numerical parameters
    RealType xmin               = -512;
    RealType xmax               = 512;
    RealType ymin               = -512;
    RealType ymax               = 512;
    RealType numberOfXCells     = 512;
    RealType numberOfYCells     = 512;
    RealType particlesPerCell   = 100;
    RealType dt                 = 0.01;
    RealType simulationTime     = 100;
    RealType iterations         = 4;
    RealType electromagneticEps = 1e-5;
    bool electrostatic          = true;
    bool hellinger_init         = true;
    bool save_particles         = true;

    // physical parameters
    RealType beta_e_parallel   = { 1 };
    RealType beta_p_parallel   = { 1 };
    RealType omega_pe_omega_ce = { 100 };//plasma frequency/electron gyrofrequency
    RealType T_p_per_T_p_par   = { 1 };  // T_p_perpendicular/T_p_parallel
    RealType T_e_per_T_e_par   = { 1 };  // T_e_perpendicular/T_e_parallel
    RealType mp_me             = { 500 };// proton mass compared to electron
    RealType polar_angle       = { 0 };  // in degrees
    RealType azimuthal_angle   = { 90 }; // in degress

    // physical parameters computed from existing
    RealType n0{ };
    RealType omega_ce_omega_pe{ };
    RealType B0{ };
    RealType alpha{ };// thermal speed / speed of light, in normalization:
    VectorClass< RealType > unit_v{ };

    RealType cos_polar{ };
    RealType sin_polar{ };
    RealType cos_azimuth{ };
    RealType sin_azimuth{ };

    // other settings
    size_t saveEachShot          = 5;
    size_t saveParticlesEachShot = 5;

    bool restart                = false;
    size_t iteration_to_restart = 0;

    void recompute_parameters( )
    {
        omega_ce_omega_pe = RealType{ 1 } / omega_pe_omega_ce;//plasma frequency/electron gyrofrequency

        constexpr RealType degree_to_rads = RealType{ M_PI } / RealType{ 180 };

        cos_polar   = std::cos( degree_to_rads * polar_angle );
        sin_polar   = std::sin( degree_to_rads * polar_angle );
        cos_azimuth = std::cos( degree_to_rads * azimuthal_angle );
        sin_azimuth = std::sin( degree_to_rads * azimuthal_angle );

        n0 = { 1 };
        if( electrostatic )
        {
            B0 = 0.;
        }
        else
        {
            B0 = omega_ce_omega_pe;
        }
        alpha  = std::sqrt( beta_e_parallel / 2. ) * omega_ce_omega_pe;
        unit_v = { cos_polar * sin_azimuth,
                   sin_polar * sin_azimuth,
                   cos_azimuth };

        std::cout << "n0: " << n0 << std::endl;
        std::cout << "B0: " << B0 << std::endl;
        std::cout << "alpha: " << alpha << std::endl;
        std::cout << "polar: " << polar_angle << ", azimuthal_angle: "
                  << azimuthal_angle << ", direction of B: " << unit_v << std::endl;
    }

    VectorClass< RealType > rotate( const VectorClass< RealType > & point )
    {
        return {
            point.x( ) * cos_azimuth * cos_polar +
                point.z( ) * sin_azimuth * cos_polar -
                point.y( ) * sin_polar,
            point.x( ) * cos_azimuth * sin_polar +
                point.z( ) * sin_azimuth * sin_polar +
                point.y( ) * cos_polar,
            -point.x( ) * sin_azimuth +
                point.z( ) * cos_azimuth
        };
    }
};

}// namespace particles

#endif
