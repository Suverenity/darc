#ifndef PROJECT_MESH1D_HPP
#define PROJECT_MESH1D_HPP

#include <assert.h>
#include <cmath>
#include <thread>

#include "Mesh.h"

namespace particles
{

template < class RealType >
Mesh< 1, RealType >::Mesh( RealType xMin, RealType xMax, unsigned long numberOfCells )
    : m_xMin( xMin )
    , m_xMax( xMax )
    , m_xSize( xMax - xMin )
    , m_numberOfCells( numberOfCells )
#if defined( _OPENMP )
    , m_buffers( init_buffers< RealType >( m_numberOfCells, std::thread::hardware_concurrency( ) ) )
#endif
{
    m_dx     = ( m_xMax - m_xMin ) / ( m_numberOfCells );
    m_dx_inv = 1. / m_dx;
    std::cout << "creating grid: " << m_xMin << ", " << m_xMax << ", dx: " << m_dx << std::endl;
}

template < class RealType >
Mesh< 1, RealType >::Mesh( Parameters< RealType > & pars )
    : Mesh( pars.xmin, pars.xmax, pars.numberOfXCells )
{
}

template < class RealType >
indexPair_t Mesh< 1, RealType >::computeXIndicesFromPosition( RealType position )
{
    const auto i = size_t( ( position - m_xMin ) * m_dx_inv );
    if( i >= m_numberOfCells )
    {
        return { 0, 0 };
    }
    assert( i >= 0 && i < m_numberOfCells );
    return { i, ( i + 1 ) >= m_numberOfCells ? 0 : i + 1 };
}

template < class RealType >
RealType Mesh< 1, RealType >::x( const size_t index ) const
{
    const RealType res = m_xMin + m_dx * index;
    return res;
}

template < class RealType >
interval_t< RealType > Mesh< 1, RealType >::computeXCellIntervalFromIndex( const size_t index )
{
    assert( index < m_numberOfCells );

    return { x( index ), x( index + 1 ) };
}

template < class RealType >
typename Mesh< 1, RealType >::real_arr_t Mesh< 1, RealType >::create_arr( RealType init_value )
{
    return real_arr_t( m_numberOfCells, init_value );
}

}// namespace particles

#endif//PROJECT_MESH1D_HPP
