#ifndef PROJECT_HDFSAVER_H
#define PROJECT_HDFSAVER_H

#include "Mpi.h"
#include "ParticleHolder.h"
#include "Types.h"

#include <H5Cpp.h>

#include <iomanip>
#include <sstream>

namespace particles
{
using namespace H5;

//http://www.drdobbs.com/genericprogramming-mappings-between-type/184403750
template < typename RealType >
struct RealTypeToHDFType
{
    typedef RealType OriginalType;
};

template < int Dim, typename MeshType, typename ArrType >
class HDFSaver
{
public:
    HDFSaver( ) = default;

    template < int D, typename T >
    struct DSpace_creator
    {
    };

    template < typename T >
    struct DSpace_creator< 1, T >
    {
        DataSpace operator( )( Array< 1, T > & arr )
        {
            hsize_t dims[ 1 ] = { arr.size( ) };
            return DataSpace( 1, dims );
        }
    };

    template < typename T >
    struct DSpace_creator< 2, T >
    {
        DataSpace operator( )( Array< 2, T > & arr )
        {
            hsize_t dims[ 2 ] = { arr.m_n0, arr.m_n1 };
            return DataSpace( 2, dims );
        }
    };

    void saveGridQuantity( Array< Dim, ArrType > & gridQuantity, std::string quantityName, uint64_t index )
    {
        std::stringstream ss;
        ss << std::setw( 8 ) << std::setfill( '0' ) << index;
        const auto timestamp = ss.str( );
        H5File file( ( quantityName + timestamp + ".h5" ).c_str( ), H5F_ACC_TRUNC );

        auto dspace_creat = DSpace_creator< Dim, ArrType >( );
        auto dspace       = dspace_creat( gridQuantity );
        DataSet dset      = file.createDataSet( datasetName.c_str( ), convertRealType( RealTypeToHDFType< ArrType >( ) ),
                                           dspace );
        dset.write( gridQuantity.data( ), convertRealType( RealTypeToHDFType< ArrType >( ) ) );
        file.close( );
    }

    template < typename RealType >
    void save( ParticleHolder< MeshType, RealType > & holder, std::string quantity_name, std::string timestamp, Array< 1, RealType > positions )
    {
        H5File file( ( holder.pname + "_" + quantity_name + "_" +
                       std::to_string( mpi::current_process_id< typename MeshType::real_arr_t >( ) ) +
                       "_" + timestamp + ".h5" )
                         .c_str( ),
                     H5F_ACC_TRUNC );
        hsize_t dims[ 1 ];
        dims[ 0 ] = positions.size( );
        DataSpace dspace( 1, dims );
        DataSet dset = file.createDataSet( datasetName.c_str( ), convertRealType( RealTypeToHDFType< RealType >( ) ),
                                           dspace );
        dset.write( positions.data( ), convertRealType( RealTypeToHDFType< RealType >( ) ) );
        file.close( );
    }

    void saveParticlesPositions( ParticleHolders< MeshType, ArrType > & holders, uint64_t index )
    {
        using real_arr_t = Array< 1, ArrType >;

        for( auto & holder : holders )
        {
            std::stringstream ss;
            ss << std::setw( 8 ) << std::setfill( '0' ) << index;
            const auto timestamp = ss.str( );

            auto & particles = holder.m_particles;

            if( Dim == 1 )
            {
                real_arr_t pposx( particles.size( ) );
#pragma omp parallel for
                for( auto i = 0u; i < pposx.size( ); ++i )
                {
                    pposx[ i ] = particles[ i ].m_x[ 0 ];
                }
                save( holder, "ppos_x", timestamp, pposx );
            }
            else if( Dim == 2 )
            {
                real_arr_t pposx( particles.size( ) );
                real_arr_t pposy( particles.size( ) );
#pragma omp parallel for
                for( auto i = 0u; i < particles.size( ); ++i )
                {
                    pposx[ i ] = particles[ i ].m_x[ 0 ];
                    pposy[ i ] = particles[ i ].m_x[ 1 ];
                }
                save( holder, "ppos_x", timestamp, pposx );
                save( holder, "ppos_y", timestamp, pposy );
            }
            else if( Dim == 3 )
            {
                real_arr_t pposx( particles.size( ) );
                real_arr_t pposy( particles.size( ) );
                real_arr_t pposz( particles.size( ) );
#pragma omp parallel for
                for( auto i = 0u; i < particles.size( ); ++i )
                {
                    pposx[ i ] = particles[ i ].m_x[ 0 ];
                    pposy[ i ] = particles[ i ].m_x[ 1 ];
                    pposz[ i ] = particles[ i ].m_x[ 2 ];
                }
                save( holder, "ppos_x", timestamp, pposx );
                save( holder, "ppos_y", timestamp, pposy );
                save( holder, "ppos_z", timestamp, pposz );
            }
        }
    }

    void saveParticlesVelocityX( ParticleHolders< MeshType, ArrType > & holders, uint64_t index )
    {
        using real_arr_t = Array< 1, ArrType >;

        for( auto & holder : holders )
        {
            std::stringstream ss;
            ss << std::setw( 8 ) << std::setfill( '0' ) << index;
            const auto timestamp = ss.str( );
            auto & particles     = holder.m_particles;
            real_arr_t vx( particles.size( ) );
            real_arr_t vy( particles.size( ) );
            real_arr_t vz( particles.size( ) );
#pragma omp parallel for
            for( auto i = 0u; i < particles.size( ); ++i )
            {
                vx[ i ] = particles[ i ].m_v[ 0 ];
                vy[ i ] = particles[ i ].m_v[ 1 ];
                vz[ i ] = particles[ i ].m_v[ 2 ];
            }
            save( holder, "v_x", timestamp, vx );
            save( holder, "v_y", timestamp, vy );
            save( holder, "v_z", timestamp, vz );
        }
    }

    std::vector< ArrType > load( std::string species_name, std::string quantity_name, size_t iteration )
    {
        std::stringstream ss;
        ss << std::setw( 8 ) << std::setfill( '0' ) << iteration;
        const auto timestamp = ss.str( );
        H5File file( ( species_name + "_" + quantity_name + "_" +
                       std::to_string( mpi::current_process_id< typename MeshType::real_arr_t >( ) ) +
                       "_" + timestamp + ".h5" )
                         .c_str( ),
                     H5F_ACC_RDONLY );
        DataSet dset    = file.openDataSet( datasetName.c_str( ) );
        DataSpace space = dset.getSpace( );

        hsize_t dims_out[ 1 ];
        space.getSimpleExtentDims( dims_out, NULL );

        std::vector< ArrType > res( dims_out[ 0 ] );

        dset.read( res.data( ), convertRealType( RealTypeToHDFType< ArrType >( ) ) );

        file.close( );

        return res;
    }

private:
    const PredType & convertRealType( RealTypeToHDFType< float > )
    {
        return PredType::NATIVE_FLOAT;
    }

    const PredType & convertRealType( RealTypeToHDFType< double > )
    {
        return PredType::NATIVE_DOUBLE;
    }

    const std::string datasetName = "dset";
};

}// namespace particles

#endif
