#pragma once

#include "Types.h"

namespace particles
{
template < class RealType >
struct thread_arrays
{
    thread_arrays( size_t size )
        : rho( size )
        , jx( size )
        , jy( size )
        , jz( size )
    {
    }
    using real_arr_t = Array< 1, RealType >;
    real_arr_t rho;

    real_arr_t jx;
    real_arr_t jy;
    real_arr_t jz;
};

template < class RealType >
using thread_arrays_t = std::vector< thread_arrays< RealType > >;

template < class RealType >
static thread_arrays_t< RealType > & init_buffers( size_t length, size_t thread_number )
{
    static thread_arrays_t< RealType > arrs( thread_number, { length } );
    return arrs;
}

}// namespace particles
