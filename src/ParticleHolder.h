#ifndef PARTICLEHOLDER_H
#define PARTICLEHOLDER_H

#include "Particle.h"

#include "Types.h"

#include <string>
#include <vector>

namespace particles
{
template < class MeshType, class RealType >
class ParticleHolder
{
public:
    using real_arr_t = typename MeshType::real_arr_t;

    ParticleHolder( std::string name, size_t numberOfParticles,
                    RealType ms_me, RealType qs_qe,
                    MeshType & mesh )
        : m_particles( numberOfParticles )
        , m_rho( mesh.create_arr( ) )
        , m_jx( mesh.create_arr( ) )
        , m_jy( mesh.create_arr( ) )
        , m_jz( mesh.create_arr( ) )
        , m_ppar( mesh.create_arr( ) )
        , m_pper( mesh.create_arr( ) )
        , pname( name )
        , m_ms_me( ms_me )
        , m_qs_qe( qs_qe )
    {
    }

    ParticleHolder( const ParticleHolder< MeshType, RealType > & holder ) = default;
    ParticleHolder( ParticleHolder< MeshType, RealType > && holder )      = default;

    template < class ParticlesToMesh >
    void compute_pressure( ParticlesToMesh & particles_to_mesh );

    std::vector< Particle< RealType > > m_particles;

    real_arr_t m_rho;

    real_arr_t m_jx;
    real_arr_t m_jy;
    real_arr_t m_jz;

    real_arr_t m_ppar;
    real_arr_t m_pper;

    std::string pname;

    RealType m_ms_me = 1;
    RealType m_qs_qe = -1;

    VectorClass< RealType > b0;
};

template < class MeshType, class RealType >
template < class ParticlesToMesh >
void ParticleHolder< MeshType, RealType >::compute_pressure( ParticlesToMesh & particles_to_mesh )
{
    std::fill( m_ppar.begin( ), m_ppar.end( ), 0 );
    std::fill( m_pper.begin( ), m_pper.end( ), 0 );

    for( size_t i = 0; i != m_particles.size( ); ++i )
    {
        Particle< RealType > & particle = m_particles[ i ];

        RealType v_par = particle.m_v.dotProduct( b0 );
        RealType v_per = ( particle.m_v - v_par * b0 ).L2Squared( );
    }
}

template < class MeshType, class RealType >
using ParticleHolders = typename std::vector< ParticleHolder< MeshType, RealType > >;

}// namespace particles

#endif
