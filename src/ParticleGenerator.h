//
// Created by suverenity on 3/29/18.
//

#ifndef PROJECT_PARTICLEGENERATOR_H
#define PROJECT_PARTICLEGENERATOR_H

#include <cstdlib>
#include <random>
#include <vector>

#include "HDFSaver.h"
#include "Mesh.h"
#include "Mpi.h"
#include "Parameters.h"
#include "ParticleHolder.h"
#include "Types.h"

namespace particles
{

template < int Dim, class MeshType, class RealType >
class ParticleGenerator
{
public:
    ParticleGenerator( MeshType & mesh, Parameters< RealType > & pars );

    ParticleHolders< MeshType, RealType > generate( );
    ParticleHolders< MeshType, RealType > load( );

    template < int Dim_, typename Mesh_t, typename RT >
    struct position_generator;

    template < typename Mesh_t, typename RT >
    struct position_generator< 1, Mesh_t, RT >
    {
        void operator( )( Mesh_t & mesh, ParticleHolder< MeshType, RealType > & species,
                          size_t ppc, std::default_random_engine & generator,
                          std::normal_distribution< RealType > & perpendicular_velocity_distr,
                          std::normal_distribution< RealType > & parallel_velocity_distr,
                          Parameters< RT > & pars )
        {
            for( size_t x = 0; x != mesh.m_numberOfCells; ++x )
            {
                auto interval = mesh.computeXCellIntervalFromIndex( x );
                std::uniform_real_distribution< RT > xcell_distr( interval.min, interval.max );
                for( size_t c = 0; c != ppc; ++c )
                {
                    auto & particle = species.m_particles[ x * ppc + c ];
                    particle.x( )   = xcell_distr( generator );

                    particle.vx( ) = perpendicular_velocity_distr( generator );
                    particle.vy( ) = perpendicular_velocity_distr( generator );
                    particle.vz( ) = parallel_velocity_distr( generator );
                    particle.m_v   = pars.rotate( particle.m_v );
                }
            }
        }
    };

    template < typename Mesh_t, typename RT >
    struct position_generator< 2, Mesh_t, RT >
    {
        void operator( )( Mesh_t & mesh, ParticleHolder< MeshType, RealType > & species,
                          size_t ppc, std::default_random_engine & generator,
                          std::normal_distribution< RealType > & perpendicular_velocity_distr,
                          std::normal_distribution< RealType > & parallel_velocity_distr,
                          Parameters< RT > & pars )
        {
            for( size_t y = 0; y != mesh.m_numberOfYCells; ++y )
            {
                auto yinterval = mesh.computeYCellIntervalFromIndex( y );
                std::uniform_real_distribution< RT > ycell_distr( yinterval.min, yinterval.max );
                for( size_t x = 0; x != mesh.m_numberOfXCells; ++x )
                {
                    auto xinterval = mesh.computeXCellIntervalFromIndex( x );
                    std::uniform_real_distribution< RT > xcell_distr( xinterval.min, xinterval.max );
                    for( size_t c = 0; c != ppc; ++c )
                    {
                        auto & particle = species.m_particles[ y * mesh.m_numberOfXCells * ppc + x * ppc + c ];
                        particle.x( )   = xcell_distr( generator );
                        particle.y( )   = ycell_distr( generator );

                        particle.vx( ) = perpendicular_velocity_distr( generator );
                        particle.vy( ) = perpendicular_velocity_distr( generator );
                        particle.vz( ) = parallel_velocity_distr( generator );
                        particle.m_v   = pars.rotate( particle.m_v );
                        //                        std::cout << "x: " << particle.x( ) << ", y:" << particle.y( ) << ", vx: " << particle.vx( ) << ", vz: " << particle.vz( ) << std::endl;
                    }
                }
            }
        }
    };

private:
    MeshType & m_mesh;
    Parameters< RealType > & m_pars;
    std::default_random_engine m_generator;
    HDFSaver< Dim, MeshType, RealType > saver;
};

template < int Dim, class MeshType, class RealType >
ParticleGenerator< Dim, MeshType, RealType >::ParticleGenerator( MeshType & mesh, Parameters< RealType > & pars )
    : m_mesh( mesh )
    , m_pars( pars )
    , m_generator( std::random_device{ }( ) )
{
}

template < int Dim, class MeshType, class RealType >
ParticleHolders< MeshType, RealType > ParticleGenerator< Dim, MeshType, RealType >::load( )
{
    auto loaded_data = saver.load( "el", "v_x", m_pars.iteration_to_restart );
    ParticleHolder< MeshType, RealType > electronHolder( "el", loaded_data.size( ), 1, -1, m_mesh );
    ParticleHolder< MeshType, RealType > protonHolder( "pt", loaded_data.size( ), m_pars.mp_me, 1, m_mesh );
    for( size_t i = 0; i != loaded_data.size( ); ++i )
    {
        electronHolder.m_particles[ i ].vx( ) = loaded_data[ i ];
    }

    loaded_data = saver.load( "el", "v_y", m_pars.iteration_to_restart );
    for( size_t i = 0; i != loaded_data.size( ); ++i )
    {
        electronHolder.m_particles[ i ].vy( ) = loaded_data[ i ];
    }

    loaded_data = saver.load( "el", "v_z", m_pars.iteration_to_restart );
    for( size_t i = 0; i != loaded_data.size( ); ++i )
    {
        electronHolder.m_particles[ i ].vz( ) = loaded_data[ i ];
    }

    loaded_data = saver.load( "pt", "v_x", m_pars.iteration_to_restart );
    for( size_t i = 0; i != loaded_data.size( ); ++i )
    {
        protonHolder.m_particles[ i ].vx( ) = loaded_data[ i ];
    }

    loaded_data = saver.load( "pt", "v_y", m_pars.iteration_to_restart );
    for( size_t i = 0; i != loaded_data.size( ); ++i )
    {
        protonHolder.m_particles[ i ].vy( ) = loaded_data[ i ];
    }

    loaded_data = saver.load( "pt", "v_z", m_pars.iteration_to_restart );
    for( size_t i = 0; i != loaded_data.size( ); ++i )
    {
        protonHolder.m_particles[ i ].vz( ) = loaded_data[ i ];
    }

    if constexpr( Dim == 1 )
    {
        loaded_data = saver.load( "el", "ppos_x", m_pars.iteration_to_restart );
        for( size_t i = 0; i != loaded_data.size( ); ++i )
        {
            electronHolder.m_particles[ i ].x( ) = loaded_data[ i ];
        }
        loaded_data = saver.load( "pt", "ppos_x", m_pars.iteration_to_restart );
        for( size_t i = 0; i != loaded_data.size( ); ++i )
        {
            protonHolder.m_particles[ i ].x( ) = loaded_data[ i ];
        }
    }
    else if constexpr( Dim == 2 )
    {
        loaded_data = saver.load( "el", "ppos_x", m_pars.iteration_to_restart );
        for( size_t i = 0; i != loaded_data.size( ); ++i )
        {
            electronHolder.m_particles[ i ].x( ) = loaded_data[ i ];
        }
        loaded_data = saver.load( "el", "ppos_y", m_pars.iteration_to_restart );
        for( size_t i = 0; i != loaded_data.size( ); ++i )
        {
            electronHolder.m_particles[ i ].y( ) = loaded_data[ i ];
        }

        loaded_data = saver.load( "pt", "ppos_x", m_pars.iteration_to_restart );
        for( size_t i = 0; i != loaded_data.size( ); ++i )
        {
            protonHolder.m_particles[ i ].x( ) = loaded_data[ i ];
        }
        loaded_data = saver.load( "pt", "ppos_y", m_pars.iteration_to_restart );
        for( size_t i = 0; i != loaded_data.size( ); ++i )
        {
            protonHolder.m_particles[ i ].y( ) = loaded_data[ i ];
        }
    }
    else
    {
        throw std::runtime_error( "unsupported dimension in load particles" );
    }
    return { electronHolder, protonHolder };
}

template < int Dim, class MeshType, class RealType >
ParticleHolders< MeshType, RealType > ParticleGenerator< Dim, MeshType, RealType >::generate( )
{
    using namespace std;

    using arr_t = typename MeshType::real_arr_t;

    auto number_processes = mpi::number_processes< arr_t >( );
    if( number_processes != 1 )
    {
        auto new_ppc = size_t( m_pars.particlesPerCell ) / size_t( number_processes );
        if( mpi::is_master< arr_t >( ) )
        {
            std::cout << "non master ppc: " << new_ppc << std::endl;
            new_ppc += m_pars.particlesPerCell - ( new_ppc * number_processes );
            std::cout << "master ppc: " << new_ppc << std::endl;
        }
        m_pars.particlesPerCell = new_ppc;
    }

    if( m_pars.particlesPerCell == 0 )
    {
        throw std::runtime_error( "0 particles per cell" );
    }

    auto numberOfSuperparticlesInMesh =
        m_pars.particlesPerCell * m_mesh.cells( );

    ParticleHolder< MeshType, RealType > electronHolder( "el", numberOfSuperparticlesInMesh, 1, -1, m_mesh );
    ParticleHolder< MeshType, RealType > protonHolder( "pt", numberOfSuperparticlesInMesh, m_pars.mp_me, 1, m_mesh );

    if( mpi::is_master< arr_t >( ) )
    {

        std::cout << "particles: " << numberOfSuperparticlesInMesh << " per " << 2 << " species" << std::endl;

        std::cout << electronHolder.pname << ": charge: " << electronHolder.m_qs_qe
                  << ", weight: " << electronHolder.m_ms_me << std::endl;

        std::cout << protonHolder.pname << ": charge: " << protonHolder.m_qs_qe
                  << ", weight: " << protonHolder.m_ms_me << std::endl;
    }

    normal_distribution< RealType > vtheParDistr( 0, 1 );
    normal_distribution< RealType > vthePerDistr( 0, sqrt( m_pars.T_e_per_T_e_par ) );

    const RealType T_p_T_e{ m_pars.beta_p_parallel / m_pars.beta_e_parallel };
    normal_distribution< RealType > vthpParDistr(
        0, sqrt( T_p_T_e / m_pars.mp_me ) );
    normal_distribution< RealType > vthpPerDistr(
        0, sqrt( T_p_T_e * m_pars.T_p_per_T_p_par / m_pars.mp_me ) );

    position_generator< Dim, MeshType, RealType > generator;
    generator( m_mesh, electronHolder, m_pars.particlesPerCell, m_generator, vthePerDistr, vtheParDistr, m_pars );
    generator( m_mesh, protonHolder, m_pars.particlesPerCell, m_generator, vthpPerDistr, vthpParDistr, m_pars );

    return { electronHolder, protonHolder };
}

}//namespace particles

#endif//PROJECT_PARTICLEGENERATOR_H
