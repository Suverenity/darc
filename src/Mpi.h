#pragma once

#ifdef USE_MPI
#include <mpi.h>
#endif

#include <algorithm>
#include <iostream>
#include <numeric>

namespace particles
{
namespace mpi
{

#ifdef USE_MPI

template < typename Arr >
class MPI_handler
{
public:
    static MPI_handler< Arr > & get_instance( )
    {
        static MPI_handler instance;
        return instance;
    }

    template < typename MeshType >
    void create_buffer( MeshType & mesh )
    {
        buffer = mesh.create_arr( );
    }

    Arr buffer;
    int number_processes{ };
    int current_process_id{ };

private:
    MPI_handler( )
    {
        MPI_Init( NULL, NULL );
        MPI_Comm_size( MPI_COMM_WORLD, &number_processes );
        MPI_Comm_rank( MPI_COMM_WORLD, &current_process_id );
    }

    ~MPI_handler( )
    {
        MPI_Finalize( );
    }
};

template < typename realtype_t >
struct RealToMpiOp
{
    using original_t = realtype_t;
};

constexpr MPI_Datatype convert_realtype( RealToMpiOp< double > )
{
    return MPI_DOUBLE;
}

constexpr MPI_Datatype convert_realtype( RealToMpiOp< float > )
{
    return MPI_FLOAT;
}

template < typename Array >
int number_processes( )
{
    return MPI_handler< Array >::get_instance( ).number_processes;
}

template < typename Array >
int current_process_id( )
{
    return MPI_handler< Array >::get_instance( ).current_process_id;
}

template < typename Array >
bool is_master( )
{
    return MPI_handler< Array >::get_instance( ).current_process_id == 0;
}

template < typename Array >
void all_reduce_sum( Array & in )
{
    auto & buffer = MPI_handler< Array >::get_instance( ).buffer;
    int res       = MPI_Allreduce( in.data( ), buffer.data( ), in.size( ),
                             convert_realtype( RealToMpiOp< typename Array::value_type >{ } ),
                             MPI_SUM, MPI_COMM_WORLD );
    if( res != MPI_SUCCESS )
    {
        throw std::runtime_error( std::string( "all_reduce_sum failed with err: " ) + std::to_string( res ) );
    }
    std::copy( buffer.begin( ), buffer.end( ), in.begin( ) );
}

// no mpi implementation
#else

template < typename Arr >
class MPI_handler
{
public:
    static MPI_handler< Arr > & get_instance( )
    {
        static MPI_handler instance;
        return instance;
    }

    template < typename MeshType >
    void create_buffer( MeshType & mesh )
    {
    }

private:
    MPI_handler( )
    {
    }

    ~MPI_handler( )
    {
    }
};

template < typename Array >
void all_reduce_sum( Array & in )
{
}

template < typename Array >
int number_processes( )
{
    return 1;
}

template < typename Array >
int current_process_id( )
{
    return 0;
}

template < typename Array >
bool is_master( )
{
    return true;
}

#endif

}// namespace mpi
}// namespace particles
