//
// Created by suverenity on 7/12/18.
//

#ifndef PROJECT_PERIODICBOUNDARYCONDITIONS_H
#define PROJECT_PERIODICBOUNDARYCONDITIONS_H

#include "ParticleHolder.h"
#include "Types.h"

namespace particles
{

template < int dim, typename MeshType, typename RealType >
class PeriodicBoundaryConditions
{
};

template < typename MeshType, typename RealType >
class PeriodicBoundaryConditions< 1, MeshType, RealType >
{
public:
    PeriodicBoundaryConditions(
        MeshType & mesh, ParticleHolders< MeshType, RealType > & particles )
        : m_mesh( mesh )
        , m_particles( particles )
    {
    }

    // this presumes that particle movements will not be bigger than size of the grid
    void applyParticlesBoundary( )
    {
        for( auto & particles : m_particles )
        {
#pragma omp parallel for
            for( size_t i = 0; i != particles.m_particles.size( ); ++i )
            {
                auto & p = particles.m_particles[ i ];
                recomputeParticleByBoundary( p );
            }
        }
    }

    void inline recomputeParticleByBoundary( Particle< RealType > & p )
    {
        const auto xMin  = m_mesh.m_xMin;
        const auto xMax  = m_mesh.m_xMax;
        const auto xSize = m_mesh.m_xSize;

        if( p.m_x[ 0 ] < xMin )
        {
            p.m_x[ 0 ] += xSize;
            return;
        }
        if( p.m_x[ 0 ] >= xMax )
        {
            p.m_x[ 0 ] -= xSize;
            return;
        }
    }

    MeshType & m_mesh;
    ParticleHolders< MeshType, RealType > & m_particles;
};

template < typename MeshType, typename RealType >
class PeriodicBoundaryConditions< 2, MeshType, RealType >
{
public:
    PeriodicBoundaryConditions(
        MeshType & mesh, ParticleHolders< MeshType, RealType > & particles )
        : m_mesh( mesh )
        , m_particles( particles )
    {
    }

    // this presumes that particle movements will not be bigger than size of the grid
    void applyParticlesBoundary( )
    {
        for( auto & particles : m_particles )
        {
            for( size_t i = 0; i != particles.m_particles.size( ); ++i )
            {
                auto & p = particles.m_particles[ i ];
                recomputeParticleByBoundary( p );
            }
        }
    }

    void inline recompute_x( Particle< RealType > & p )
    {
        const auto xMin  = m_mesh.m_xMin;
        const auto xMax  = m_mesh.m_xMax;
        const auto xSize = m_mesh.m_xSize;
        if( p.x( ) < xMin )
        {
            p.x( ) += xSize;
            return;
        }
        if( p.x( ) >= xMax )
        {
            p.x( ) -= xSize;
            return;
        }
    }

    void inline recompute_y( Particle< RealType > & p )
    {
        const auto yMin  = m_mesh.m_yMin;
        const auto yMax  = m_mesh.m_yMax;
        const auto ySize = m_mesh.m_ySize;
        if( p.y( ) < yMin )
        {
            p.y( ) += ySize;
            return;
        }
        if( p.y( ) >= yMax )
        {
            p.y( ) -= ySize;
            return;
        }
    }

    void inline recomputeParticleByBoundary( Particle< RealType > & p )
    {
        recompute_x( p );
        recompute_y( p );
    }

    MeshType & m_mesh;
    ParticleHolders< MeshType, RealType > & m_particles;
};

}// namespace particles

#endif//PROJECT_PERIODICBOUNDARYCONDITIONS_H
