#pragma once

#include "Array_2d.h"
#include "Particle.h"
#include "Vector.h"
#include "fftw_allocator.h"

#include <complex>
#include <ostream>
#include <type_traits>
#include <vector>

namespace particles
{

template < int Dim >
struct ArrayBase;

template <>
struct ArrayBase< 1 >
{
    template < typename TypeReal >
    using type = typename std::vector< TypeReal, fftw_allocator< TypeReal > >;
};

template <>
struct ArrayBase< 2 >
{
    template < typename TypeReal >
    using type = typename particles::Array2d< TypeReal, fftw_allocator< TypeReal > >;
};

template < int Dim, class TypeReal >
using Array = typename ArrayBase< Dim >::template type< TypeReal >;

template < int Dim, class TypeReal >
using ArrayComplex = Array< Dim, std::complex< TypeReal > >;

using Array1D_r_d = Array< 1, double >;
using Array1D_r_f = Array< 1, float >;

using Array2D_r_d = Array< 2, double >;
using Array2D_r_f = Array< 2, float >;

template < typename RealType >
using complex_t = std::complex< RealType >;

//template < int Dim >
//using Array_c_d = Array< Dim, complex_t< double > >;

//template < int Dim >
//using Array_c_f = Array< Dim, complex_t< float > >;

template < class Tdata >
std::ostream & operator<<( std::ostream & os, Array< 1, Tdata > & vec )
{
    os << "[";
    for( auto it = vec.begin( ); it != vec.end( ); it++ )
    {
        os << ( ( it != vec.begin( ) ) ? "," : "" ) << *it;
    }
    os << "]";
    return os;
}

struct float_or_double
{
#ifdef USE_FLOAT
    using type = float;
#else
    using type = double;
#endif
};

template < bool is_float >
struct complex_s
{
};

template <>
struct complex_s< true >
{
    static constexpr std::complex< float > one{ 1, 0 };
};

template <>
struct complex_s< false >
{
    static constexpr std::complex< double > one{ 1, 0 };
};

using complex = complex_s< std::is_same< float, float_or_double::type >::value >;

}// namespace particles
