//
// Created by suverenity on 5/24/18.
//

#ifndef PROJECT_BORISMOVER_H
#define PROJECT_BORISMOVER_H

#include "Mpi.h"
#include "Particle.h"
#include "ParticleHolder.h"
#include "Types.h"

#include <vector>

namespace particles
{

template < int Dim, class MeshType, class GridParticlesWeighting, class BoundaryConditions, class RealType >
class BorisMover
{
public:
    using real_arr_t = typename MeshType::real_arr_t;
    BorisMover( RealType dt, GridParticlesWeighting & weighting, BoundaryConditions & boundary,
                ParticleHolders< MeshType, RealType > & particles,
                real_arr_t & Ex, real_arr_t & Ey, real_arr_t & Ez,
                real_arr_t & Bx, real_arr_t & By, real_arr_t & Bz,
                RealType ppc )
        : m_weighting( weighting )
        , m_boundary( boundary )
        , m_dt( dt )
        , m_dt_2( dt / 2 )
        , m_dt_inv( 1. / dt )
        , m_particles( particles )
        , m_ExGrid( Ex )
        , m_EyGrid( Ey )
        , m_EzGrid( Ez )
        , m_BxGrid( Bx )
        , m_ByGrid( By )
        , m_BzGrid( Bz )
        , inversed_ppc( RealType{ 1 } / ppc )
    {
    }

    void run_with_map( )
    {
        for( auto & species : m_particles )
        {
            std::fill( species.m_rho.begin( ), species.m_rho.end( ), 0. );

            std::fill( species.m_jx.begin( ), species.m_jx.end( ), 0. );
            std::fill( species.m_jy.begin( ), species.m_jy.end( ), 0. );
            std::fill( species.m_jz.begin( ), species.m_jz.end( ), 0. );
            for( auto & p : species.m_particles )
            {
                p.m_v = computeSpeedForOneParticle( p, species.m_qs_qe, species.m_ms_me );
                p.m_x = p.m_x + p.m_v * m_dt;
                m_weighting.runOneParticle(
                    p, species.m_qs_qe, species.m_rho, species.m_jx,
                    species.m_jy, species.m_jz );
                m_boundary.recomputeParticleByBoundary( p );
            }

            mpi::all_reduce_sum( species.m_rho );

            mpi::all_reduce_sum( species.m_jx );
            mpi::all_reduce_sum( species.m_jy );
            mpi::all_reduce_sum( species.m_jz );
            const auto size = species.m_rho.size( );
            for( size_t i = 0; i != size; ++i )
            {
                species.m_rho[ i ] *= inversed_ppc;

                species.m_jx[ i ] *= inversed_ppc;
                species.m_jy[ i ] *= inversed_ppc;
                species.m_jz[ i ] *= inversed_ppc;
            }
        }
    }

    void run( )
    {
        for( auto & particles : m_particles )
        {
#pragma omp parallel for
            for( auto partIdx = 0u; partIdx < particles.m_particles.size( ); partIdx++ )
            {
                auto & p = particles.m_particles[ partIdx ];
                p.m_v    = computeSpeedForOneParticle( p, particles.m_qs_qe, particles.m_ms_me );
                p.m_x    = p.m_x + p.m_v * m_dt;
            }
        }
    }

    // to avoid unnecessary memory allocations this method automatically
    // interpolates between grid quantities and particle
    VectorClass< RealType > computeSpeedForOneParticle(
        Particle< RealType > & p, RealType qs_qe, RealType ms_me )
    {
        const RealType q_m = qs_qe / ms_me;

        const RealType qdt_2_m = q_m * m_dt_2;
        const VectorClass< RealType > ETmp( m_weighting.weightQuantityOnParticle( p, m_ExGrid ),
                                            m_weighting.weightQuantityOnParticle( p, m_EyGrid ),
                                            m_weighting.weightQuantityOnParticle( p, m_EzGrid ) );
        const VectorClass< RealType > BTmp( m_weighting.weightQuantityOnParticle( p, m_BxGrid ),
                                            m_weighting.weightQuantityOnParticle( p, m_ByGrid ),
                                            m_weighting.weightQuantityOnParticle( p, m_BzGrid ) );

        //acceleration with efield and half step
        const auto initV   = p.m_v;
        const auto v_minus = initV + qdt_2_m * ETmp;
        //full rotation
        const auto t        = qdt_2_m * BTmp;
        const auto s        = 2 * t * ( 1. / ( 1. + t.L2Squared( ) ) );
        const auto v_primed = v_minus + v_minus.crossProduct( t );
        const auto v_plus   = v_minus + v_primed.crossProduct( s );
        //acceleration with efield second half step
        const auto v_new = v_plus + qdt_2_m * ETmp;
        return v_new;
    }

public:
    GridParticlesWeighting & m_weighting;
    BoundaryConditions & m_boundary;
    const RealType m_dt;
    const RealType m_dt_2;
    const RealType m_dt_inv;
    ParticleHolders< MeshType, RealType > & m_particles;

    real_arr_t & m_ExGrid;
    real_arr_t & m_EyGrid;
    real_arr_t & m_EzGrid;

    real_arr_t & m_BxGrid;
    real_arr_t & m_ByGrid;
    real_arr_t & m_BzGrid;

    RealType inversed_ppc;
};

}//namespace particles

#endif//PROJECT_BORISMOVER_H
