
include(ExternalProject)
include(ProcessorCount)

ProcessorCount(CPUS)

set(HDF5_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/hdf5)
set(HDF5_INSTALL_DIR ${CMAKE_CURRENT_BINARY_DIR}/hdf5)
set(HDF5_LIB_DIR ${HDF5_INSTALL_DIR}/lib)
set(HDF5_INCLUDE_DIR ${HDF5_INSTALL_DIR}/include)

message("hdf5 source dir: ${HDF5_SOURCE_DIR}")
message("hdf5 install dir: ${HDF5_INSTALL_DIR}")
message("hdf5 lib dir: ${HDF5_LIB_DIR}")
message("hdf5 include dir: ${HDF5_INCLUDE_DIR}")

ExternalProject_Add(hdf5_external
    PREFIX ${HDF5_INSTALL_DIR}
    SOURCE_DIR ${HDF5_SOURCE_DIR}
    CMAKE_ARGS -D "CMAKE_LINKER=${CMAKE_LINKER}" -D "CMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}" -D "CMAKE_C_COMPILER=${CMAKE_C_COMPILER}" -D CMAKE_BUILD_TYPE=Release -D "CMAKE_INSTALL_PREFIX=${CMAKE_CURRENT_BINARY_DIR}/hdf5"
)

#libhdf5.a  libhdf5_cpp.a  libhdf5_hl.a  libhdf5_hl_cpp.a  libhdf5_tools.a

file(MAKE_DIRECTORY ${HDF5_INCLUDE_DIR})

add_library(hdf5_core STATIC IMPORTED)
set_target_properties(hdf5_core PROPERTIES 
    IMPORTED_LOCATION ${HDF5_LIB_DIR}/libhdf5.a
    INTERFACE_INCLUDE_DIRECTORIES ${HDF5_INCLUDE_DIR}
)
add_dependencies(hdf5_core hdf5_external)

add_library(hdf5_cpp STATIC IMPORTED)
set_target_properties(hdf5_cpp PROPERTIES 
    IMPORTED_LOCATION ${HDF5_LIB_DIR}/libhdf5_cpp.a
    INTERFACE_INCLUDE_DIRECTORIES ${HDF5_INCLUDE_DIR}
)
add_dependencies(hdf5_cpp hdf5_external)

add_library(hdf5_hl STATIC IMPORTED)
set_target_properties(hdf5_hl PROPERTIES 
    IMPORTED_LOCATION ${HDF5_LIB_DIR}/libhdf5_hl.a
    INTERFACE_INCLUDE_DIRECTORIES ${HDF5_INCLUDE_DIR}
)
add_dependencies(hdf5_hl hdf5_external)

add_library(hdf5_hl_cpp STATIC IMPORTED)
set_target_properties(hdf5_hl_cpp PROPERTIES 
    IMPORTED_LOCATION ${HDF5_LIB_DIR}/libhdf5_hl_cpp.a
    INTERFACE_INCLUDE_DIRECTORIES ${HDF5_INCLUDE_DIR}
)
add_dependencies(hdf5_hl_cpp hdf5_external)

add_library(hdf5 INTERFACE IMPORTED GLOBAL)
add_dependencies(hdf5 hdf5_core hdf5_hl hdf5_hl_cpp hdf5_cpp)
set_property(TARGET hdf5 PROPERTY INTERFACE_LINK_LIBRARIES hdf5_cpp hdf5_hl_cpp hdf5_hl hdf5_core dl)


######################################## FFTW3 ########################################

set(FFTW3_CFG --prefix=${CMAKE_CURRENT_BINARY_DIR}/fftw3 --enable-sse --enable-sse2 --enable-avx --enable-avx2 --enable-avx512 --enable-avx-128-fma --disable-fortran)

if(${USE_FLOAT})
    list(APPEND FFTW3_CFG --enable-float)
endif(${USE_FLOAT})

if(${USE_MPI})
    list(APPEND FFTW3_CFG --enable-mpi)
endif(${USE_MPI})


message("fftw3 configure parameters: ${FFTW3_CFG}")

set(FFTW3_INSTALL_DIR ${CMAKE_CURRENT_BINARY_DIR}/fftw3)
set(FFTW3_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/fftw3)
set(FFTW3_INSTALL_LIB_DIR ${FFTW3_INSTALL_DIR}/lib)
set(FFTW3_INSTALL_INCLUDE_DIR ${FFTW3_INSTALL_DIR}/include)

message("fftw3 install directory: ${FFTW3_INSTALL_DIR}")
message("fftw3 source directory: ${FFTW3_SOURCE_DIR}")
message("fftw3 install lib directory: ${FFTW3_INSTALL_LIB_DIR}")
message("fftw3 install include directory: ${FFTW3_INSTALL_INCLUDE_DIR}")

ExternalProject_Add(fftw3_external
    PREFIX ${FFTW3_INSTALL_DIR}
    SOURCE_DIR ${FFTW3_SOURCE_DIR}
    CONFIGURE_COMMAND ${FFTW3_SOURCE_DIR}/configure "${FFTW3_CFG}"
    INSTALL_DIR ${FFTW3_INSTALL_DIR}
)

add_library(fftw3_core STATIC IMPORTED GLOBAL)
add_dependencies(fftw3_core fftw3_external)

file(MAKE_DIRECTORY ${FFTW3_INSTALL_INCLUDE_DIR})
set_target_properties(fftw3_core PROPERTIES INTERFACE_INCLUDE_DIRECTORIES ${FFTW3_INSTALL_INCLUDE_DIR})

if(${USE_MPI})
    add_library(fftw3_mpi STATIC IMPORTED GLOBAL)
    add_dependencies(fftw3_mpi fftw3_external)
    set_target_properties(fftw3_mpi PROPERTIES INTERFACE_INCLUDE_DIRECTORIES ${FFTW3_INSTALL_INCLUDE_DIR})
endif(${USE_MPI})

if(${USE_FLOAT})
    set_target_properties(fftw3_core PROPERTIES 
        IMPORTED_LOCATION ${FFTW3_INSTALL_LIB_DIR}/libfftw3f.a
    )
    if(${USE_MPI})
        set_target_properties(fftw3_mpi PROPERTIES 
            IMPORTED_LOCATION ${FFTW3_INSTALL_LIB_DIR}/libfftw3f_mpi.a
            )
    endif(${USE_MPI})

else(${USE_FLOAT})
    set_target_properties(fftw3_core PROPERTIES 
        IMPORTED_LOCATION ${FFTW3_INSTALL_LIB_DIR}/libfftw3.a
        )
    if(${USE_MPI})
        set_target_properties(fftw3_mpi PROPERTIES 
            IMPORTED_LOCATION ${FFTW3_INSTALL_LIB_DIR}/libfftw3_mpi.a
        )
    endif(${USE_MPI})
endif(${USE_FLOAT})

add_library(fftw3 INTERFACE IMPORTED GLOBAL)

if(${USE_MPI})
    set(fftw3_libs  fftw3_mpi fftw3_core )
else(${USE_MPI})
    set(fftw3_libs fftw3_core)
endif(${USE_MPI})

add_dependencies(fftw3 ${fftw3_libs})
set_property(TARGET fftw3 PROPERTY INTERFACE_LINK_LIBRARIES ${fftw3_libs})



