
#include "../src/BorisMover.h"
#include "../src/Controller.h"
#include "../src/FFTFieldSolver.h"
#include "../src/Fourier.h"
#include "../src/FromParticlesToGrid.h"
#include "../src/HDFSaver.h"
#include "../src/JsonParser.h"
#include "../src/Mesh.h"
#include "../src/Mpi.h"
#include "../src/PeriodicBoundaryConditions.h"

#include "../src/Types.h"

#include <iostream>
#include <math.h>
#include <string>

void fft2d_derivative_test( )
{
    using namespace particles;

    using type = float_or_double::type;
    Parameters< type > pars;
    pars.xmin = -10;
    pars.xmax = 10;
    pars.ymin = -10;
    pars.ymax = 10;

    pars.particlesPerCell = 0;

    pars.numberOfXCells = 10;
    pars.numberOfYCells = 100;

    constexpr int dim         = 2;
    using MeshType            = Mesh< dim, type >;
    using BoundaryConditions  = PeriodicBoundaryConditions< dim, MeshType, type >;
    using FromParticlestoGrid = FromParticlesToGridFirstOrder< dim, MeshType, type >;
    using Mover               = BorisMover< dim, MeshType, FromParticlestoGrid, BoundaryConditions, type >;
    using Solver              = FFTFieldSolver< dim, MeshType, BoundaryConditions,
                                   FromParticlestoGrid, Mover, type >;
    using ControllerType      = Controller< dim,
                                       MeshType,
                                       Mover,
                                       Solver,
                                       BoundaryConditions,
                                       FromParticlestoGrid,
                                       type >;

    ControllerType controller( pars );

    auto & mesh = controller.m_mesh;
    auto & to   = controller.m_fieldSolver;

    auto & rho = to.rho;

    for( size_t y = 0; y != rho.m_ysize; ++y )
    {
        for( size_t x = 0; x != rho.m_xsize; ++x )
        {
            const auto x_d = mesh.x( x );
            const auto y_d = mesh.y( y );

            rho.xy_coord( x, y ) = type( 1 ) / ( 2. * M_PI ) *
                std::exp( -( x_d * x_d + y_d * y_d ) / 2. );
        }
    }

    controller.saver.saveGridQuantity( rho, "gauss", 0 );

    to.ftRho.runForward( );

    using namespace std::complex_literals;

    auto & rho_complex = to.ftRho.m_fourierArr;

    for( size_t y = 0; y != rho_complex.m_ysize; ++y )
    {
        for( size_t x = 0; x != rho_complex.m_xsize; ++x )
        {
            const auto rho_v             = rho_complex.xy_coord( x, y );
            rho_complex.xy_coord( x, y ) = complex::one * to.kk_x[ x ] * rho_v;
        }
    }
    for( size_t x = 0; x != rho_complex.m_xsize; ++x )
    {
        rho_complex.xy_coord( x, to.half_cells_y ) = 0i;
    }

    for( size_t y = 0; y != rho_complex.m_ysize; ++y )
    {
        rho_complex.xy_coord( to.half_cells_x, y ) = 0i;
    }

    to.ftRho.runBackward( );

    controller.saver.saveGridQuantity( rho, "gauss_x_der_fft", 0 );

    typename MeshType::real_arr_t x_gauss_derivative = controller.m_mesh.create_arr( );

    for( size_t y = 0; y != x_gauss_derivative.m_ysize; ++y )
    {
        for( size_t x = 0; x != x_gauss_derivative.m_xsize; ++x )
        {
            const auto x_d = mesh.x( x );
            const auto y_d = mesh.y( y );

            x_gauss_derivative.xy_coord( x, y ) = -x_d / ( 2. * M_PI ) *
                std::exp( -( x_d * x_d + y_d * y_d ) / 2. );
        }
    }

    controller.saver.saveGridQuantity( x_gauss_derivative, "x_gauss_derivative", 0 );

    auto compute_diff_between_arrs = []( auto & vec_a, auto & vec_b ) {
        type diff{ };
        for( size_t i = 0; i < vec_a.size( ); ++i )
        {
            diff = std::max( diff, std::abs( vec_a[ i ] - vec_b[ i ] ) );
        }
        return diff;
    };

    if( compute_diff_between_arrs( rho, x_gauss_derivative ) > 1e-2 )
    {
        throw std::runtime_error( "x derivative failed" );
    }

    typename MeshType::real_arr_t y_gauss_derivative = controller.m_mesh.create_arr( );

    for( size_t y = 0; y != x_gauss_derivative.m_ysize; ++y )
    {
        for( size_t x = 0; x != x_gauss_derivative.m_xsize; ++x )
        {
            const auto x_d                      = mesh.x( x );
            const auto y_d                      = mesh.y( y );
            y_gauss_derivative.xy_coord( x, y ) = -y_d / ( 2. * M_PI ) *
                std::exp( -( x_d * x_d + y_d * y_d ) / 2. );
        }
    }
    controller.saver.saveGridQuantity( y_gauss_derivative, "y_gauss_derivative", 0 );

    for( size_t y = 0; y != rho.m_ysize; ++y )
    {
        for( size_t x = 0; x != rho.m_xsize; ++x )
        {
            const auto x_d = mesh.x( x );
            const auto y_d = mesh.y( y );

            rho.xy_coord( x, y ) = type( 1 ) / ( 2. * M_PI ) *
                std::exp( -( x_d * x_d + y_d * y_d ) / 2. );
        }
    }

    to.ftRho.runForward( );

    for( size_t y = 0; y != rho_complex.m_ysize; ++y )
    {
        for( size_t x = 0; x != rho_complex.m_xsize; ++x )
        {
            const auto rho_v             = rho_complex.xy_coord( x, y );
            rho_complex.xy_coord( x, y ) = complex::one * to.kk_y[ y ] * rho_v;
        }
    }
    for( size_t x = 0; x != rho_complex.m_xsize; ++x )
    {
        rho_complex.xy_coord( x, to.half_cells_y ) = 0i;
    }

    for( size_t y = 0; y != rho_complex.m_ysize; ++y )
    {
        rho_complex.xy_coord( to.half_cells_x, y ) = 0i;
    }

    to.ftRho.runBackward( );

    if( compute_diff_between_arrs( rho, y_gauss_derivative ) > 1e-2 )
    {
        throw std::runtime_error( "y derivative failed" );
    }
}

void fft2d_test( )
{
    using namespace particles;
    using type = float_or_double::type;
    HDFSaver< 2, int, type > saver;

    Array< 1, type > a1;
    Array< 2, type > a2;

    const size_t X = 28;
    const size_t Y = 18;

    Array< 2, type > arr( Y, X, 0.5 );
    Array< 2, type > real_arr( Y, X, 0.5 );

    saver.saveGridQuantity( arr, "prefft", 0 );

    Fourier< 2, type > f( real_arr );
    {
        auto & ra = f.m_realArr;
        std::fill( ra.begin( ), ra.end( ), 0.5 );
    }

    f.runForward( );
    f.runBackward( );

    auto & convr_arr = f.m_realArr;

    saver.saveGridQuantity( convr_arr, "postfft", 0 );

    //std::cout << convr_arr << std::endl;

    for( size_t n1_idx = 0; n1_idx != convr_arr.m_n1; ++n1_idx )
    {
        for( size_t n0_idx = 0; n0_idx != convr_arr.m_n0; ++n0_idx )
        {
            if( std::abs( convr_arr( n0_idx, n1_idx ) - arr( n0_idx, n1_idx ) ) > 1e-4 )
            {
                std::cout << "n0:" << n0_idx << ",n1:" << n1_idx << "," << convr_arr( n0_idx, n1_idx ) << "," << arr( n0_idx, n1_idx ) << std::endl;
                throw std::runtime_error( "wrong conversion" );
            }
        }
    }
    std::cout << "all correct" << std::endl;
}

void bilinear_interpolation_test( )
{
    using namespace particles;
    //Array
    //Mesh( RealType xMin, RealType xMax, RealType yMin, RealType yMax, size_t numberOfXCells, size_t numberOfYCells );
    using type     = float_or_double::type;
    using MeshType = Mesh< 2, type >;
    MeshType mesh( 0, 2, 0, 2, 2, 2 );
    ParticleHolders< MeshType, type > parts;
    FromParticlesToGridFirstOrder< 2, MeshType, type > from_parts_to_grid( mesh, parts, 0 );

    auto arr             = mesh.create_arr( );
    arr.xy_coord( 0, 0 ) = 91;
    arr.xy_coord( 1, 0 ) = 210;
    arr.xy_coord( 0, 1 ) = 162;
    arr.xy_coord( 1, 1 ) = 95;

    Particle< type > p;
    p.x( ) = 0.4;
    p.y( ) = 0.2;
    if( std::abs( from_parts_to_grid.weightQuantityOnParticle( p, arr ) - type( 137.92 ) ) > 1e-4 )
    {
        std::cout << from_parts_to_grid.weightQuantityOnParticle( p, arr ) - type( 137.92 ) << std::endl;
        throw std::runtime_error( "wrong result" );
    }
}

int main( int argc, char const * argv[] )
{
    //bilinear_interpolation_test( );
    //return 0;
    //fft2d_derivative_test( );
    //return 0;

    using namespace particles;

    using type = float_or_double::type;

    JsonParser json;
    Parameters< type > pars;
    if( argc == 2 )
    {
        pars = json.parseParameters< type >( std::string( argv[ 1 ] ) );
    }

    constexpr int dim         = 2;
    using MeshType            = Mesh< dim, type >;
    using BoundaryConditions  = PeriodicBoundaryConditions< dim, MeshType, type >;
    using FromParticlestoGrid = FromParticlesToGridFirstOrder< dim, MeshType, type >;
    using Mover               = BorisMover< dim, MeshType, FromParticlestoGrid, BoundaryConditions, type >;
    using Solver              = FFTFieldSolver< dim, MeshType, BoundaryConditions,
                                   FromParticlestoGrid, Mover, type >;
    using ControllerType      = Controller< dim,
                                       MeshType,
                                       Mover,
                                       Solver,
                                       BoundaryConditions,
                                       FromParticlestoGrid,
                                       type >;

    ControllerType controller( pars );

    controller.run( );
}
