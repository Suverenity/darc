#include <algorithm>
#include <fstream>
#include <iostream>
#include <random>

#include "../src/BorisMover.h"
#include "../src/Controller.h"
#include "../src/FFTFieldSolver.h"
#include "../src/Fourier.h"
#include "../src/FromGridToParticles.h"
#include "../src/HDFSaver.h"
#include "../src/JsonParser.h"
#include "../src/Mesh.h"
#include "../src/Particle.h"
#include "../src/ParticleGenerator.h"
#include "../src/PeriodicBoundaryConditions.h"
#include "../src/Test.h"

#include "../src/Array_2d.h"
#include "../src/Parameters.h"

void test_particles_to_grid( )
{
    using namespace std;
    using namespace particles;

    using type = float_or_double::type;
    Parameters< type > pars;
    pars.numberOfXCells    = 10;
    pars.xmax              = 10;
    pars.xmin              = 0;
    pars.particlesPerCell  = 2;
    pars.n0                = 1;
    pars.beta_e_parallel   = 1;
    pars.beta_p_parallel   = 1;
    pars.omega_pe_omega_ce = 1;
    pars.T_e_per_T_e_par   = 1;
    pars.T_p_per_T_p_par   = 1;

    pars.recompute_parameters( );

    constexpr int dim         = 1;
    using MeshType            = Mesh< dim, type >;
    using BoundaryConditions  = PeriodicBoundaryConditions< dim, MeshType, type >;
    using FromParticlestoGrid = FromParticlesToGridFirstOrder< dim, MeshType, type >;
    using Mover               = BorisMover< dim, MeshType, FromParticlestoGrid, BoundaryConditions, type >;
    using Solver              = FFTFieldSolver<
        dim, MeshType, BoundaryConditions, FromParticlestoGrid, Mover, type >;
    using ControllerType = Controller<
        dim,
        MeshType,
        Mover,
        Solver,
        BoundaryConditions,
        FromParticlestoGrid,
        type >;

    ControllerType c( pars );

    //auto & electrons = c.m_particles[ 0 ].m_particles;
    //for( auto & e : electrons )
    //{
    //    e.m_x.x( ) = 0.25;
    //    e.m_v      = { -1, 1, 1 };
    //}

    //auto & protons = c.m_particles[ 1 ].m_particles;
    //for( auto & p : protons )
    //{
    //    p.m_x.x( ) = 9.75;
    //    p.m_v      = { -1, 1, -1 };
    //}

    c.m_particlesToGrid.run( );

    //std::cout << "rho: " << std::endl;
    //for( auto & particles : c.m_particles )
    //{
    //    std::cout << particles.m_rho << std::endl;
    //}

    //std::cout << "jx: " << std::endl;
    //for( auto & particles : c.m_particles )
    //{
    //    std::cout << particles.m_jx << std::endl;
    //}

    //std::cout << "jy: " << std::endl;
    //for( auto & particles : c.m_particles )
    //{
    //    std::cout << particles.m_jy << std::endl;
    //}

    //std::cout << "jz: " << std::endl;
    //for( auto & particles : c.m_particles )
    //{
    //    std::cout << particles.m_jz << std::endl;
    //}
}

void test_mover( )
{
    using namespace std;
    using namespace particles;

    using type = float_or_double::type;
    Parameters< type > pars;
    pars.numberOfXCells    = 10;
    pars.xmax              = 100;
    pars.xmin              = -100;
    pars.particlesPerCell  = 2;
    pars.n0                = 1;
    pars.beta_e_parallel   = 1;
    pars.beta_p_parallel   = 1;
    pars.omega_pe_omega_ce = 1;
    pars.T_e_per_T_e_par   = 1;
    pars.T_p_per_T_p_par   = 1;
    pars.dt                = 0.001;

    pars.recompute_parameters( );

    constexpr int dim         = 1;
    using MeshType            = Mesh< dim, type >;
    using BoundaryConditions  = PeriodicBoundaryConditions< dim, MeshType, type >;
    using FromParticlestoGrid = FromParticlesToGridFirstOrder< dim, MeshType, type >;
    using Mover               = BorisMover< dim, MeshType, FromParticlestoGrid, BoundaryConditions, type >;
    using Solver              = FFTFieldSolver< dim, MeshType, BoundaryConditions, FromParticlestoGrid, Mover, type >;
    using ControllerType      = Controller< dim, MeshType, Mover, Solver, BoundaryConditions, FromParticlestoGrid, type >;

    ControllerType c( pars );

    std::fill( c.m_Ex.begin( ), c.m_Ex.end( ), 1 );
    //std::fill( c.m_Ey.begin( ), c.m_Ey.end( ), -1 );
    //std::fill( c.m_Ey.begin( ), c.m_Ey.end( ), 1 );
    //std::fill( c.m_By.begin( ), c.m_By.end( ), 1. / 2 );
    //std::fill( c.m_Bz.begin( ), c.m_Bz.end( ), 1. / 2 );
    //std::fill( c.m_Bz.begin( ), c.m_Bz.end( ), 0 );
    c.m_particles[ 0 ].m_particles.resize( 1 );
    c.m_particles[ 1 ].m_particles.resize( 0 );
    c.m_particles[ 0 ].m_particles[ 0 ].m_v = { 0, 0, 0 };
    c.m_particles[ 0 ].m_particles[ 0 ].m_x = { 0, 0, 0 };

    double time{ };
    const double max_time{ 1000 };

    std::ofstream file( "test_mover" );
    auto & x_part = c.m_particles[ 0 ].m_particles[ 0 ].m_x;

    c.m_particles[ 0 ].m_ms_me = 1;
    c.m_particles[ 0 ].m_qs_qe = 1;

    while( time < max_time )
    {
        //for( size_t i = 0; i != 10; ++i )
        //{
        c.m_mover.run( );
        //        file << x_part[ 0 ] << " " << x_part[ 1 ] << " " << x_part[ 2 ] << std::endl;
        time += pars.dt;
        //std::cout << "iteration in time " << time << "!!!!!!!!!!!" << std::endl;
    }

    //according to kulhanek particle should be at
    // x = 0,438448084, y = −999,173558494
    if( std::abs( x_part[ 0 ] - 0.438448084 ) > 1e-2 )
    {
        std::cout << "x is " << x_part[ 0 ] << "vs " << 0.438448084 << std::endl;
        throw std::runtime_error( "wrong location" );
    }

    if( std::abs( x_part[ 1 ] + 999.173558494 ) > 1e-2 )
    {
        std::cout << "y is " << x_part[ 1 ] << "vs " << 999.173558494 << std::endl;
        throw std::runtime_error( "wrong location" );
    }

    if( std::abs( x_part[ 2 ] - 0 ) > 1e-4 )
    {
        std::cout << "z is " << x_part[ 2 ] << "vs " << 0 << std::endl;
        throw std::runtime_error( "wrong location" );
    }
}

void test_fft_derivative_odd_number_cells( )
{
    using namespace std;
    using namespace particles;

    using type = float_or_double::type;

    Parameters< type > pars;
    pars.numberOfXCells = 999;
    pars.xmax           = 100;
    pars.xmin           = 0;

    constexpr int dim         = 1;
    using MeshType            = Mesh< dim, type >;
    using BoundaryConditions  = PeriodicBoundaryConditions< dim, MeshType, type >;
    using FromParticlestoGrid = FromParticlesToGridFirstOrder< dim, MeshType, type >;
    using Mover               = BorisMover< dim, MeshType, FromParticlestoGrid, BoundaryConditions, type >;
    using Solver              = FFTFieldSolver< dim, MeshType, BoundaryConditions,
                                   FromParticlestoGrid, Mover, type >;
    using ControllerType      = Controller<
        dim,
        MeshType,
        Mover,
        Solver,
        BoundaryConditions,
        FromParticlestoGrid,
        type >;

    ControllerType controller( pars );
    auto & solver = controller.m_fieldSolver;

    //////squre wave
    //for( size_t i = 0; i != gs; ++i )
    //{
    //    if( i < gs / 4 )
    //        function_v[ i ] = 1;
    //    else if( i < gs * 3. / 4. )
    //        function_v[ i ] = -1;
    //    else
    //        function_v[ i ] = 1;
    //}

    /////triangle function
    //const float period = float( pars.xmax - pars.xmin ) / 3.;
    //for( size_t i = 0; i != gs; ++i )
    //{
    //    const type t          = controller.m_mesh.x( i );
    //    const type floor_part = floor( 2 * t / period + 0.5 );
    //    function_v[ i ]       = 4. / period *
    //        ( t - period / 2 * floor_part ) *
    //        pow( -1, floor_part );
    //}

    const auto gs = solver.m_rho.size( );
    Array< dim, type > sin_v_der;
    sin_v_der.resize( gs );

    ///sin
    for( size_t i = 0; i != gs; ++i )
    {
        auto x         = 2 * type( i ) * M_PI / gs;
        sin_v_der[ i ] = 4.5 * std::sin( x );
        //cosarr[ i ] = std::cos( x );
    }

    Array< dim, type > cos_v;
    Array< dim, type > minus_cos_v;
    cos_v.resize( gs );
    minus_cos_v.resize( gs );
    for( size_t i = 0; i != gs; ++i )
    {
        auto x           = 2 * type( i ) * M_PI / gs;
        cos_v[ i ]       = 4.5 * std::cos( x );
        minus_cos_v[ i ] = -4.5 * std::cos( x );
    }

    Fourier< dim, type > f_der( sin_v_der );
    Array< dim, type > sin_v_integr( sin_v_der.begin( ), sin_v_der.end( ) );
    Fourier< dim, type > f_integr( sin_v_integr );

    f_der.runForward( );
    f_integr.runForward( );

    auto & fourier_v_der = f_der.m_fourierArr;

    // note: as controller k numbers are normalized on ints [xmin, xmax] it does has to be resized on this example
    // derivation
    for( size_t j = 0; j != fourier_v_der.size( ); ++j )
    {
        fourier_v_der[ j ] *= particles::complex::one * solver.m_kk[ j ] * ( pars.xmax - pars.xmin ) / ( 2 * static_cast< type >( M_PI ) );
    }

    auto & fourier_v_integr = f_integr.m_fourierArr;

    // integration
    for( size_t j = 0; j != fourier_v_integr.size( ); ++j )
    {
        fourier_v_integr[ j ] /= particles::complex::one * solver.m_kk[ j ] * ( pars.xmax - pars.xmin ) / ( 2 * static_cast< type >( M_PI ) );
    }

    fourier_v_integr[ 0 ]                       = { };
    fourier_v_der[ 0 ]                          = { };
    fourier_v_integr[ pars.numberOfXCells / 2 ] = { };
    fourier_v_der[ pars.numberOfXCells / 2 ]    = { };

    f_der.runBackward( );
    f_integr.runBackward( );

    type diff{ 0 };
    for( size_t i = 0; i < cos_v.size( ); ++i )
    {
        diff = std::max( cos_v[ i ] - f_der.m_realArr[ i ], diff );
    }
    if( diff > 1e-4 )
    {
        std::cout << "der dif: " << diff << std::endl;
        throw std::runtime_error( std::string( "der diff: " ) + std::to_string( diff ) );
    }

    diff = 0;
    for( size_t i = 0; i < cos_v.size( ); ++i )
    {
        diff = std::max( minus_cos_v[ i ] - f_integr.m_realArr[ i ], diff );
    }
    if( diff > 1e-4 )
    {
        std::cout << "int dif: " << diff << std::endl;
        throw std::runtime_error( std::string( "integr diff: " ) + std::to_string( diff ) );
    }
}

void test_fft_derivative_even_number_cells( )
{
    using namespace std;
    using namespace particles;

    using type = float_or_double::type;

    Parameters< type > pars;
    pars.numberOfXCells = 1000;
    pars.xmax           = 100;
    pars.xmin           = 0;

    constexpr int dim         = 1;
    using MeshType            = Mesh< dim, type >;
    using BoundaryConditions  = PeriodicBoundaryConditions< dim, MeshType, type >;
    using FromParticlestoGrid = FromParticlesToGridFirstOrder< dim, MeshType, type >;
    using Mover               = BorisMover< dim, MeshType, FromParticlestoGrid, BoundaryConditions, type >;
    using Solver              = FFTFieldSolver< dim, MeshType, BoundaryConditions,
                                   FromParticlestoGrid, Mover, type >;
    using ControllerType      = Controller< dim, MeshType, Mover, Solver, BoundaryConditions, FromParticlestoGrid, type >;

    ControllerType controller( pars );
    auto & solver = controller.m_fieldSolver;

    //////squre wave
    //for( size_t i = 0; i != gs; ++i )
    //{
    //    if( i < gs / 4 )
    //        function_v[ i ] = 1;
    //    else if( i < gs * 3. / 4. )
    //        function_v[ i ] = -1;
    //    else
    //        function_v[ i ] = 1;
    //}

    /////triangle function
    //const float period = float( pars.xmax - pars.xmin ) / 3.;
    //for( size_t i = 0; i != gs; ++i )
    //{
    //    const type t          = controller.m_mesh.x( i );
    //    const type floor_part = floor( 2 * t / period + 0.5 );
    //    function_v[ i ]       = 4. / period *
    //        ( t - period / 2 * floor_part ) *
    //        pow( -1, floor_part );
    //}

    const auto gs = solver.m_rho.size( );
    Array< dim, type > sin_v_der;
    sin_v_der.resize( gs );

    ///sin
    for( size_t i = 0; i != gs; ++i )
    {
        auto x         = 2 * type( i ) * M_PI / gs;
        sin_v_der[ i ] = 4.5 * std::sin( x );
        //cosarr[ i ] = std::cos( x );
    }

    Array< dim, type > cos_v;
    Array< dim, type > minus_cos_v;
    cos_v.resize( gs );
    minus_cos_v.resize( gs );
    for( size_t i = 0; i != gs; ++i )
    {
        auto x           = 2 * type( i ) * M_PI / gs;
        cos_v[ i ]       = 4.5 * std::cos( x );
        minus_cos_v[ i ] = -4.5 * std::cos( x );
    }

    Fourier< dim, type > f_der( sin_v_der );
    Array< dim, type > sin_v_integr( sin_v_der.begin( ), sin_v_der.end( ) );
    Fourier< dim, type > f_integr( sin_v_integr );

    f_der.runForward( );
    f_integr.runForward( );

    auto & fourier_v_der = f_der.m_fourierArr;

    // note: as controller k numbers are normalized on ints [xmin, xmax] it does has to be resized on this example
    // derivation
    for( size_t j = 0; j != fourier_v_der.size( ); ++j )
    {
        fourier_v_der[ j ] *= particles::complex::one * solver.m_kk[ j ] * ( pars.xmax - pars.xmin ) / ( 2 * static_cast< type >( M_PI ) );
    }

    auto & fourier_v_integr = f_integr.m_fourierArr;

    // integration
    for( size_t j = 0; j != fourier_v_integr.size( ); ++j )
    {
        fourier_v_integr[ j ] /= particles::complex::one * solver.m_kk[ j ] * ( pars.xmax - pars.xmin ) / ( 2 * static_cast< type >( M_PI ) );
    }

    fourier_v_integr[ 0 ]                       = { };
    fourier_v_der[ 0 ]                          = { };
    fourier_v_integr[ pars.numberOfXCells / 2 ] = { };
    fourier_v_der[ pars.numberOfXCells / 2 ]    = { };

    f_der.runBackward( );
    f_integr.runBackward( );

    type diff{ 0 };
    for( size_t i = 0; i < cos_v.size( ); ++i )
    {
        diff = std::max( cos_v[ i ] - f_der.m_realArr[ i ], diff );
    }
    if( diff > 1e-2 )
    {
        std::cout << "der dif: " << diff << std::endl;
        throw std::runtime_error( std::string( "der diff: " ) + std::to_string( diff ) );
    }

    diff = 0;
    for( size_t i = 0; i < cos_v.size( ); ++i )
    {
        diff = std::max( minus_cos_v[ i ] - f_integr.m_realArr[ i ], diff );
    }
    if( diff > 1e-2 )
    {
        std::cout << "int dif: " << diff << std::endl;
        throw std::runtime_error( std::string( "integr diff: " ) + std::to_string( diff ) );
    }
}

void tests( )
{
    test_fft_derivative_odd_number_cells( );
    test_fft_derivative_even_number_cells( );
    test_mover( );
    test_particles_to_grid( );
}

struct indices_lin_koefs
{
    float a1;
    float a2;
    size_t i1;
    size_t i2;
};

indices_lin_koefs compute_indices_lin_koefs( double position, const size_t N )
{
    const size_t i1 = size_t( position );
    size_t i2       = i1 + 1;
    if( i2 >= N )
        i2 = 0;
    const float a2 = position - i1;
    const float a1 = 1. - a2;
    if( i1 >= N )
    {
        throw std::runtime_error( "i1 fucked" );
    }
    if( i2 >= N )
    {
        throw std::runtime_error( "i2 fucked" );
    }
    return { a1, a2, i1, i2 };
}

template < typename RealType >
bool cmp( RealType a, RealType b )
{
    //std::cout << a << ", " << b << std::endl;
    return std::abs( a - b ) < 1e-5;
}

template < int dim, typename type >
void test_generate_save_load( )
{
    using namespace particles;

    using MeshType = Mesh< dim, type >;

    Parameters< type > pars;

    pars.particlesPerCell = 1;
    pars.numberOfXCells   = 10;
    pars.numberOfYCells   = 10;
    pars.beta_e_parallel  = 1;
    pars.beta_p_parallel  = 1;
    pars.T_e_per_T_e_par  = 1;
    pars.T_p_per_T_p_par  = 1;

    pars.recompute_parameters( );
    MeshType m( pars );

    ParticleGenerator< dim, MeshType, type > generator( m, pars );

    auto particles = generator.generate( );

    HDFSaver< dim, MeshType, type > saver;
    saver.saveParticlesPositions( particles, 10 );
    saver.saveParticlesVelocityX( particles, 10 );

    pars.iteration_to_restart = 10;

    auto loaded_parts = generator.load( );

    const auto compare_particles = []( Particle< type > & a, Particle< type > & b ) {
        return cmp( a.vx( ), b.vx( ) ) &&
            cmp( a.vy( ), b.vy( ) ) &&
            cmp( a.vz( ), b.vz( ) ) &&
            cmp( a.x( ), b.x( ) ) &&
            cmp( a.y( ), b.y( ) ) &&
            cmp( a.z( ), b.z( ) );
    };

    for( size_t species_idx = 0; species_idx != particles.size( ); ++species_idx )
    {
        if( particles[ species_idx ].m_particles.size( ) != loaded_parts[ species_idx ].m_particles.size( ) )
        {
            throw std::runtime_error( "wrong number of particles" );
        }
        for( size_t i = 0; i != particles[ species_idx ].m_particles.size( ); ++i )
        {
            if( !compare_particles(
                    particles[ species_idx ].m_particles[ i ],
                    loaded_parts[ species_idx ].m_particles[ i ] ) )
            {
                throw std::runtime_error( "particles are not the same" );
            }
        }
    }
}

void test_saving_loading( )
{
    test_generate_save_load< 1, double >( );
    std::cout << "1D double ok" << std::endl;
    test_generate_save_load< 1, float >( );
    std::cout << "1D float ok" << std::endl;
    test_generate_save_load< 2, double >( );
    std::cout << "2D double ok" << std::endl;
    test_generate_save_load< 2, float >( );
    std::cout << "2D float ok" << std::endl;
}

int main( int argc, char const * argv[] )
{
    //test_saving_loading( );
    //return 0;

    using namespace std;
    using namespace particles;

#ifdef USE_FLOAT
    using type = float;
#else
    using type = double;
#endif

    JsonParser json;
    Parameters< type > pars;
    if( argc == 2 )
    {
        pars = json.parseParameters< type >( std::string( argv[ 1 ] ) );
    }

    constexpr int dim         = 1;
    using MeshType            = Mesh< dim, type >;
    using BoundaryConditions  = PeriodicBoundaryConditions< dim, MeshType, type >;
    using FromParticlestoGrid = FromParticlesToGridFirstOrder< dim, MeshType, type >;
    using Mover               = BorisMover< dim, MeshType, FromParticlestoGrid, BoundaryConditions, type >;
    using Solver              = FFTFieldSolver< dim, MeshType, BoundaryConditions,
                                   FromParticlestoGrid, Mover, type >;
    using ControllerType      = Controller< dim, MeshType, Mover, Solver, BoundaryConditions, FromParticlestoGrid, type >;

    ControllerType controller( pars );

    controller.run( );

    return 0;
}
