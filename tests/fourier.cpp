#include <iostream>
#include <complex>
#include "../src/Fourier.h"

int main(int argc, char *argv[]) {
    using namespace particles;
    int N = 20001;
    ArrayReal<double> testing(N);
    ArrayReal<double> reference(N);
    Fourier<double, std::complex<double>> f(testing);

    for (auto i = 0; i < testing.size(); i++) {
        testing[i] = reference[i] = 1.;
//        testing[i] = reference[i] = i%2;
    }
    f.runForward();
    f.runBackward();

    ArrayReal <double> diff(N);
    for (auto i =0; i < N; i++){
        diff[i]= pow((testing[i] - reference[i]),2);
    }
    double max = *std::max_element(diff.begin(), diff.end());

    double res = 0;
    std::for_each(diff.begin(), diff.end(), [&res](double &val){
        res+= val;
    });
    res /=N;

    std::cout << "max diff: " << max
              << "; sigma diff: " << res
              << std::endl;
    return static_cast<int>(!(max < 0.01 && res < 0.01 ));
}

