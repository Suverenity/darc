
#include "../src/HDFSaver.h"
#include <algorithm>
#include <complex>
#include <iostream>
#include <random>
#include <vector>

#include "../src/Fourier.h"
#include "../src/Types.h"

using namespace particles;

using real_t = float_or_double::type;

template < typename >
constexpr inline bool always_false = false;

//dx = 1
//B = (1,0,0)
constexpr size_t PPC   = 100;
constexpr size_t N     = 1024;
constexpr size_t X_MAX = N;
constexpr real_t QE    = -1;
constexpr real_t QP    = 1;
constexpr real_t ME    = 1;
constexpr real_t MP    = 1836;
constexpr real_t QM_E  = QE / ME;
constexpr real_t QM_P  = QP / MP;

constexpr size_t save_each_nth_iter = 10;

constexpr real_t beta_e_par        = 1;
constexpr real_t beta_p_par        = 1;
constexpr real_t omega_pe_omega_ce = 4;
constexpr real_t omega_ce_omega_pe = 1. / omega_pe_omega_ce;
constexpr real_t T_p_per_T_p_par   = 1;
constexpr real_t T_e_per_T_e_par   = 1.681;
//constexpr real_t T_e_per_T_e_par = 1.;

constexpr real_t FAC    = 1. / PPC;
constexpr real_t DT     = 0.01;
constexpr real_t DT_inv = 1. / DT;
constexpr real_t TM     = 10;
constexpr real_t DTV_E  = DT * QM_E;
constexpr real_t DTV_P  = DT * QM_P;
constexpr real_t DT_2   = DT / 2;
const real_t ALPHA      = std::sqrt( 0.5 * beta_e_par ) * 1. / omega_pe_omega_ce;
const real_t ALPHA_sq   = ALPHA * ALPHA;

constexpr size_t NP = N * PPC;
constexpr size_t NT = TM / DT;

Array< 1, real_t > dn_p( N );
Array< 1, real_t > dn_e( N );

Array< 1, real_t > x_e( NP );
Array< 1, real_t > x_p( NP );

Array< 1, real_t > vx_e( NP );
Array< 1, real_t > vy_e( NP );
Array< 1, real_t > vz_e( NP );
Array< 1, real_t > vx_p( NP );
Array< 1, real_t > vy_p( NP );
Array< 1, real_t > vz_p( NP );

Array< 1, real_t > Ex( N );
Array< 1, real_t > Ey( N );
Array< 1, real_t > Ez( N );

Array< 1, std::complex< real_t > > Ey_prev( N );
Array< 1, std::complex< real_t > > Ez_prev( N );

Array< 1, real_t > Bx( N, omega_ce_omega_pe );
Array< 1, real_t > By( N );
Array< 1, real_t > Bz( N );

Array< 1, real_t > jy( N );
Array< 1, real_t > jz( N );

Array< 1, real_t > ay( N );
Array< 1, real_t > az( N );

Array< 1, real_t > Mxy( N );
Array< 1, real_t > Mxz( N );

Array< 1, real_t > rho( N );

Fourier< 1, real_t > rho_ft_conv( rho );

Fourier< 1, real_t > ay_ft_conv( ay );
Fourier< 1, real_t > az_ft_conv( az );

Fourier< 1, real_t > Mxy_ft_conv( Mxy );
Fourier< 1, real_t > Mxz_ft_conv( Mxz );

Fourier< 1, real_t > jy_ft_conv( jy );
Fourier< 1, real_t > jz_ft_conv( jz );

Array< 1, real_t > kk( N );

HDFSaver< 1, int, real_t > saver;

std::default_random_engine generator;

struct indices_lin_koefs
{
    real_t a1;
    real_t a2;
    size_t i1;
    size_t i2;
};

indices_lin_koefs compute_indices_lin_koefs( real_t position )
{
    const size_t i1 = size_t( position );
    size_t i2       = i1 + 1;
    if( i2 >= N )
        i2 = 0;
    const real_t a2 = position - i1;
    const real_t a1 = 1. - a2;
    if( i1 >= N )
    {
        throw std::runtime_error( "i1 fucked" );
    }
    if( i2 >= N )
    {
        throw std::runtime_error( "i2 fucked" );
    }
    return { a1, a2, i1, i2 };
}

void initialization( )
{
    std::cout << "N:" << N << ",PPC:" << PPC << ",NP:" << NP << std::endl;

    std::uniform_real_distribution< real_t > uniform_distr( 0, 1 );
    for( auto & pos : x_e )
    {
        pos = uniform_distr( generator );
    }
    for( auto & pos : x_p )
    {
        pos = uniform_distr( generator );
    }

    std::normal_distribution< real_t > normal_distr( 0, 1 );
    for( auto & v : vx_e )
    {
        v = normal_distr( generator );
    }
    const auto anisotrophy_e = sqrt( T_e_per_T_e_par );
    for( auto & v : vy_e )
    {
        v = normal_distr( generator ) * anisotrophy_e;
    }
    for( auto & v : vz_e )
    {
        v = normal_distr( generator ) * anisotrophy_e;
    }

    const auto m_p_sq_inv    = 1. / sqrt( MP );
    const auto anisotrophy_p = sqrt( T_p_per_T_p_par );
    for( auto & v : vx_p )
    {
        v = normal_distr( generator ) * m_p_sq_inv;
    }
    for( auto & v : vy_p )
    {
        v = normal_distr( generator ) * m_p_sq_inv * anisotrophy_p;
    }
    for( auto & v : vz_p )
    {
        v = normal_distr( generator ) * m_p_sq_inv * anisotrophy_p;
    }

    for( size_t i = 0; i < x_e.size( ); ++i )
    {
        const auto a = size_t( i / PPC );
        auto xx      = x_e[ i ] + a;
        if( xx < 0 )
            xx = xx + X_MAX;
        if( xx > X_MAX )
            xx = xx - X_MAX;
        x_e[ i ] = xx;
        auto b   = compute_indices_lin_koefs( x_e[ i ] );
        dn_e[ b.i1 ] += b.a1;
        dn_e[ b.i2 ] += b.a2;
    }

    for( size_t i = 0; i < x_p.size( ); ++i )
    {
        const auto a = size_t( i / PPC );
        auto xx      = x_p[ i ] + a;
        if( xx < 0 )
            xx = xx + X_MAX;
        if( xx > X_MAX )
            xx = xx - X_MAX;
        x_p[ i ] = xx;
        auto b   = compute_indices_lin_koefs( x_p[ i ] );
        dn_p[ b.i1 ] += b.a1;
        dn_p[ b.i2 ] += b.a2;
    }
}

void init_ks( )
{
    std::iota( kk.begin( ), kk.end( ), 0 );

    for( auto & k : kk )
    {
        k *= real_t( 2 * M_PI ) / X_MAX;
    }
    for( size_t i = N / 2 + 1; i != kk.size( ); ++i )
    {
        kk[ i ] = -kk[ N - i ];
    }
    kk[ 0 ] = 1;
}

struct vec_t
{
    real_t x;
    real_t y;
    real_t z;
};

vec_t cross_product( const vec_t & Aa, const vec_t & Ab )
{
    return {
        Aa.y * Ab.z - Aa.z * Ab.y,
        Aa.z * Ab.x - Aa.x * Ab.z,
        Aa.x * Ab.y - Aa.y * Ab.x
    };
}

vec_t add( const vec_t & Aa, const vec_t & Ab )
{
    return {
        Aa.x + Ab.x,
        Aa.y + Ab.y,
        Aa.z + Ab.z,
    };
}

vec_t new_particle_velocity( real_t qm, real_t & position, real_t & vx, real_t & vy, real_t & vz )
{
    const auto b = compute_indices_lin_koefs( position );
    const vec_t Ei{
        b.a1 * Ex[ b.i1 ] + b.a2 * Ex[ b.i2 ],
        b.a1 * Ey[ b.i1 ] + b.a2 * Ey[ b.i2 ],
        b.a1 * Ez[ b.i1 ] + b.a2 * Ez[ b.i2 ]
    };

    const vec_t Bi{
        b.a1 * Bx[ b.i1 ] + b.a2 * Bx[ b.i2 ],
        b.a1 * By[ b.i1 ] + b.a2 * By[ b.i2 ],
        b.a1 * Bz[ b.i1 ] + b.a2 * Bz[ b.i2 ]
    };

    const vec_t t{
        qm * Bi.x * DT_2,
        qm * Bi.y * DT_2,
        qm * Bi.z * DT_2
    };

    const auto t_sq = t.x * t.x + t.y * t.y + t.z * t.z;

    const vec_t s{
        2 * t.x / ( 1 + t_sq ),
        2 * t.y / ( 1 + t_sq ),
        2 * t.z / ( 1 + t_sq )
    };

    const vec_t v_minus{
        vx + qm * Ei.x * DT_2,
        vy + qm * Ei.y * DT_2,
        vz + qm * Ei.z * DT_2
    };

    const vec_t v_primed = add( v_minus, cross_product( v_minus, t ) );
    const vec_t v_plus   = add( v_minus, cross_product( v_primed, s ) );

    return { v_plus.x + qm * Ei.x * DT_2,
             v_plus.y + qm * Ei.y * DT_2,
             v_plus.z + qm * Ei.z * DT_2 };
}

void compute_B_ft( )
{
    using namespace std::complex_literals;

    jy_ft_conv.runForward( );
    jz_ft_conv.runForward( );

    auto & fjy = jy_ft_conv.m_fourierArr;
    auto & fjz = jz_ft_conv.m_fourierArr;

    for( size_t i = 1; i < N; ++i )
    {
        const std::complex< real_t > fjy_new = -ALPHA_sq * particles::complex::one * fjz[ i ] / kk[ i ];
        const std::complex< real_t > fjz_new = ALPHA_sq * particles::complex::one * fjy[ i ] / kk[ i ];

        fjy[ i ] = fjy_new;
        fjz[ i ] = fjz_new;
    }
    fjy[ 0 ]     = 0i;
    fjy[ N / 2 ] = 0i;
    fjz[ 0 ]     = 0i;
    fjz[ N / 2 ] = 0i;

    jy_ft_conv.runBackward( );
    jz_ft_conv.runBackward( );

    std::copy( jy.begin( ), jy.end( ), By.begin( ) );
    std::copy( jz.begin( ), jz.end( ), Bz.begin( ) );
}

void compute_E_ft( )
{
    using namespace std::complex_literals;
    ay_ft_conv.runForward( );
    az_ft_conv.runForward( );

    Mxy_ft_conv.runForward( );
    Mxz_ft_conv.runForward( );

    auto & fay = ay_ft_conv.m_fourierArr;
    auto & faz = az_ft_conv.m_fourierArr;

    auto & fMxy = Mxy_ft_conv.m_fourierArr;
    auto & fMxz = Mxz_ft_conv.m_fourierArr;

    for( size_t i = 0; i < N; ++i )
    {
        const auto ftdjydt = fay[ i ] - particles::complex::one * kk[ i ] * fMxy[ i ];
        const auto ftdjzdt = faz[ i ] - particles::complex::one * kk[ i ] * fMxz[ i ];

        const auto koef = -ALPHA_sq / ( kk[ i ] * kk[ i ] + ALPHA_sq );

        fay[ i ]     = koef * ( ftdjydt - Ey_prev[ i ] );
        faz[ i ]     = koef * ( ftdjzdt - Ez_prev[ i ] );
        Ey_prev[ i ] = fay[ i ];
        Ez_prev[ i ] = faz[ i ];
    }

    fay[ 0 ]     = 0i;
    faz[ 0 ]     = 0i;
    fay[ N / 2 ] = 0i;
    faz[ N / 2 ] = 0i;

    ay_ft_conv.runBackward( );
    az_ft_conv.runBackward( );

    std::copy( ay.begin( ), ay.end( ), Ey.begin( ) );
    std::copy( az.begin( ), az.end( ), Ez.begin( ) );
}

void one_iteration( )
{
    using namespace std::complex_literals;
    for( size_t i = 0; i != rho.size( ); ++i )
    {
        rho[ i ] = ( QE * dn_e[ i ] + QP * dn_p[ i ] ) * FAC;
    }
    rho_ft_conv.runForward( );

    // use rho_ft for computing Ex
    {
        auto & rho_ft = rho_ft_conv.m_fourierArr;
        for( size_t j = 0; j != rho_ft.size( ); ++j )
        {
            rho_ft[ j ] = -particles::complex::one * rho_ft[ j ] / kk[ j ];
        }
        rho_ft[ 0 ]     = 0i;
        rho_ft[ N / 2 ] = 0i;
        rho_ft_conv.runBackward( );
        std::copy( rho.begin( ), rho.end( ), Ex.begin( ) );
    }

    // compute Ey, Ez, By Bz
    {
        std::fill( jy.begin( ), jy.end( ), 0 );
        std::fill( jz.begin( ), jz.end( ), 0 );
        std::fill( ay.begin( ), ay.end( ), 0 );
        std::fill( az.begin( ), az.end( ), 0 );
        std::fill( Mxy.begin( ), Mxy.end( ), 0 );
        std::fill( Mxz.begin( ), Mxz.end( ), 0 );

        for( size_t i = 0; i != NP; ++i )
        {
            auto b = compute_indices_lin_koefs( x_e[ i ] );
            jy[ b.i1 ] += b.a1 * QE * vy_e[ i ];
            jy[ b.i2 ] += b.a2 * QE * vy_e[ i ];
            jz[ b.i1 ] += b.a1 * QE * vz_e[ i ];
            jz[ b.i2 ] += b.a2 * QE * vz_e[ i ];
            Mxy[ b.i1 ] += b.a1 * QE * vy_e[ i ] * vx_e[ i ];
            Mxy[ b.i2 ] += b.a2 * QE * vy_e[ i ] * vx_e[ i ];
            Mxz[ b.i1 ] += b.a1 * QE * vz_e[ i ] * vx_e[ i ];
            Mxz[ b.i2 ] += b.a2 * QE * vz_e[ i ] * vx_e[ i ];
        }

        for( size_t i = 0; i != NP; ++i )
        {
            auto b = compute_indices_lin_koefs( x_p[ i ] );
            jy[ b.i1 ] += b.a1 * QP * vy_p[ i ];
            jy[ b.i2 ] += b.a2 * QP * vy_p[ i ];
            jz[ b.i1 ] += b.a1 * QP * vz_p[ i ];
            jz[ b.i2 ] += b.a2 * QP * vz_p[ i ];
            Mxy[ b.i1 ] += b.a1 * QE * vy_p[ i ] * vx_p[ i ];
            Mxy[ b.i2 ] += b.a2 * QE * vy_p[ i ] * vx_p[ i ];
            Mxz[ b.i1 ] += b.a1 * QE * vz_p[ i ] * vx_p[ i ];
            Mxz[ b.i2 ] += b.a2 * QE * vz_p[ i ] * vx_p[ i ];
        }

        for( size_t i = 0; i < N; ++i )
        {
            jy[ i ] *= FAC;
            jz[ i ] *= FAC;
            Mxy[ i ] *= FAC;
            Mxz[ i ] *= FAC;
        }

        std::fill( Ey_prev.begin( ), Ey_prev.end( ), 0i );
        std::fill( Ez_prev.begin( ), Ez_prev.end( ), 0i );

        compute_B_ft( );
        compute_E_ft( );

        for( size_t i = 0; i < 3; ++i )
        {
            std::fill( jy.begin( ), jy.end( ), 0 );
            std::fill( jz.begin( ), jz.end( ), 0 );
            std::fill( ay.begin( ), ay.end( ), 0 );
            std::fill( az.begin( ), az.end( ), 0 );
            std::fill( Mxy.begin( ), Mxy.end( ), 0 );
            std::fill( Mxz.begin( ), Mxz.end( ), 0 );

            for( size_t i = 0; i < NP; ++i )
            {
                const auto v_new = new_particle_velocity(
                    QM_E, x_e[ i ], vx_e[ i ], vy_e[ i ], vz_e[ i ] );
                const auto dvdty = ( v_new.y - vy_e[ i ] ) * DT_inv;
                const auto dvdtz = ( v_new.z - vz_e[ i ] ) * DT_inv;

                const auto vx = ( v_new.x + vx_e[ i ] ) * 0.5;
                const auto vy = ( v_new.y + vy_e[ i ] ) * 0.5;
                const auto vz = ( v_new.z + vz_e[ i ] ) * 0.5;
                auto b        = compute_indices_lin_koefs( x_e[ i ] );
                ay[ b.i1 ] += b.a1 * QE * dvdty;
                ay[ b.i2 ] += b.a2 * QE * dvdty;
                az[ b.i1 ] += b.a1 * QE * dvdtz;
                az[ b.i2 ] += b.a2 * QE * dvdtz;

                jy[ b.i1 ] += b.a1 * QE * vy;
                jy[ b.i2 ] += b.a2 * QE * vy;
                jz[ b.i1 ] += b.a1 * QE * vz;
                jz[ b.i2 ] += b.a2 * QE * vz;

                Mxy[ b.i1 ] += b.a1 * QE * vy * vx;
                Mxy[ b.i2 ] += b.a2 * QE * vy * vx;
                Mxz[ b.i1 ] += b.a1 * QE * vz * vx;
                Mxz[ b.i2 ] += b.a2 * QE * vz * vx;
            }

            for( size_t i = 0; i < NP; ++i )
            {
                const auto v_new = new_particle_velocity(
                    QM_P, x_p[ i ], vx_p[ i ], vy_p[ i ], vz_p[ i ] );
                const auto dvdty = ( v_new.y - vy_p[ i ] ) * DT_inv;
                const auto dvdtz = ( v_new.z - vz_p[ i ] ) * DT_inv;

                const auto vx = ( v_new.x + vx_p[ i ] ) * 0.5;
                const auto vy = ( v_new.y + vy_p[ i ] ) * 0.5;
                const auto vz = ( v_new.z + vz_p[ i ] ) * 0.5;

                auto b = compute_indices_lin_koefs( x_p[ i ] );
                ay[ b.i1 ] += b.a1 * QP * dvdty;
                ay[ b.i2 ] += b.a2 * QP * dvdty;
                az[ b.i1 ] += b.a1 * QP * dvdtz;
                az[ b.i2 ] += b.a2 * QP * dvdtz;

                jy[ b.i1 ] += b.a1 * QP * vy;
                jy[ b.i2 ] += b.a2 * QP * vy;
                jz[ b.i1 ] += b.a1 * QP * vz;
                jz[ b.i2 ] += b.a2 * QP * vz;

                Mxy[ b.i1 ] += b.a1 * QP * vy * vx;
                Mxy[ b.i2 ] += b.a2 * QP * vy * vx;
                Mxz[ b.i1 ] += b.a1 * QP * vz * vx;
                Mxz[ b.i2 ] += b.a2 * QP * vz * vx;
            }

            for( size_t i = 0; i < N; ++i )
            {
                jy[ i ] *= FAC;
                jz[ i ] *= FAC;
                ay[ i ] *= FAC;
                az[ i ] *= FAC;
                Mxy[ i ] *= FAC;
                Mxz[ i ] *= FAC;
            }
            compute_E_ft( );
            compute_B_ft( );
        }
    }

    std::fill( dn_e.begin( ), dn_e.end( ), 0 );
    std::fill( dn_p.begin( ), dn_p.end( ), 0 );

    // electrons
    for( size_t i = 0; i != NP; i++ )
    {
        const auto new_v = new_particle_velocity(
            QM_E, x_e[ i ], vx_e[ i ], vy_e[ i ], vz_e[ i ] );
        vx_e[ i ] = new_v.x;
        vy_e[ i ] = new_v.y;
        vz_e[ i ] = new_v.z;

        x_e[ i ] += vx_e[ i ] * DT;
        if( x_e[ i ] < 0 )
            x_e[ i ] += X_MAX;
        if( x_e[ i ] > X_MAX )
            x_e[ i ] -= X_MAX;
        auto b = compute_indices_lin_koefs( x_e[ i ] );
        dn_e[ b.i1 ] += b.a1;
        dn_e[ b.i2 ] += b.a2;
    }

    //protons
    for( size_t i = 0; i != NP; i++ )
    {
        const auto new_v = new_particle_velocity(
            QM_P, x_p[ i ], vx_p[ i ], vy_p[ i ], vz_p[ i ] );
        vx_p[ i ] = new_v.x;
        vy_p[ i ] = new_v.y;
        vz_p[ i ] = new_v.z;
        x_p[ i ] += vx_p[ i ] * DT;
        if( x_p[ i ] < 0 )
            x_p[ i ] += X_MAX;
        if( x_p[ i ] > X_MAX )
            x_p[ i ] -= X_MAX;
        auto b = compute_indices_lin_koefs( x_p[ i ] );
        dn_p[ b.i1 ] += b.a1;
        dn_p[ b.i2 ] += b.a2;
    }
}

int main( )
{
    initialization( );
    init_ks( );

    std::cout << "number of iterations: " << NT << std::endl;
    for( size_t i = 0; i != NT; ++i )
    {
        one_iteration( );
        if( i % save_each_nth_iter == 0 )
        {
            saver.saveGridQuantity( Ex, "Ex", i );
            saver.saveGridQuantity( Ey, "Ey", i );
            saver.saveGridQuantity( Ez, "Ez", i );

            saver.saveGridQuantity( Bx, "Bx", i );
            saver.saveGridQuantity( By, "By", i );
            saver.saveGridQuantity( Bz, "Bz", i );
        }
        std::cout << "iteration: " << i << "/" << NT << std::endl;
    }
}
