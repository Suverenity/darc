#include <iostream>
#include <numeric>
#include <vector>

#include "../src/Mesh.h"
#include "../src/Mpi.h"
#include "../src/Types.h"

#include "../src/fftw_mpi_initializator.h"

void simple_mpi_test( )
{
    std::cout << "version: " << MPI_VERSION << std::endl;
    using type      = double;
    using arr_t     = std::vector< type >;
    const auto size = 100;
    arr_t arr_per_rpocess( size, -1. );

    auto & instance = particles::mpi::MPI_handler< arr_t >::get_instance( );
    instance.buffer = arr_t( 100 );
    std::fill( instance.buffer.begin( ), instance.buffer.end( ), 1 );

    particles::mpi::all_reduce_sum( arr_per_rpocess );

    std::cout << "proc id: " << particles::mpi::current_process_id< arr_t >( );

    int number_processes = particles::mpi::number_processes< arr_t >( );

    const auto sum = std::accumulate( arr_per_rpocess.begin( ), arr_per_rpocess.end( ), 0 );

    if( sum != -type( number_processes ) * size )
    {
        throw std::runtime_error( std::string( "wrong sum: " ) + std::to_string( arr_per_rpocess[ 0 ] ) );
    }
    std::cout << "sum is: " << sum << ", for processes: " << number_processes << std::endl;
}

void fftw_mpi_test( )
{
    using namespace particles;

    using type        = float_or_double::type;
    constexpr int dim = 2;

    using mesh_t = Mesh< dim, type >;

    mesh_t mesh( 0, 10, 15, 30, 100, 200 );
    fftw_mpi_initializator< dim, type, mesh_t > init( mesh );
}

int main( )
{
}
