import argparse
import h5py
import os
from matplotlib import pyplot as plt
import numpy as np

parser = argparse.ArgumentParser(description='Draw all h5 files.')
parser.add_argument('--path', default='./',
                    help="Path to directory with files.")
parser.add_argument('--prefix', help="Prefix of filese to draw, eg. rho.")
parser.add_argument('--xmin', default='0')
parser.add_argument('--xmax', default='1')
parser.add_argument('--ymin', default='0')
parser.add_argument('--ymax', default='1')
parser.add_argument('--nolims', type=int, default=1)

args = parser.parse_args()

print(args)
# print(os.listdir(args.path))

for file in os.listdir(args.path):
    filename = file.split('/')[-1]
    if args.prefix in filename:
        if not filename[-3:] == ".h5":
            continue

        print('drawing: fft_' + filename)
        with h5py.File(filename, 'r') as f:
            array = f["dset"][()]
        fft_array = np.fft.fft(array)
        plt.plot(fft_array)

        # plt.title("Profil elektrického pole v čase $t= 15\, \omega_{pe}^{-1}$")
        # plt.xlabel(r"$x/\lambda_D$")
        # plt.ylabel(r"$n$")

        # if not bool(args.nolims):
        #     plt.xlim(float(args.xmin), float(args.xmax))
        #     plt.ylim(float(args.ymin), float(args.ymax))
        fname_bits = filename.split('.')
        del fname_bits[-1]
        plt.savefig('fft_' + ''.join(fname_bits))
        plt.close()
