import os
import h5py
import argparse
from matplotlib import pyplot as plt
import numpy as np
import math
import scipy.stats as sc
from matplotlib.colors import LogNorm
import matplotlib

parser = argparse.ArgumentParser(
    description='Combine theoreticla growth with growth from simulation')
parser.add_argument('--Kvect_theo_filename', type=str)
parser.add_argument('--Gamma_theo_filename', type=str)
parser.add_argument('--Kvect_sim_filename', type=str)
parser.add_argument('--Gamma_sim_filename', type=str)

args = parser.parse_args()


if __name__ == "__main__":
    print(args)

    sim_k = np.genfromtxt(args.Kvect_sim_filename)
    theo_k = np.genfromtxt(args.Kvect_theo_filename)

    sim_gamma = np.genfromtxt(args.Gamma_sim_filename)
    theo_gamma = np.genfromtxt(args.Gamma_theo_filename)

    plt.plot(theo_k, theo_gamma)
    plt.plot(sim_k, sim_gamma, 'ro')
    plt.xlabel(r"$k \lambda_D$")
    plt.ylabel(r"$\gamma/|\omega_{pe}|$")
    plt.title('Whistler dispersion')
    plt.grid(True)

    plt.show()

