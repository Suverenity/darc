import argparse
import h5py
import os
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import multiprocessing

parser = argparse.ArgumentParser(description='Draw all h5 files.')
# parser.add_argument('--prefix', help="Prefix of filese to draw, eg. rho.")
parser.add_argument('--path')
parser.add_argument('--xmin', default='0')
parser.add_argument('--xmax', default='1')
parser.add_argument('--ymin', default='0')
parser.add_argument('--ymax', default='1')
parser.add_argument('--xaxisfile')
parser.add_argument('--yaxisfile')
parser.add_argument('-xaf')
parser.add_argument('-yaf')
parser.add_argument('--output')
parser.add_argument('-o')
parser.add_argument('--xbins', default='100', type=int)
parser.add_argument('--ybins', default='100', type=int)
parser.add_argument('--xmask', default='el_vx')
parser.add_argument('--ymask', default='el_vy')
parser.add_argument('--cpus', type=int, default=1)

args = parser.parse_args()


def draw_hdf(xaxisfile, yaxisfile, output_filename):
    if not xaxisfile.endswith(".h5"):
        print(xaxisfile, xaxisfile[-3:])
        return
    if not yaxisfile.endswith(".h5"):
        print(yaxisfile, yaxisfile[-3:])
        return

    with h5py.File(xaxisfile, 'r') as f:
        x = f["dset"][()]

    with h5py.File(yaxisfile, 'r') as f:
        y = f["dset"][()]

    xmin = min((float(args.xmin), min(x)))
    ymin = min((float(args.ymin), min(y)))

    # xmin = args.xmin
    # xmax = args.xmax

    xmax = max((float(args.xmax), max(x)))
    ymax = max((float(args.ymax), max(y)))

    # ymin = args.ymin
    # ymax = args.ymax

    # print('xmin: ', xmin, ', xmax: ', xmax, ', ymin: ', ymin, ', ymax: ', ymax)

    H, _, _ = np.histogram2d(x, y, bins=(args.xbins, args.ybins),
                             range=[[xmin, xmax], [ymin, ymax]])
    # print(H)

    plt.imshow(H, origin='low', extent=[
               xmin, xmax, ymin, ymax], aspect='equal')

    if output_filename is None:
        plt.show()
    else:
        plt.savefig(output_filename)
    plt.close()
    plt.clf


def draw_hdf_files(xfiles, yfiles, outputfiles):
    # print(xfiles, yfiles, outputfiles)
    for i in range(len(xfiles)):
        xfile = xfiles[i]
        yfile = yfiles[i]
        output_file = outputfiles[i]
        print('creating ', i, len(xfiles), xfile, yfile, output_file)
        draw_hdf(xfile, yfile, output_file)


def run_draw_hdf_parallel(xfiles, yfiles, outputfiles):
    # print(xfiles)
    # print(yfiles)
    # print(outputfiles)
    cpu_count = args.cpus

    if cpu_count == 1:
        draw_hdf_files(xfiles, yfiles, outputfiles)
        return

    files_per_cpu = int(len(xfiles) / cpu_count)
    processes = []

    for i in range(cpu_count):
        bottom = i*files_per_cpu
        if i == cpu_count-1:
            upper = len(xfiles)-1
        else:
            upper = (i+1)*files_per_cpu

        print(bottom, upper)
        p = multiprocessing.Process(
            target=draw_hdf_files,
            args=(xfiles[bottom: upper], yfiles[bottom: upper], outputfiles[bottom: upper],))
        p.start()
        processes.append(p)

    for p in processes:
        p.join()


if __name__ == "__main__":
    if args.path is None:
        raise Exception('wtf?')
        # xaxisfile = args.xaxisfile
        # if xaxisfile is None:
        #     xaxisfile = args.xaf
        # xaxisfile = xaxisfile.split('/')[-1]

        # yaxisfile = args.yaxisfile
        # if yaxisfile is None:
        #     yaxisfile = args.yaf
        # yaxisfile = yaxisfile.split('/')[-1]

        # output_filename = args.output
        # if output_filename is None:
        #     output_filename = args.o
        # draw_hdf(xaxisfile, yaxisfile, output_filename)
    else:
        xmask = args.xmask
        ymask = args.ymask
        xfiles = []
        yfiles = []
        for file in os.listdir(args.path):
            if xmask in file and file.endswith(".h5"):
                xfiles.append(file)
            if ymask in file and file.endswith(".h5"):
                yfiles.append(file)

        xfiles.sort()
        yfiles.sort()
        output_filenames = []
        print(xmask)
        print(xfiles)

        output_filename = xmask + '_' + ymask
        for i in range(len(xfiles)):
            fname_bits = xfiles[i].split('.')
            del fname_bits[-1]
            string_with_numbers = ''.join(fname_bits)
            number = ''.join(c for c in string_with_numbers if c.isdigit())
            output_filenames.append(output_filename + number)

        run_draw_hdf_parallel(xfiles, yfiles, output_filenames)
