from matplotlib import pyplot as plt
import numpy as np
import os
import h5py
import argparse

import math

from matplotlib.colors import LogNorm

# import matplotlib
# matplotlib.use('Agg')

parser = argparse.ArgumentParser(description='Draw all h5 files.')
parser.add_argument('--path', default='./',
                    help="Path to directory with files.")
parser.add_argument('--prefix', help="Prefix of files to draw, eg. rho.")
parser.add_argument('--dims', type=str, help='example: x or x,y,z')
parser.add_argument('--xmin', type=int)
parser.add_argument('--xmax', type=int)
parser.add_argument('--ymin', type=int)
parser.add_argument('--ymax', type=int)
parser.add_argument('--tmax', type=int)

args = parser.parse_args()


def cmp(a, b):
    return float(a) < float(b)


def compose(prefix):
    files = []
    for file in os.listdir(args.path):
        filename = file.split('/')[-1]
        if prefix in filename:
            if not filename[-3:] == ".h5":
                continue
            files.append(filename)
            # print('drawing: ' + filename)
            # with h5py.File(filename, 'r') as f:
            #     array = f["dset"][()]
    if len(files) == 0:
        print('no files found')
        return

    files.sort()
    # print(files)

    composite = []
    for i in range(len(files)):
        # if i is 0:
        #     continue

        filename = files[i]
        print('drawing: ' + filename)
        with h5py.File(filename, 'r') as f:
            array = f["dset"][()]
            composite.append(array)
            # np.vstack((composite, array))
            # print(composite.shape)

    return np.array(composite)


def draw_image(arr_to_draw=[], k_lims=(), omega_lims=(), name_specification=''):
    """draw 2d image!!"""

    plt.imshow(arr_to_draw, cmap='Blues', norm=LogNorm(), interpolation='bicubic',
               aspect=(k_lims[1]-k_lims[0])/(omega_lims[1] - omega_lims[0]),
               extent=[k_lims[0], k_lims[-1], omega_lims[0], omega_lims[1]]
               )
    plt.xlabel('k')
    plt.ylabel('omega')
    plt.colorbar()
    # plt.show()
    name = args.prefix + args.dims.replace(',', '') + '_' + name_specification
    print('saving: ', name)
    # plt.show()
    plt.savefig(name + '_powerspectrum')
    np.savetxt(name+'powerspectrum.txt', arr_to_draw)
    plt.close()


def create_image(arrays_to_fft):
    ffts = []

    for arr in arrays_to_fft:
        # arr = arr.transpose()

        fft = np.fft.fftshift(np.fft.fftn(arr))
        fft = np.absolute(fft)**2
        ffts.append(fft)

    summed = ffts[0]
    for i in range(1, len(ffts)):
        summed += ffts[i]

    # plt.imshow(summed, cmap='Blues', vmin=1e-10, norm=LogNorm(),
    #           aspect=summed.shape[1]/summed.shape[0])
    #summed += 1e-10

    # for drawing logarithm is used
    summed += 1

    time_size = math.pi * summed.shape[0] / args.tmax

    if len(summed.shape) == 2:
        space_size = math.pi * summed.shape[1] / \
            (float(args.xmax)-float(args.xmin))
        draw_image(summed, (-space_size, space_size), (-time_size, time_size))

    if len(summed.shape) == 3:
        np.save(args.prefix + args.dims.replace(',', '') +
                '_2d', summed)
        x_lim = math.pi * summed.shape[1] / \
            (float(args.xmax)-float(args.xmin))
        draw_image(summed[:, :, int(summed.shape[2]/2)],
                   (-x_lim, x_lim), (-time_size, time_size), 'xcut')

        y_lim = math.pi * summed.shape[2] / \
            (float(args.ymax)-float(args.ymin))
        draw_image(summed[:, int(summed.shape[1]/2), :],
                   (-y_lim, y_lim), (-time_size, time_size), 'ycut')


if __name__ == "__main__":

    dims = args.dims.split(',')

    dims_prefix = [args.prefix + dim for dim in dims]

    arrs = []

    print(dims_prefix)
    for dim_prefix in dims_prefix:
        arrs.append(compose(dim_prefix))

    create_image(arrs)
