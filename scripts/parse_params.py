import math
import json


def from_json(json_file_name):
    with open(json_file_name) as f:
        params = json.load(f)

    omega_ce_omega_pe = 1 / params['omega_pe_omega_ce']

    degree_to_rads = math.pi / 180

    cos_polar = math.cos(degree_to_rads * params['polar_angle'])
    sin_polar = math.sin(degree_to_rads * params['polar_angle'])
    cos_azimuth = math.cos(degree_to_rads * params['azimuthal_angle'])
    sin_azimuth = math.sin(degree_to_rads * params['azimuthal_angle'])

    params['n0'] = 1
    params['B'] = [omega_ce_omega_pe*cos_polar * sin_azimuth,
                   omega_ce_omega_pe*sin_polar * sin_azimuth,
                   omega_ce_omega_pe*cos_azimuth]
    params['alpha'] = math.sqrt(
        params['beta_e_parallel']/2) * omega_ce_omega_pe
    params['mu'] = params['alpha'] ** 2
    params['eps'] = 1

    print(params)
    return params
