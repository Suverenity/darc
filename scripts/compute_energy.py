from matplotlib import pyplot as plt
import numpy as np
import os
import h5py
import argparse
from mpmath import *
import re

import parse_params

import math

from matplotlib.colors import LogNorm

parser = argparse.ArgumentParser(
    description='Compute energy from E*, B* and pressure h5 files.')
parser.add_argument('--params', type=str)
parser.add_argument('--maxiteration', type=int, default=999999999)

args = parser.parse_args()


def check_iteration(filename):
    m = re.search(r'\d+', filename)
    return int(m[0]) <= args.maxiteration


def compute_magnetic_eg(mu, xsize, ysize, B):
    Bxs_fnames = []
    Bys_fnames = []
    Bzs_fnames = []
    for file in os.listdir('.'):
        filename = file.split('/')[-1]
        if not filename[-3:] == ".h5":
            continue

        if 'Bx' in filename and check_iteration(filename):
            Bxs_fnames.append(file)

        if 'By' in filename and check_iteration(filename):
            Bys_fnames.append(file)

        if 'Bz' in filename and check_iteration(filename):
            Bzs_fnames.append(file)

    Bxs_fnames.sort()
    Bys_fnames.sort()
    Bzs_fnames.sort()

    if not(len(Bxs_fnames) == len(Bys_fnames) == len(Bzs_fnames)):
        raise Exception('magnetic files not the same size')

    magnetic_energy = np.zeros(len(Bxs_fnames))

    with h5py.File(Bxs_fnames[0], 'r') as f:
        dims = len(np.array(f["dset"][()]).shape)

    normalization = xsize
    if dims == 2:
        normalization *= ysize

    for i in range(0, len(Bxs_fnames)):
        with h5py.File(Bxs_fnames[i], 'r') as f:
            Bx = np.array(f["dset"][()])
        with h5py.File(Bys_fnames[i], 'r') as f:
            By = np.array(f["dset"][()])
        with h5py.File(Bzs_fnames[i], 'r') as f:
            Bz = np.array(f["dset"][()])

        magnetic_energy[i] = (np.sum(
            (Bx-B[0])**2 + (By-B[1])**2 + (Bz - B[2])**2)) * 0.5 / (mu*normalization)

    init_eg = magnetic_energy[0]
    return magnetic_energy - init_eg


def compute_electric_eg(eps, xsize, ysize):
    Exs_fnames = []
    Eys_fnames = []
    Ezs_fnames = []
    for file in os.listdir('.'):
        filename = file.split('/')[-1]
        if not filename[-3:] == ".h5":
            continue

        if 'Ex' in filename and check_iteration(filename):
            Exs_fnames.append(file)

        if 'Ey' in filename and check_iteration(filename):
            Eys_fnames.append(file)

        if 'Ez' in filename and check_iteration(filename):
            Ezs_fnames.append(file)

    Exs_fnames.sort()
    Eys_fnames.sort()
    Ezs_fnames.sort()

    if not(len(Exs_fnames) == len(Eys_fnames) == len(Ezs_fnames)):
        print(len(Exs_fnames), len(Eys_fnames), len(Ezs_fnames))
        raise Exception('Electric files not the same size')

    electric_energy = np.zeros(len(Exs_fnames))

    with h5py.File(Exs_fnames[0], 'r') as f:
        dims = len(np.array(f["dset"][()]).shape)

    normalization = xsize
    if dims == 2:
        normalization *= ysize

    for i in range(0, len(Exs_fnames)):
        with h5py.File(Exs_fnames[i], 'r') as f:
            Ex = np.array(f["dset"][()])
        with h5py.File(Eys_fnames[i], 'r') as f:
            Ey = np.array(f["dset"][()])
        with h5py.File(Ezs_fnames[i], 'r') as f:
            Ez = np.array(f["dset"][()])

        electric_energy[i] = (np.sum(
            (Ex)**2 + (Ey)**2 + (Ez)**2)) * 0.5 * eps / (normalization)

    init_eg = electric_energy[0]

    return electric_energy-init_eg


def compute_kinetic_eg(ppc, xsize, ysize):
    ppar_el_fnames = []
    pper_el_fnames = []
    ppar_pt_fnames = []
    pper_pt_fnames = []
    for file in os.listdir('.'):
        filename = file.split('/')[-1]
        if not filename[-3:] == ".h5":
            continue

        if 'ppar' in filename:
            if 'el' in filename and check_iteration(filename):
                ppar_el_fnames.append(file)
            if 'pt' in filename and check_iteration(filename):
                ppar_pt_fnames.append(file)

        if 'pper' in filename:
            if 'el' in filename and check_iteration(filename):
                pper_el_fnames.append(file)
            if 'pt' in filename and check_iteration(filename):
                pper_pt_fnames.append(file)

    ppar_el_fnames.sort()
    pper_el_fnames.sort()
    ppar_pt_fnames.sort()
    pper_pt_fnames.sort()

    if not(len(ppar_el_fnames) == len(pper_el_fnames) == len(ppar_pt_fnames) == len(pper_pt_fnames)):
        print(len(ppar_el_fnames), len(ppar_pt_fnames),
              len(pper_el_fnames), len(pper_pt_fnames))
        raise Exception('pressure files not the same size')

    kinetic_el_energy = np.zeros(len(ppar_el_fnames))
    kinetic_pt_energy = np.zeros(len(ppar_el_fnames))

    for i in range(0, len(ppar_el_fnames)):
        with h5py.File(ppar_el_fnames[i], 'r') as f:
            array = f["dset"][()]
            kinetic_el_energy[i] += np.sum(array)

        with h5py.File(ppar_pt_fnames[i], 'r') as f:
            array = f["dset"][()]
            kinetic_pt_energy[i] += np.sum(array)

        with h5py.File(pper_el_fnames[i], 'r') as f:
            array = f["dset"][()]
            kinetic_el_energy[i] += 2 * np.sum(array)

        with h5py.File(pper_pt_fnames[i], 'r') as f:
            array = f["dset"][()]
            dims = len(array.shape)
            kinetic_pt_energy[i] += 2 * np.sum(array)

    normalization = xsize
    if dims == 2:
        normalization *= ysize
    init_el = kinetic_el_energy[0]
    init_pt = kinetic_pt_energy[0]
    return (0.5/(ppc*normalization)*(kinetic_el_energy-init_el), 0.5/(ppc*normalization)*(kinetic_pt_energy-init_pt))


if __name__ == "__main__":
    params = parse_params.from_json(args.params)

    electric_eg = compute_electric_eg(
        params['eps'], params['numberOfXCells'], params['numberOfYCells'])
    (kinetic_el_eg, kinetic_pt_eg) = compute_kinetic_eg(
        params['particlesPerCell'], params['numberOfXCells'], params['numberOfYCells'])
    magnetic_eg = compute_magnetic_eg(
        params['mu'], params['numberOfXCells'], params['numberOfYCells'], params['B'])

    # if not(len(electric_eg) == len(magnetic_eg) == len(kinetic_eg)):
    #    print('electric_eg:', len(electric_eg), 'magnetic_eg:',
    #          len(magnetic_eg), 'kinetic_eg:', len(kinetic_eg))
    #    raise Exception('energies not having same length')

    plt.plot(electric_eg, label='electric')
    plt.savefig('electric_eg')
    plt.close()

    plt.plot(magnetic_eg, label='magnetic')
    plt.savefig('magnetic_eg')
    plt.close()

    plt.plot(kinetic_pt_eg, label='kinetic_pt')
    plt.savefig('kinetic_eg_pt')
    plt.close()

    plt.plot(kinetic_el_eg, label='kinetic_el')
    plt.savefig('kinetic_eg_el')
    plt.close()

    energy = electric_eg + magnetic_eg + kinetic_el_eg + kinetic_pt_eg
    plt.plot(energy, label='energy')
    plt.savefig('energy')
    plt.close()

    plt.plot(electric_eg, 'y-', label='electric')
    plt.plot(magnetic_eg, 'b-', label='magnetic')
    plt.plot(kinetic_el_eg,  'r-', label='kinetic_el')
    plt.plot(kinetic_pt_eg, 'g-', label='kinetic_pt')
    plt.plot(energy, '-k', label='energy')
    plt.legend()
    # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
    #           ncol=2, mode="expand", borderaxespad=0.)

    plt.xlabel(r"$t \omega_{pe}^{-1}$")
    plt.title('Energy change')
    plt.grid(True)
    plt.savefig('energychange')

    np.savetxt('electric_energy.txt', electric_eg)
    np.savetxt('magnetic_energy.txt', magnetic_eg)
    np.savetxt('kinetic_el_energy.txt', kinetic_el_eg)
    np.savetxt('kinetic_pt_energy.txt', kinetic_pt_eg)
    np.savetxt('energychange.txt', energy)
