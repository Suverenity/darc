import argparse

import h5py

from matplotlib import pyplot as plt
import numpy as np

parser = argparse.ArgumentParser(description='Draw all h5 files.')
parser.add_argument('--path', default='./',
                    help="Path to directory with files.")
parser.add_argument('--prefix', help="Prefix of filese to draw, eg. rho.")

parser.add_argument('--file1', type=str)
parser.add_argument('--file2', type=str)

args = parser.parse_args()

with h5py.File(args.file1) as f:
    array1 = f["dset"][()]

with h5py.File(args.file2) as f:
    array2 = f["dset"][()]

for v in array1:
    print(v)


MIN = 0
MAX = 1024
BINS_SIZE = 1

bins = np.arange(MIN, MAX, BINS_SIZE)

h = plt.hist2d(array1, array2, bins=(100, 100))

print(h)

plt.show()
