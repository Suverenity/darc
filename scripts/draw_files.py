import multiprocessing
import threading
from matplotlib import pyplot as plt
import argparse
import h5py
import os
import math

import matplotlib
matplotlib.use('Agg')


parser = argparse.ArgumentParser(description='Draw all h5 files.')
parser.add_argument('--path', default='./',
                    help="Path to directory with files.")
parser.add_argument('--prefix', help="Prefix of filese to draw, eg. rho.")
parser.add_argument('--xmin', default='0')
parser.add_argument('--xmax', default='1')
parser.add_argument('--ymin', default='0')
parser.add_argument('--ymax', default='1')
parser.add_argument('--nolims', type=int, default=1)
parser.add_argument('--cpus', type=int, default=1)
parser.add_argument('--dim', type=int, default=1)
parser.add_argument('--lower_bound', type=float, default=0)
parser.add_argument('--upper_bound', type=float, default=0)

args = parser.parse_args()

print(args)
# print(os.listdir(args.path))

# for file in os.listdir(args.path):


def plot_files(files):
    nth_file = 0
    # for file in files:
    for filename in files:
        nth_file += 1
        # filename = file.split('/')[-1]
        # if args.prefix in filename:
        #     if not filename[-3:] == ".h5":
        #         continue

        print('drawing: ' + filename, nth_file, len(files))
        with h5py.File(filename, 'r') as f:
            array = f["dset"][()]
        if args.dim == 1:
            if args.lower_bound != args.upper_bound:
                plt.ylim(args.lower_bound, args.upper_bound)
            plt.plot(array)

        elif args.dim == 2:
            if args.lower_bound != args.upper_bound:
                plt.imshow(array, interpolation='bilinear', origin='lower',
                           vmin=args.lower_bound, vmax=args.upper_bound)
            else:
                plt.imshow(array, interpolation='bilinear', origin='lower')
            plt.colorbar()

        plt.xlabel(r"$x$")
        plt.ylabel(r"$A$")

        # plt.ylim(-1, 1)
        # plt.xlim(-100,100)
        if not bool(args.nolims):
            plt.xlim(float(args.xmin), float(args.xmax))
            plt.ylim(float(args.ymin), float(args.ymax))
        fname_bits = filename.split('.')
        del fname_bits[-1]
        plt.savefig(''.join(fname_bits))
        plt.close('all')
        plt.clf()


def fun_test(files):
    pass
    # for file in files:
    # print(file)


def filter_files(files):
    res = []
    for file in files:
        filename = file.split('/')[-1]
        if args.prefix in filename:
            if not filename[-3:] == ".h5":
                continue
            res.append(filename)
    return res


if __name__ == "__main__":
    files = os.listdir(args.path)
    files = filter_files(files)
    print(files)
    if len(files) == 1:
        plot_files(files)

    else:
        threads = []

        cpu_count = args.cpus

        number = int(len(files) / cpu_count)
        for i in range(cpu_count):
            bottom = i*number
            if i == cpu_count-1:
                upper = len(files)-1
            else:
                upper = i*number+number

            print(bottom, upper, upper - bottom)
            # t = multiprocessing.Process(
            #     target=fun_test, args=([files[bottom:upper]],))
            t = multiprocessing.Process(
                target=plot_files, args=(files[bottom:upper],))
            t.start()
            threads.append(t)

        for t in threads:
            t.join()
