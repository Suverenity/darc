from matplotlib import pyplot as plt
import sys
import csv
import matplotlib
matplotlib.use('Agg')

with open(sys.argv[1], 'r') as csvfile:
    kinetic_eg = []
    electric_eg = []
    magnetic_eg = []
    potential_eg = []
    total_eg = []
    reader = csv.reader(csvfile, delimiter=',')
    i = 0
    title = []
    for row in reader:
        if i == 0:
            print(row)
            title = row
        if i > 0:
            #file << k_eg << "," << e_eg << ","
            #     << "," << m_eg << std::endl;
            kinetic_eg.append(float(row[0]))
            electric_eg.append(float(row[1]))
            magnetic_eg.append(float(row[2]))
            potential_eg.append(float(row[2]) + float(row[1]))
            total_eg.append(float(row[0]) + float(row[2]) + float(row[1]))

        i += 1

    # print(kinetic_eg)
    # print(em_eg)
    # print(total_eg)

    plt.plot(kinetic_eg, label=title[0])
    plt.plot(electric_eg, label=title[1])
    plt.plot(magnetic_eg, label=title[2])
    plt.plot(potential_eg, label="potential energy")
    plt.plot(total_eg, label="total energy")
    plt.legend()
    # plt.show()
    plt.savefig('energy')
