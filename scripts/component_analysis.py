import os
import h5py
import argparse
from matplotlib import pyplot as plt
import numpy as np
import math
import scipy.stats as sc
import itertools
from matplotlib.colors import LogNorm

import matplotlib
# matplotlib.use('Agg')

parser = argparse.ArgumentParser(
    description='Draw growth factor of whistler')
parser.add_argument('--ignore_x', type=bool,
                    default=False, help='ignore Bx field')
parser.add_argument('--ignore_y', type=bool,
                    default=False, help='ignore By field')
parser.add_argument('--ignore_z', type=bool,
                    default=False, help='ignore Bz field')
parser.add_argument('--path',  default='.')
parser.add_argument('--kxmin', type=float, default=0)
parser.add_argument('--kxmax', type=float, default=0)
parser.add_argument('--kymin', type=float, default=0)
parser.add_argument('--kymax', type=float, default=0)
parser.add_argument('--xmin', type=float, default=0)
parser.add_argument('--xmax', type=float, default=1024)
parser.add_argument('--ymin', type=float, default=0)
parser.add_argument('--ymax', type=float, default=1024)
parser.add_argument('--tmax', type=float, default=1)
parser.add_argument('--fitt0', type=float, default=0)
parser.add_argument('--fitt1', type=float, default=0)
parser.add_argument('--fitlength', type=float, default=100)

args = parser.parse_args()


def read_file(filename):
    print('reading: ' + filename)
    with h5py.File(filename, 'r') as f:
        array = f["dset"][()]
        return np.array(array)


def linear_fit(k_values, time_frame):
    (slope, intercept, corr_coef, _, _) = sc.linregress(time_frame, k_values)
    return (slope, intercept, corr_coef)


def save_mode(k_in_time, time_frame, k, dir_specification=''):
    if args.kxmin != args.kxmax and(k < args.kxmin or k > args.kxmax):
        return

    plt.plot(time_frame, k_in_time)
    # plt.ylim(0, max)
    if dir_specification == '':
        name = 'whistler_growth_'+"{:.3f}".format(k).replace('.', '-')
    else:
        name = 'whistler_growth' + dir_specification + \
            '_' "{:.3f}".format(k).replace('.', '-')

    print('saving: ', name)
    plt.savefig(name)
    plt.close('all')
    plt.clf()


def get_dims_ks(filename):
    sh = read_file(filename).shape
    dims = len(sh)
    number_ksx = sh[0]
    number_ksy = 0
    if dims == 2:
        number_ksy = sh[1]

    return (dims, number_ksx, number_ksy)


def find_best_fit(time_frame, time_frame_step, mode_in_time, t0, t1, fit_step):

    if t1 - t0 < fit_step:
        print('time differences must be bigget than 100', t1-t0)
        raise Exception('error')

    lower_bounds = [*range(int(t0), int(t1-fit_step), 10)]

    bounds_to_search = [[x, x+fit_step] for x in lower_bounds]

    best_slope = 0
    best_intercept = 0
    best_corr = -1
    best_interval = []
    for c in bounds_to_search:
        lower_idx = int(c[0]/time_frame_step)
        upper_idx = int(c[1]/time_frame_step)
        (slope, intercept, corr_coef) = linear_fit(mode_in_time[lower_idx:upper_idx],
                                                   time_frame[lower_idx:upper_idx])
        if corr_coef > best_corr:
            best_slope = slope
            best_intercept = intercept
            best_corr = corr_coef
            best_interval = [time_frame[int(c[0])], time_frame[int(c[1])]]

    # print('found best slope: ', best_slope, 'best intercept: ',
    #      best_intercept, 'best corr:', best_corr, 'best_interval', best_interval)
    return best_slope, best_intercept, best_corr, np.array(best_interval)


if __name__ == "__main__":

    Bx_files = []
    By_files = []
    Bz_files = []
    number_ksx = 0
    number_ksy = 0
    dims = 0

    for file in os.listdir(args.path):
        filename = file.split('/')[-1]
        if not args.ignore_x and 'Bx' in filename:
            if not filename[-3:] == ".h5":
                continue

            if dims == 0:
                (dims, number_ksx, number_ksy) = get_dims_ks(filename)

            Bx_files.append(filename)

        if not args.ignore_y and 'By' in filename:
            if not filename[-3:] == ".h5":
                continue

            if dims == 0:
                (dims, number_ksx, number_ksy) = get_dims_ks(filename)

            By_files.append(filename)

        if not args.ignore_z and 'Bz' in filename:
            if not filename[-3:] == ".h5":
                continue

            if dims == 0:
                (dims, number_ksx, number_ksy) = get_dims_ks(filename)

            Bz_files.append(filename)

    if len(Bx_files) == 0 and len(By_files) == 0 and (Bz_files) == 0:
        print('no files found')

    Bx_files.sort()
    By_files.sort()
    Bz_files.sort()

    number_time_snapshots = len(Bx_files)
    if not args.ignore_y and number_time_snapshots == 0:
        number_time_snapshots = len(By_files)
    if not args.ignore_z and number_time_snapshots == 0:
        number_time_snapshots = len(Bz_files)

    print('ksx: ', number_ksx)
    print('ksy: ', number_ksy)
    print('time snaps: ', number_time_snapshots)

    final_array = []
    if dims == 1:
        final_array = np.zeros((number_ksx, number_time_snapshots))
        for i in range(number_time_snapshots):
            print(str(i), 'th t is being processed')
            if not args.ignore_x:
                arr = read_file(Bx_files[i])
                final_array[:, i] += np.absolute(np.fft.fft(arr))**2
            if not args.ignore_y:
                arr = read_file(By_files[i])
                final_array[:, i] += np.absolute(np.fft.fft(arr))**2
            if not args.ignore_z:
                arr = read_file(Bz_files[i])
                final_array[:, i] += np.absolute(np.fft.fft(arr))**2

    if dims == 2:
        final_array = np.zeros((number_ksx, number_ksy, number_time_snapshots))
        for i in range(number_time_snapshots):
            print(str(i), 'th t is being processed')
            if not args.ignore_x:
                arr = read_file(Bx_files[i])
                final_array[:, :, i] += np.absolute(np.fft.fft2(arr))**2
            if not args.ignore_y:
                arr = read_file(By_files[i])
                final_array[:, :, i] += np.absolute(np.fft.fft2(arr))**2
            if not args.ignore_z:
                arr = read_file(Bz_files[i])
                final_array[:, :, i] += np.absolute(np.fft.fft2(arr))**2

    dkx = 2*math.pi / (args.xmax - args.xmin)
    kxmax = final_array.shape[0] * dkx

    dky = 0
    kymax = 0
    if dims == 2:
        dky = 2*math.pi / (args.ymax - args.ymin)
        kymax = final_array.shape[1] * dky

    final_array = np.log(final_array+1)

    if dims == 1:
        final_array_full = np.fft.fftshift(final_array)
        final_array = final_array[0:int(final_array.shape[0]/2), :]

    if dims == 2:
        final_array = final_array[0:int(final_array.shape[0]/2),
                                  0:int(final_array.shape[1]/2),
                                  :]

        # plt.imshow(final_array, cmap='Blues', norm=LogNorm(),
        #           aspect=args.tmax/kxmax, extent=(0, args.tmax, kxmax, 0))

    (time_frame, time_step) = np.linspace(
        0, args.tmax, num=final_array.shape[-1], endpoint=False, retstep=True)

    fit_interval = None

    if(args.fitt0 != args.fitt1):
        fit_interval = (int(args.fitt0/time_step), int(args.fitt1/time_step))

    print(args.fitt0, args.fitt1)
    print(fit_interval)

    if dims == 1:

        growth = []
        fit_ks = []

        for i in range(final_array.shape[0]):
            k = i * dkx

            k_in_time = final_array[i, :]
            save_mode(k_in_time, time_frame, k)

            if fit_interval != None:
                time_to_fit = time_frame[fit_interval[0]:fit_interval[1]]
                if args.kxmin != args.kxmax and(k < args.kxmin or k > args.kxmax):
                    continue
                (slope, intercept, corr, found_interval) = find_best_fit(time_frame, time_step, k_in_time,
                                                                         args.fitt0, args.fitt1, args.fitlength)
                print('correlation:', corr, 'interval:', found_interval)
                # (slope, intercept) = linear_fit(
                #    k_in_time[fit_interval[0]: fit_interval[1]],
                #    time_to_fit)
                growth.append(slope)
                fit_ks.append(k)

                plt.plot(time_frame, k_in_time)
                plotted_interval = time_frame[
                    int(found_interval[0]/time_step):int(found_interval[1]/time_step)]
                plt.plot(plotted_interval, slope * plotted_interval
                         + intercept)
                name = 'withline_'+"{:.3f}".format(k).replace('.', '-')
                print('saving: ', name)
                plt.savefig(name)
                plt.close('all')
                plt.clf()

        if fit_interval != None:
            np.savetxt('Gamma_sim.txt', growth)
            np.savetxt('Kvect_sim.txt', fit_ks)

    if dims == 2:
        whole_growth = []
        kxs = []
        kys = []
        # for i in range(final_array.shape[0]):
        #    print('shape0', i)
        #    plt.imshow(final_array[i, :, :])
        #    plt.colorbar()
        #    plt.savefig('shape0_' + str(i))
        #    plt.close()

        # for i in range(final_array.shape[1]):
        #    print('shape1', i)
        #    plt.imshow(final_array[:, i, :])
        #    plt.colorbar()
        #    plt.savefig('shape1_' + str(i))
        #    plt.close()

        # for i in range(final_array.shape[2]):
        #    print('shape2', i)
        #    plt.imshow(final_array[:, :, i])
        #    plt.colorbar()
        #    plt.savefig('shape2_' + str(i))
        #    plt.close()

        # for i in range(final_array.shape[0]):
        #    print('diagonal', i)
        #    plt.plot(final_array[i, i, :])
        #    plt.savefig('diagonal' + str(i))
        #    plt.close()

        print('shape', final_array.shape, dkx, dky)
        for i in range(final_array.shape[0]):
            kx = i*dkx
            if kx < args.kxmin or kx > args.kxmax:
                continue
            kxs.append(kx)

        for j in range(final_array.shape[1]):
            ky = j*dky
            if ky < args.kymin or ky > args.kymax:
                continue
            kys.append(ky)

        for i in range(final_array.shape[0]):
            kx = i*dkx
            if kx < args.kxmin or kx > args.kxmax:
                continue
            growthx = []
            for j in range(final_array.shape[1]):
                ky = j*dky
                if ky < args.kymin or ky > args.kymax:
                    continue

                (slope, intercept, corr, found_interval) = find_best_fit(time_frame, time_step, final_array[j, i, :],
                                                                         args.fitt0, args.fitt1, args.fitlength)
                plt.plot(time_frame, final_array[j, i, :])
                plotted_interval = time_frame[
                    int(found_interval[0]/time_step):int(found_interval[1]/time_step)]
                plt.plot(plotted_interval, slope * plotted_interval
                         + intercept)
                name = 'withline_x_' + \
                    '{:.3f}'.format(kx).replace('.', '-') + \
                    '_y_'+'{:.3f}'.format(ky).replace('.', '-')
                plt.savefig(name)
                plt.close()
                print('k:[', kx, ky, '], slope:', slope, 'correlation:',
                      corr, 'interval:', found_interval)
                growthx.append(slope)
            if len(growthx) > 0:
                whole_growth.append(growthx)

        if(args.fitt0 != args.fitt1):
            # plt.imshow(whole_growth)
            # plt.show()
            np.save('Gamma_whole_sim', whole_growth)
            np.save('Kvecty_sim', kys)
            np.save('Kvectx_sim', kxs)

