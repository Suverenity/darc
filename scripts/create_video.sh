#/bin/bash
ffmpeg -y -framerate 120 -pattern_type glob -i "$1*.png" -c:v libx264 -r 30 -vf "scale=1200:-2,format=yuv420p" -movflags +faststart $1.mp4
