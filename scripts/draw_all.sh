#!/bin/bash

CURR_PATH="`dirname \"$0\"`"
draw_files="$CURR_PATH"/draw_files.py
show_hdf2d="$CURR_PATH"/show_hdf2d.py
create_video="$CURR_PATH"/create_video.sh
draw_energy="$CURR_PATH"/draw_energy_csv.py

cpus=`nproc`

python $draw_files --path . --prefix rho --ymin=-0.1 --ymax=0.1 --cpus=$cpus 
$create_video rho_el
$create_video rho_pt

python $draw_files --path . --prefix Bx --ymin=-1 --ymax=1 --cpus=$cpus
$create_video Bx
python $draw_files --path . --prefix By --ymin=-1 --ymax=1 --cpus=$cpus
$create_video By
python $draw_files --path . --prefix Bz --ymin=-1 --ymax=1 --cpus=$cpus
$create_video Bz

python $draw_files --path . --prefix Ex --ymin=-1 --ymax=1 --cpus=$cpus
$create_video Ex
python $draw_files --path . --prefix Ey --ymin=-1 --ymax=1 --cpus=$cpus
$create_video Ey
python $draw_files --path . --prefix Ez --ymin=-1 --ymax=1 --cpus=$cpus
$create_video Ez
#
#python $draw_files --path . --prefix djdtx --ymin=-1 --ymax=1 --cpus=$cpus && $create_video djdtx
#python $draw_files --path . --prefix djdty --ymin=-1 --ymax=1 --cpus=$cpus && $create_video djdty
#python $draw_files --path . --prefix djdtz --ymin=-1 --ymax=1 --cpus=$cpus && $create_video djdtz
#
#python $draw_files --path . --prefix jx --ymin=-1 --ymax=1 --cpus=$cpus
#$create_video jx_el
#$create_video jx_pt
#python $draw_files --path . --prefix jy --ymin=-1 --ymax=1 --cpus=$cpus
#$create_video jy_el
#$create_video jy_pt
#python $draw_files --path . --prefix jz --ymin=-1 --ymax=1 --cpus=$cpus
#$create_video jz_el
#$create_video jz_pt
##
#python $show_hdf2d --path . --xbin=100 --ybin=100 --xmin=-5 --xmax=5 --ymin=-5 --ymax=5 --xmask=el_vx --ymask=el_vy --cpus=$cpus && $create_video el_vx_el_vy
#python $show_hdf2d --path . --xbin=100 --ybin=100 --xmin=-5 --xmax=5 --ymin=-5 --ymax=5 --xmask=el_vy --ymask=el_vz --cpus=$cpus && $create_video el_vy_el_vz
#python $show_hdf2d --path . --xbin=100 --ybin=100 --xmin=-5 --xmax=5 --ymin=-5 --ymax=5 --xmask=el_vx --ymask=el_vz --cpus=$cpus && $create_video el_vx_el_vz
#python $draw_energy energy.csv

#python $draw_files --path . --prefix ay --ymin=-0.1 --ymax=0.1 --cpus=1 # && $create_video rho 
#python $draw_files --path . --prefix az --ymin=-0.1 --ymax=0.1 --cpus=1 # && $create_video rho 
#
#python $draw_files --path . --prefix mxy --ymin=-0.1 --ymax=0.1 --cpus=1 # && $create_video rho 
#python $draw_files --path . --prefix mxz --ymin=-0.1 --ymax=0.1 --cpus=1 # && $create_video rho 
#
#python $draw_files --path . --prefix bx --ymin=-0.1 --ymax=0.1 --cpus=1 # && $create_video rho 
#python $draw_files --path . --prefix by --ymin=-0.1 --ymax=0.1 --cpus=1 # && $create_video rho 
#python $draw_files --path . --prefix bz --ymin=-0.1 --ymax=0.1 --cpus=1 # && $create_video rho 

#python $draw_files --path . --prefix ex --ymin=-0.1 --ymax=0.1 --cpus=1 # && $create_video rho 
#python $draw_files --path . --prefix ey --ymin=-0.1 --ymax=0.1 --cpus=1 # && $create_video rho 
#python $draw_files --path . --prefix ez --ymin=-0.1 --ymax=0.1 --cpus=1 # && $create_video rho 

#python $draw_files --path . --prefix tbx --ymin=-0.1 --ymax=0.1 --cpus=1 # && $create_video rho 
#python $draw_files --path . --prefix tby --ymin=-0.1 --ymax=0.1 --cpus=1 # && $create_video rho 
#python $draw_files --path . --prefix tbz --ymin=-0.1 --ymax=0.1 --cpus=1 # && $create_video rho 

#python $draw_files --path . --prefix tex --ymin=-0.1 --ymax=0.1 --cpus=1 # && $create_video rho 
#python $draw_files --path . --prefix tey --ymin=-0.1 --ymax=0.1 --cpus=1 # && $create_video rho 
#python $draw_files --path . --prefix tez --ymin=-0.1 --ymax=0.1 --cpus=1 # && $create_video rho 

