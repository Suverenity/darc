#!/bin/bash

#nohup mpirun -x HDF5_USE_FILE_LOCKING="FALSE" -np 256 --hostfile hostfile ../particles/bin/sim_2d parameters.json &

mkdir -p build_amalka
mkdir -p bin
cd build_amalka
cmake .. -DCMAKE_BUILD_TYPE=release -DUSE_FLOAT=ON -DUSE_MPI=ON -DAMALKA=ON -DMPI_CXX_COMPILER=mpicxx.openmpi -DMPI_C_COMPILER=mpicc.openmpi
make -j

cp ./tests/sim_1d ../bin
cp ./tests/sim_2d ../bin
